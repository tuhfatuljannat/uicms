<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>Repo Index</title>

	<style type="text/css">

		::selection { background-color: #E13300; color: white; }
		::-moz-selection { background-color: #E13300; color: white; }

		body {
			background-color: #fff;
			margin: 40px;
			font: 13px/20px normal Helvetica, Arial, sans-serif;
			color: #4F5155;
		}

		a {
			color: #003399;
			background-color: transparent;
			font-weight: normal;
		}

		h1 {
			color: #444;
			background-color: transparent;
			border-bottom: 1px solid #D0D0D0;
			font-size: 19px;
			font-weight: normal;
			margin: 0 0 14px 0;
			padding: 14px 15px 10px 15px;
		}

		code {
			font-family: Consolas, Monaco, Courier New, Courier, monospace;
			font-size: 12px;
			background-color: #f9f9f9;
			border: 1px solid #D0D0D0;
			color: #002166;
			display: block;
			margin: 14px 0 14px 0;
			padding: 12px 10px 12px 10px;
		}

		#body {
			margin: 0 15px 0 15px;
		}

		p.footer {
			text-align: right;
			font-size: 11px;
			border-top: 1px solid #D0D0D0;
			line-height: 32px;
			padding: 0 10px 0 10px;
			margin: 20px 0 0 0;
		}

		#container {
			margin: 10px;
			border: 1px solid #D0D0D0;
			box-shadow: 0 0 8px #D0D0D0;
		}


		p.title {
			color: #444;
			background-color: transparent;
			border-bottom: 1px solid #D0D0D0;
			font-size: 14px;
			font-weight: normal;
			margin: 0 0 14px 0;
			padding: 14px 15px 10px 15px;
		}
	</style>
</head>
<body>

<div id="container">
	<h1>Welcome to APPTEUM CI Repository Documentation</h1>

	<div id="body">

		<p class="title">The corresponding controller for this page is found at:</p>
		<code>application/controllers/Repo.php</code>

		<p class="title">If you would like to debug / data array print you'll use:</p>
		<code>
			echo dump($data, TREU);
			<br>
			[Note: param TRUE is used for die() code here. Without die() you'll use echo dump($data);]
		</code>

		<p class="title">If you would like to debug / data array print with datatype you'll use:</p>
		<code>
			echo vdump($data, TREU);
			<br>
			[Note: param TRUE is used for die() code here. Without die() you'll use echo vdump($data);]
		</code>

		<p class="title">If you would like to add <b>css and js</b> file in controller, you'll use:</p>
		<code>
			add_css(array('style.css','custom.css','admin/style.css')); <br>
			add_js('custom.js');
			<br>
			[Note: param ARRAY is used for multile file add. Default directory link is <b>base_url().'public/js/</b> for js and <b>base_url().'public/css/</b> for css]
		</code>

		<p class="title">If you would like to add <b>css and js library</b> file in controller, you'll use:</p>
		<code>
			add_lib_css('admin/css/plugins/dataTables/datatables.min.css'); <br>
			add_lib_js(array( <br>
				&nbsp;&nbsp;&nbsp;&nbsp;	'admin/js/plugins/dataTables/datatables.min.js', <br>
				&nbsp;&nbsp;&nbsp;&nbsp;	'admin/js/plugins/dataTables/dataTables.bootstrap4.min.js' <br>
				));
			<br>
			[Note: param ARRAY is used for multile file add. Default directory link is <b>base_url().'public/</b>]
		</code>


		<p class="title">If you would like to put config item you'll use:</p>
		<code>
			 echo config_item('site_footer_content'); [Note: use site footer]<br>
			 echo config_item('meta_title'); [Note: use meta title]<br>
			 echo config_item('site_name'); [Note: use site name]<br>
			 echo config_item('site_address'); [Note: use site address]<br>
			 echo en2bn_number(6); [Note: covvert eng to ban number]<br>
			 echo vdump('This is text'); [Note: use data array with datatype]<br>
		</code>
	</div>

	<p class="footer">Page rendered in <strong>{elapsed_time}</strong> seconds. <?php echo  (ENVIRONMENT === 'development') ?  'CodeIgniter Version <strong>' . CI_VERSION . '</strong>' : '' ?></p>
</div>

</body>
</html>