<?php
defined('BASEPATH') OR exit('No direct script access allowed');
// echo $this->session->flashdata('msg');
?>


<div class="ibox-content">
    <form id="send-mail" method="post">
        <input type="email" name="from" class="form-control" placeholder="Enter Email" required><br>
        <textarea name="message" class="form-control" placeholder="Enter message here" required></textarea><br>
        <button type="submit" class="btn btn-primary">Send Message</button>
    </form>
</div>