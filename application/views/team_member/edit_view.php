<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Team</h2>
        <ol class="breadcrumb">
            <li class="breadcrumb-item">
                <a href="index.html">Home</a>
            </li>
            <li class="breadcrumb-item">
                <a>Team</a>
            </li>
            <li class="breadcrumb-item active">
                <strong class="form-title">Team Create</strong>
            </li>
        </ol>
    </div>
    <div class="col-lg-2">
        <div class="title-action">
            <a href="<?php echo base_url(); ?>team" class="btn btn-primary">Team List</a>
        </div>
    </div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
                <div class="ibox ">
                    <div class="ibox-title">
                        <h5 class="form-title">Team Create</h5>
                        <div class="ibox-tools">
                            <a class="collapse-link">
                                <i class="fa fa-chevron-up"></i>
                            </a>
                            <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                                <i class="fa fa-wrench"></i>
                            </a>
                            <ul class="dropdown-menu dropdown-user">
                                <li><a href="#" class="dropdown-item">Config option 1</a>
                                </li>
                                <li><a href="#" class="dropdown-item">Config option 2</a>
                                </li>
                            </ul>
                            <a class="close-link">
                                <i class="fa fa-times"></i>
                            </a>
                        </div>
                    </div>
                    <div class="ibox-content">
                        <form id="form-team" method="POST" enctype="multipart/form-data">
                                <!-- <p>Sign in today for more expirience.</p> -->
                                    <div class="form-group row">
                                        <div class="col-lg-2"></div>
                                        <label class="col-lg-2 col-form-label">Name</label>
                                        <div class="col-lg-6">
                                            <input type="hidden" class="form-control" id="id" name="id">
                                            <input type="text" placeholder="Name" id="name" class="form-control" name="name">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <div class="col-lg-2"></div>
                                        <label class="col-lg-2 col-form-label">Designation</label>
                                        <div class="col-lg-6">
                                            <input type="text" placeholder="" id="designation" class="form-control" name="designation">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <div class="col-lg-2"></div>
                                        <label class="col-lg-2 col-form-label">File</label>
                                        <div class="col-lg-6">
                                            <div class="fileinput fileinput-new" data-provides="fileinput">
                                                <div>
                                                    <span class="btn btn-default btn-file"><span class="fileinput-new"><span class="fa fa-camera fa-2x"></span> <!-- File --></span>
                                                    <div id="preview" name="preview"  class="fileinput-preview fileinput-exists thumbnail" style="width: 150px; height: 100px; display:block;"></div>
                                                    <img id="prevImg" name="prevImg" />
                                                    <input type="file" name="file" id="file" class="ephoto-upload" accept="image/jpeg">
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-lg-2"></div>
                                    <label class="col-lg-2 col-form-label">Link Fb</label>
                                    <div class="col-lg-6">
                                        <input type="text" placeholder="" id="link_fb" class="form-control" name="link_fb">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-lg-2"></div>
                                    <label class="col-lg-2 col-form-label">Link Twt</label>
                                    <div class="col-lg-6">
                                        <input type="text" placeholder="" id="link_twt" class="form-control" name="link_twt">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-lg-2"></div>
                                    <label class="col-lg-2 col-form-label">Link In</label>
                                    <div class="col-lg-6">
                                        <input type="text" placeholder="" id="link_in" class="form-control" name="link_in">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-lg-2"></div>

                                    <label class="col-lg-2 col-form-label">Link Gp</label>
                                    <div class="col-lg-6">
                                        <input type="text" placeholder="" id="link_gp" class="form-control" name="link_gp">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-lg-2"></div>
                                    <label class="col-lg-2 col-form-label" id="status">Status</label>
                                    <div class="col-lg-6">
                                        <div class="i-checks"><label> <input type="checkbox" id="is_active" name="is_active" checked></label></div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-lg-8"></div>
                                    <div class="col-lg-3">
                                        <button class="btn btn-sm btn-default btn-form-reset" type="submit">RESET</button>
                                        <button class="btn btn-sm btn-primary btn-form-submit" type="submit">SAVE</button>
                                    </div>
                                </div>
                        </form>
                    </div>
                </div>
        </div>
    </div>
</div>