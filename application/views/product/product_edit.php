<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<div class="row wrapper border-bottom white-bg page-heading">
	<div class="col-lg-10">
		<h2>Product</h2>
		<ol class="breadcrumb">
			<li class="breadcrumb-item">
				<a href="index.html">Home</a>
			</li>
			<li class="breadcrumb-item">
				<a>Product</a>
			</li>
			<li class="breadcrumb-item active">
				<strong class="form-title">Product Create</strong>
			</li>
		</ol>
	</div>
	<div class="col-lg-2">
		<div class="title-action">
			<a href="<?php echo base_url(); ?>product" 
				class="btn btn-primary">Product List</a>
		</div>
	</div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">
 <div class="row">
     <div class="col-lg-10">
         <div class="ibox ">
            <div class="ibox-title">
               <h3 class="form-title">ADD NEW PRODUCT</h3>
               <div class="ibox-tools">
                  <a class="collapse-link">
                  <i class="fa fa-chevron-up"></i>
                  </a>
                  <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                  <i class="fa fa-wrench"></i>
                  </a>
                  <ul class="dropdown-menu dropdown-user">
                     <li><a href="#" class="dropdown-item">Config option 1</a>
                     </li>
                     <li><a href="#" class="dropdown-item">Config option 2</a>
                     </li>
                  </ul>
                  <a class="close-link">
                  <i class="fa fa-times"></i>
                  </a>
               </div>
            </div>
            <div class="ibox-content">
               <form id="product_form" method="post">
                  <div class="row">
	                   <div class="col-md-6">
		                    <div class="form-group row">
		                       <label class=" col-form-label">Name</label>
		                       <div class="col-lg-9">
		                          <input type="hidden" class="form-control" 
		                          id="id" name="id">
		                          <input type="text" class="form-control" placeholder="Product Name" name="name" 
		                          id="name"/>
		                       </div>
		                    </div>
	                   </div>
		                 <div class="col-md-6">
		                   <div class="form-group">
		                       <div class="fileinput fileinput-new" data-provides="fileinput">
		                          <div>
		                             <span class="btn btn-default btn-file">
		                                <span class="fileinput-new">
		                                   <span class="fa-2x"> ADD Photo</span> <!-- File -->
		                                </span>
		                                <div id="preview" name="preview"  class="fileinput-preview fileinput-exists thumbnail" style="width: 150px; height: 100px; display:block;"></div>
		                                <img id="prevImg" name="prevImg" />
		                                <input type="file" name="file" id="file" class="ephoto-upload" accept="image/jpeg">
		                             </span>
		                          </div>
		                       </div>
		                       <a href="#" class="fa fa-plus btn btn-primary">Add More</a>
		                   </div>
		                 </div>
                  </div>

                  <div class="row">
                     <div class="col-md-6">
                        <div class="form-group row">
                           <label class=" col-form-label">ProductType:</label>
                           <div class="col-lg-9">
                              <select name="type_id" id="type_id" 
                              		class="form-control">
                                 <option value="">Select Product Type</option>
                                 <option value="1">product01</option>
                                 <option value="2">product02</option>
                                 <option value="3">product03</option>
                              </select>
                           </div>
                        </div>
                     </div>
                  </div>

                  <div class="row">
                     <div class="col-md-6">
                         <div class="form-group row">
                           <label class="col-lg-2 col-form-label">Model:</label>
                           <div class="col-lg-3" id="margin-left">
                              <input type="text" class="form-control" placeholder="Model" name="code[]" id ="code[]"/>
                           </div>
                           <div class="col-lg-3">
                              <input type="text" class="form-control" placeholder="Model" name="code[]" id ="code[]"/>
                           </div>
                           <div class="col-lg-3">
                              <input type="text" class="form-control" placeholder="Model" name="code[]" id ="code[]"/>
                           </div>
                         </div>
                     </div>
                     <!-- <div class="col-md-6">
                        <div class="form-group">
                        <label class="col-lg-3 col-form-label">ProductType:</label>
                        <label class="i-checks"> <input type="radio" value="" class="productType" name="product_type" id="finished_good" checked="True">&nbsp;FINISHED GOODS</label>
                        <label class="i-checks"> <input type="radio" value="" class="productType" name="product_type" id="parts">&nbsp;PARTS</label>
                        
                        </div>
                        </div> -->
                     <div class="col-md-6">
                         <div class="form-group">
                           <label class="col-lg-3 col-form-label">
                           	ProductType:
                           </label>
                           <label class=""><input type="radio" 
                           	value="finished_good" class="productType" 
                           	name="product_type" id="finished_good" checked="True">&nbsp;FINISHED GOODS</label>
                           <label class=""> <input type="radio" value="parts" class="productType" name="product_type" id="parts">&nbsp;PARTS</label>
                         </div>
                     </div>
                  </div>

                  <div class="row">
                     <div class="col-md-6">
                         <div class="form-group row">
                           <label class="col-lg-2 col-form-label">Category:</label>
                           <div class="col-lg-9">
                              <select name="category_id"id="category_id" 
                              		class="form-control">
                                 <option value="">Select Category</option>
                                 <option value="1">Electronics</option>
                                 <option value="2">Clothes</option>
                                 <option value="3">Mackup</option>
                              </select>
                           </div>
                         </div>
                     </div>
                     <div class="col-md-6">
                         <div class="form-group row">
                           <label class="col-lg-3 col-form-label">
                           	SubCategory:</label>
                           <div class="col-lg-8">
                              <select name="sub_category_id"id="sub_category_id" 	class="form-control">
                                 <option value="">Select Sub Category</option>
                                 <option value="1">Tv</option>
                                 <option value="2">Shirt</option>
                                 <option value="3">Lipstick</option>
                              </select>
                           </div>
                         </div>
                     </div>
                  </div>

                  <div class="row">
                     <div class="col-md-6">
                         <div class="form-group row">
                           <label class="col-lg-2 col-form-label">Brand:</label>
                           <div class="col-lg-9">
                              <select name="brand_id"id="brand_id" 
                              	 class="form-control">
                                 <option value="">Select Brand</option>
                                 <option value="1">Sony</option>
                                 <option value="2">Rich Man</option>
                                 <option value="3">Flora</option>
                              </select>
                           </div>
                         </div>
                     </div>
                     <div class="col-md-6">
                         <div class="form-group row">
                           <label class="col-lg-2 col-form-label">Unit:</label>
                           <div class="col-lg-9">
                              <select name="unit_id"id="unit_id" 
                                 class="form-control">
                                 <option value="">Select Unit</option>
                                 <option value="1">2pics</option>
                                 <option value="2">3pics</option>
                                 <option value="3">4pics</option>
                              </select>
                           </div>
                         </div>
                     </div>
                  </div>

                  <div class="row">
                     <div class="col-md-6">
                         <div class="form-group row">
                           <label class="col-lg-2 col-form-label">BulkUnit:</label>
                           <div class="col-lg-9">
                              <select name="bulk_unit_id"id="bulk_unit_id" 
                                 class="form-control">
                                 <option value="">Select Bulk Unit</option>
                                 <option value="1">5 packet</option>
                                 <option value="2">6 packet</option>
                              </select>
                           </div>
                         </div>
                     </div>
                  </div>

                  <div class="row">
                     <div class="col-md-6">
                         <div class="form-group row">
                           <label class="col-form-label">Dimentions:</label>
                           <div class="col-lg-3">
                              <input type="text" class="form-control" placeholder="L" name="dim_in[]" id ="dim_in[]"/>
                           </div>
                           <div class="col-lg-3">
                              <input type="text" class="form-control" placeholder="L" name="dim_in[]" id ="dim_in[]"/>
                           </div>
                           <div class="col-lg-3">
                              <input type="text" class="form-control" placeholder="L" name="dim_in[]" id ="dim_in[]"/></br>
                           </div>
                           <input type="text" class="form-control col-lg-3" placeholder="L" name="dim_op" id ="dim_op" style="margin-left:90px;"/>
                         </div>
                     </div>
                     <div class="col-md-6">
                         <div class="form-group row">
                           <label class="col-lg-2 col-form-label">
                           	Weight:</label>
                           <div class="col-lg-9">
                              <select name="weight_id"id="weight_id" 
                              	 class="form-control">
                                 <option value="">Select Weight</option>
                                 <option value="1">6 kg</option>
                                 <option value="2">6 kg</option>
                              </select>
                           </div>
                         </div>
                     </div>
                  </div>

                  <div class="row">
                     <div class="col-md-6">
                         <div class="form-group row">
                           <label class="col-form-label">DefaultPackaging:</label>
                           <div class="col-lg-3">
                              <input type="text" class="form-control" placeholder="L" name="pack_in[]" id ="pack_in[]"/>
                           </div>
                           <div class="col-lg-3">
                              <input type="text" class="form-control" placeholder="L" name="pack_in[]" id ="pack_in[]"/>
                           </div>
                           <div class="col-lg-3">
                              <input type="text" class="form-control" placeholder="L" name="pack_in[]" id ="pack_in[]"/></br>
                           </div>
                           <input type="text" class="form-control col-lg-3" placeholder="L" name="pack_op" id ="pack_op" 
                           style="margin-left:125px;"/>
                         </div>
                     </div>
                     <div class="col-md-6">
                         <div class="form-group row">
                           <label class=" col-form-label">GrossWeight:</label>
                           <div class="col-lg-9">
                             <select name="gross_weight_id" id="gross_weight_id"        class="form-control">
                                  <option value="">Select Gross Weight</option>
                                  <option value="1">3 kg</option>
                                  <option value="2">2 kg</option>
                             </select>
                           </div>
                         </div>
                     </div>
                  </div>

                  <div class="row">
                     <div class="col-md-6">
                        <div class="form-group row">
                           <label class="col-lg-2 col-form-label">Options:</label>
                           <div class="col-lg-9">
                              <select name="options"id="options" class="form-control">
                                 <option value="">Select Options</option>
                              </select>
                           </div>
                        </div>
                     </div>
                  </div>
                  </br>

                  <div class="color_div">
                     <div class="row margin">
                        <div class="col-md-6">
                           <div class="form-group row">
                              <label class="col-form-label">Color:</label>
                              <div class="col-lg-9">
                                 <select name="color"id="color" class="form-control">
                                    <option value="">Select Color</option>
                                 </select>
                              </div>
                           </div>
                        </div>
                        <div class="col-md-6">
                           <div class="form-group row">
                              <label class="col-form-label">Size:</label>
                              <div class="col-lg-9">
                                 <select name="size"id="size" class="form-control">
                                    <option value="">Select Size</option>
                                 </select>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="row margin">
                        <div class="col-md-6">
                           <div class="form-group row">
                              <label class="col-form-label">Capacity:</label>
                              <div class="col-lg-9">
                                 <select name="capacity"id="capacity" class="form-control">
                                    <option value="">Select Capacity</option>
                                 </select>
                              </div>
                           </div>
                        </div>
                        <div class="col-md-6">
                           <div class="form-group row">
                              <label class="col-form-label">Storage:</label>
                              <div class="col-lg-9">
                                 <select name="storage"id="storage" class="form-control">
                                    <option value="">Select Storage</option>
                                 </select>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
                  </br>

                  <div class="row">
                     <div class="col-md-6">
                         <div class="form-group row">
                             <div class="">
                              <label class="i-checks"> <input type="checkbox" value="" name="building_parts" 
                              	id="building_parts[]" checked="True">&nbsp;Buildin Part :</label><br>
                              <small>(For Finished Product)</small>
                             </div>
                             <div class="col-lg-7 building_part_input">
                              <input type="text" class="form-control" placeholder="Part Name" name="building_parts[]" 
                              id ="building_parts[]"/></br>
                              <input type="text" class="form-control" placeholder="Part Name" name="building_parts[]" 
                              id ="building_parts[]"/></br>
                             </div>
                         </div>
                         <button class="fa fa-plus btn btn-primary add_input_field" style="margin-left:108px;">Add More</button>
                     </div>
                     <div class="col-md-6">
                         <div class="form-group row">
                           <label class="i-checks"> <input type="checkbox" 
                           	value="" name="add_custom_field[]" 
                           	id="add_custom_field[]" checked="True">&nbsp;Add Custom Field:</label><br>
                           <div class="col-lg-9 input_field">
                              <textarea class="form-control" 
                              id="add_custom_field[] "name="add_custom_field[]">
                              </textarea><br>
                           </div>
                         </div>
                         <button class="fa fa-plus btn btn-primary add_field">Add More</button>
                     </div>
                  </div>
                  <br>

                  <div id="even">
                     <div class="row">
                         <div class="col-md-6">
                           <div class="form-group row">
                              <div class="">
                                 <label class="i-checks"> <input type="checkbox" value="" name="add_parts[]" id="add_parts[]" checked="True">&nbsp;Add Parts :</label><br>
                                 <small>(For Custom Product)</small>
                              </div>
                              <div class="col-lg-7 part_input">
                                 <input type="text" class="form-control" placeholder="Part Name" name="add_parts[]" 
                                 id ="add_parts[]"/></br>
                                 <input type="text" class="form-control" placeholder="Part Name" name="add_parts[]" 
                                 id ="add_parts[]"/></br>
                              </div>
                           </div>
                           <button class="fa fa-plus btn btn-primary add_part_input_field" style="margin-left:108px;">Add More</button>
                         </div>
                     </div>
                     <br>

                     <div class="row">
                         <div class="col-md-6">
                           <div class="form-group row">
                              <div class="">
                                 <label class="i-checks"> <input type="checkbox" value="" name="add_service[]" 
                                 	id="add_service[]" checked="True">&nbsp;Add Service :</label><br>
                                 <small>(For Custom Product)</small>
                              </div>
                              <div class="col-lg-7 service_input">
                                 <input type="text" class="form-control" placeholder="Part Name" name="add_service[]" 
                                 id ="add_service[]"/></br>
                                 <input type="text" class="form-control" placeholder="Part Name" name="add_service[]" 
                                 id ="add_service[]"/></br>
                              </div>
                           </div>
                           <button class="fa fa-plus btn btn-primary add_service_input_field" style="margin-left:108px;">Add More</button>
                         </div>
                     </div>
                  </div>

                  <div class="form-group row">
                     <div class="col-lg-12">
                        <button class="btn btn-sm btn-primary btn-form-submit pull-right" type="submit">SAVE</button>
                     </div>
                  </div>
               </form>
            </div>
         </div>
     </div>
 </div>
</div>
