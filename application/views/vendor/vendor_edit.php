<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>vendor</h2>
        <ol class="breadcrumb">
            <li class="breadcrumb-item">
                <a href="index.html">Home</a>
            </li>
            <li class="breadcrumb-item">
                <a>vendor</a>
            </li>
            <li class="breadcrumb-item active">
                <strong class="form-title">vendor Create</strong>
            </li>
        </ol>
    </div>
    <div class="col-lg-2">
        <div class="title-action">
            <a href="<?php echo base_url(); ?>vendors" 
                class="btn btn-primary">vendor List</a>
        </div>
    </div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-6">
            <div class="ibox ">
                <div class="ibox-title">
                    <h5 class="form-title">vendor Create</h5>
                    <div class="ibox-tools">
                        <a class="collapse-link">
                            <i class="fa fa-chevron-up"></i>
                        </a>
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                            <i class="fa fa-wrench"></i>
                        </a>
                        <ul class="dropdown-menu dropdown-user">
                            <li><a href="#" class="dropdown-item">Config option 1</a>
                            </li>
                            <li><a href="#" class="dropdown-item">Config option 2</a>
                            </li>
                        </ul>
                        <a class="close-link">
                            <i class="fa fa-times"></i>
                        </a>
                    </div>
                </div>
                <div class="ibox-content">
                    <form id="form-vendor" method="POST">
                        <div class="form-group row">
                            <label class="col-lg-3 col-form-label">Code</label>
                            <div class="col-lg-9">
                                <input type="hidden" class="form-control" id="id" name="id">
                                <input type="text" placeholder="Code" id="code" class="form-control" name="code">
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-lg-3 col-form-label">Name</label>
                            <div class="col-lg-9">
                                <input type="text" placeholder="Name" id="name" class="form-control" name="name">
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-lg-3 col-form-label">Mobile</label>
                            <div class="col-lg-9">
                                <input type="text" placeholder="Mobile" id="mobile" class="form-control" name="mobile">
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-lg-3 col-form-label">Email</label>
                            <div class="col-lg-9">
                                <input type="email" placeholder="Email" id="email" class="form-control" name="email">
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-lg-3 col-form-label">Address</label>
                            <div class="col-lg-9">
                                <textarea class="form-control" id="address" 
                                name="address"></textarea>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-lg-3 col-form-label">Product Service
                            </label>
                            <div class="col-lg-9">
                                <select name="product_service[]" 
                                id="product_service" data-placeholder="Choose Product..." 
                                class="form-control chosen-select" 
                                multiple tabindex="4">
                                    <option value="Product01">Product01</option>
                                    <option value="Product02">Product02</option>
                                    <option value="Product03">Product03</option>
                                    <option value="Product04">Product04</option>
                                </select>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-lg-3 col-form-label">Status
                            </label>
                            <div class="col-lg-9">
                                <label class="i-checks"> <input type="checkbox" value="1" name="is_active" id="is_active" checked="1">&nbsp;Active </label>
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-lg-offset-3 col-lg-9">
                                <button class="btn btn-sm btn-primary btn-form-submit" type="submit">SAVE</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>