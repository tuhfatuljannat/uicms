<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Add Service</h2>
        <ol class="breadcrumb">
            <li class="breadcrumb-item">
                <a href="index.html">Home</a>
            </li>
            <li class="breadcrumb-item">
                <a>Forms</a>
            </li>
            <li class="breadcrumb-item active">
                <strong>Service</strong>
            </li>
        </ol>
    </div>
    <div class="col-lg-2">
        <div class="title-action">
            <a href="<?php echo base_url(); ?>service" class="btn btn-primary">Service List</a>
        </div>
    </div>
</div>

<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-4">
            <div class="ibox-content">
                <h2 style="text-align:center;">Create Service</h2><br>
                <form id="form-service" method="POST">
                    <div class="form-group row">
                        <label class="col-lg-3 col-form-label"> Service Name</label>
                        <div class="col-lg-9">
                            <input type="hidden" class="form-control" id="id" name="id">
                            <input type="text" placeholder="Name" id="name" class="form-control" name="name">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-lg-3 col-form-label">Code</label>
                        <div class="col-lg-9">
                            <input type="text" placeholder="Code" id="code" class="form-control" name="code">
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-lg-3 col-form-label">Type</label>
                        <div class="col-lg-9">
                            <select name="type_id" id="type_id" class="form-control">
                                <option value="">Select Type</option>
                                <option value="1">Type 1</option>

                                <option value="2">Type 2</option>
                                <option value="3">Type 3</option>

                            </select>
                        </div>
                    </div>


                    <div class="form-group row">
                        <label class="col-lg-3 col-form-label">Category</label>
                        <div class="col-lg-9">
                            <select name="category_id" id="category_id" class="form-control input-lg">

                                <option value="">Select Category</option>


                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-lg-3 col-form-label"> Sub Category</label>
                        <div class="col-lg-9">
                            <select name="sub_category_id" id="sub_category_id" class="form-control input-lg">

                                <!-- <option value="">Select Sub Category</option> -->


                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-lg-3 col-form-label">Unit</label>
                        <div class="col-lg-9">
                            <select name="unit_id" id="unit_id" class="form-control">
                                <option value="">Select Unit</option>
                                <option value="1">UNIT 1</option>

                                <option value="2">UNIT 2</option>

                                <option value="3">UNIT 3</option>

                            </select>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-lg-3 col-form-label">Status</label>

                        <div class="col-lg-9">
                              <label class="i-checks"> <input type="checkbox" value="1" name="is_active" 
                                id="is_active" checked="1">&nbsp;Active </label>

                        </div>
                    </div>


                    <div class="form-group row">
                        <div class="col-lg-offset-3 col-lg-9">
                            <button class="btn btn-sm btn-primary btn-form-submit" type="submit">SAVE</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>

    </div>
</div>