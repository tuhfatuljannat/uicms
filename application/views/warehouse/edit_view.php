<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Add Warehouse</h2>
        <ol class="breadcrumb">
            <li class="breadcrumb-item">
                <a href="index.html">Home</a>
            </li>
            <li class="breadcrumb-item">
                <a>Warehouse</a>
            </li>
            <li class="breadcrumb-item active">
                <strong>Create </strong>
            </li>
        </ol>
    </div>
    <div class="col-lg-2">
        <div class="title-action">
           <a href="<?php echo base_url(); ?>warehouse" class="btn btn-primary">Warehouse List</a>
        </div>
    </div>
</div>

<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-4">
            <div class="ibox-content">
                <h2 style="text-align:center;">Create Warehouse</h2><br>
                <form id="form-warehouse" method="POST">
                    <div class="form-group row">
                        <label class="col-lg-3 col-form-label">Name</label>
                        <div class="col-lg-9">
                            <input type="hidden" class="form-control" id="id" name="id">
                            <input type="text" placeholder="Name" id="name" class="form-control" name="name">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-lg-3 col-form-label">Code</label>
                        <div class="col-lg-9">
                            <input type="text" placeholder="Code" id="code" class="form-control" name="code">
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-lg-3 col-form-label">Type</label>
                        <div class="col-lg-9">
                            <select name="type_id" id="type_id" class="form-control">
                                <option value="">Select Type</option>
                                <option value="1">Type 1</option>

                                <option value="2">Type 2</option>

                                <option value="3">Type 3</option>

                             </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-lg-3 col-form-label">Mobile</label>
                        <div class="col-lg-9">
                            <input type="text" placeholder="Mobile" id="mobile" class="form-control" name="mobile">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-lg-3 col-form-label">Email</label>
                        <div class="col-lg-9">
                            <input type="email" placeholder="Email" id="email" class="form-control" name="email">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-lg-3 col-form-label">Address</label>
                        <div class="col-lg-9">
                            <textarea class="form-control" id="address" name="address"></textarea>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-lg-3 col-form-label">Add User</label>
                        <div class="col-lg-9">
                                 <select name="user_id" id="user_id" class="form-control input-lg">

                                  <option value="">Select USER</option>
                               

                               </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-lg-3 col-form-label">Status</label>

                        <div class="col-lg-9">
                            <label class="i-checks"> <input type="checkbox" value="1" name="is_active" id="is_active" checked="1">&nbsp;Active </label>
                        </div>
                    </div>


                    <div class="form-group row">
                        <div class="col-lg-offset-3 col-lg-9">
                            <button class="btn btn-sm btn-primary btn-form-submit" type="submit">SAVE</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        
    </div>
</div>