<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Category</h2>
        <ol class="breadcrumb">
            <li class="breadcrumb-item">
                <a href="index.html">Home</a>
            </li>
            <li class="breadcrumb-item">
                <a>Category</a>
            </li>
            <li class="breadcrumb-item active">
                <strong class="form-title">Category Create</strong>
            </li>
        </ol>
    </div>
    <div class="col-lg-2">
        <div class="title-action">
            <a href="<?php echo base_url(); ?>category" 
                class="btn btn-primary">Category List</a>
        </div>
    </div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-6">
            <div class="ibox ">
                <div class="ibox-title">
                    <h5 class="form-title">Category Create</h5>
                    <div class="ibox-tools">
                        <a class="collapse-link">
                            <i class="fa fa-chevron-up"></i>
                        </a>
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                            <i class="fa fa-wrench"></i>
                        </a>
                        <ul class="dropdown-menu dropdown-user">
                            <li><a href="#" class="dropdown-item">Config option 1</a>
                            </li>
                            <li><a href="#" class="dropdown-item">Config option 2</a>
                            </li>
                        </ul>
                        <a class="close-link">
                            <i class="fa fa-times"></i>
                        </a>
                    </div>
                </div>
                <div class="ibox-content">
                    <form id="form-category" method="POST">
                        
                        <div class="form-group row">
                            <label class="col-lg-3 col-form-label">Name</label>
                            <div class="col-lg-9">
                                 <input type="hidden" class="form-control" id="id" name="id">
                                <input type="text" placeholder="Name" id="name" class="form-control" name="name">
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-lg-3 col-form-label">Code</label>
                            <div class="col-lg-9">
                                <input type="text" placeholder="Code" id="code" class="form-control" name="code">
                            </div>
                        </div>

                         <div class="form-group row">
                            <label class="col-lg-3 col-form-label">Pid
                            </label>
                            <div class="col-lg-9">
                                <select name="pid" id="pid" class="form-control">
                                    <option value="">Select category</option>
                                </select>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-lg-3 col-form-label">Sort Order</label>
                            <div class="col-lg-9">
                                <input type="text" placeholder="Sort Order" 
                                id="sort_order" class="form-control" name="sort_order">
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-lg-3 col-form-label">Prefix
                            </label>
                            <div class="col-lg-9">
                                <input type="text" placeholder="Prefix" 
                                id="prefix" class="form-control" name="prefix">
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-lg-3 col-form-label">Type
                            </label>
                            <div class="col-lg-9">
                                <select name="type_id" 
                                id="type_id" class="form-control">
                                    <option value="">Select Type</option>
                                    <option value="1">Type 1</option>
                                    <option value="2">Type 2</option>
                                    <option value="3">Type 3</option>
                                    <option value="4">Type 4</option>
                                </select>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-lg-3 col-form-label">Status
                            </label>
                            <div class="col-lg-9">
                                <label class="i-checks"> <input type="checkbox" value="1" name="is_active" id="is_active" checked="1">&nbsp;Active </label>
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-lg-offset-3 col-lg-9">
                                <button class="btn btn-sm btn-primary btn-form-submit" type="submit">SAVE</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>