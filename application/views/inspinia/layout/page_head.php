<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title><?php echo config_item('meta_title'); ?></title>

    <link href="<?php echo base_url(); ?>public/inspinia/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>public/inspinia/font-awesome/css/font-awesome.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>public/inspinia/css/plugins/toastr/toastr.min.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>public/inspinia/css/plugins/chosen/bootstrap-chosen.css" rel="stylesheet">

    <link href="<?php echo base_url(); ?>public/inspinia/css/plugins/iCheck/custom.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>public/inspinia/css/plugins/datapicker/datepicker3.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>public/inspinia/css/plugins/dataTables/datatables.min.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>public/inspinia/css/plugins/jasny/jasny-bootstrap.min.css" rel="stylesheet">
    <!-- <link href="<?php echo base_url(); ?>public/inspinia/css/plugins/summernote/summernote-bs4.css" rel="stylesheet"> -->

    <?php echo put_lib_css(); ?>
    
    <link href="<?php echo base_url(); ?>public/inspinia/css/animate.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>public/inspinia/css/style.css" rel="stylesheet">

    <!-- Admin Custom CSS -->
    <link href="<?php echo base_url(); ?>public/css/admin/admin.css" rel="stylesheet">

    <?php echo put_css(); ?>
    
</head>

<body class="">