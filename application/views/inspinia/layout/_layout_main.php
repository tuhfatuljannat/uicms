<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<?php $this->load->view('inspinia/layout/page_head');  ?>

    <div id="wrapper">
	    <nav class="navbar-default navbar-static-side" role="navigation">
	        <div class="sidebar-collapse">
	            <ul class="nav metismenu" id="side-menu">
	                <li class="nav-header">
	                    <div class="dropdown profile-element">
	                        <img alt="image" class="rounded-circle" style="width: 48px; height: 48px;" id="upic" />
	                        <a data-toggle="dropdown" class="dropdown-toggle" href="#">
	                            <span class="block m-t-xs font-bold uid" style="visibility: hidden;"><?php echo $this->session->userdata('id'); ?></span>
	                            <span class="block m-t-xs font-bold uname"><?php //echo $this->session->userdata('name'); ?></span>
	                            <span class="text-muted text-xs block uemail"><?php //echo $this->session->userdata('email'); ?><b class="caret"></b></span>
	                        </a>
	                        <ul class="dropdown-menu animated fadeInRight m-t-xs">
	                            <li><a class="dropdown-item" href="<?php echo base_url();?>user/profile/<?php echo $this->session->userdata('id'); ?> ">Profile</a></li>
	                            <li><a class="dropdown-item" href="contacts.html">Contacts</a></li>
	                            <li><a class="dropdown-item" href="mailbox.html">Mailbox</a></li>
	                            <li class="dropdown-divider"></li>
	                            <li><a class="dropdown-item" href="<?php echo base_url(); ?>login/logout">Logout</a></li>
	                        </ul>
	                    </div>
	                    <div class="logo-element">
	                        IN+
	                    </div>
	                </li>
	                <li>
	                    <a href="<?php echo base_url(); ?>inspinia/index"><i class="fa fa-th-large"></i> <span class="nav-label">Dashboard</span></a>
	                </li>
	               <!--  <li>
	                    <a href="#"><i class="fa fa-edit"></i> <span class="nav-label">Demo </span><span class="fa arrow"></span></a>
	                    <ul class="nav nav-second-level collapse">
	                    	
                            <li>
			                    <a href="<?php echo base_url(); ?>inspinia/table"><i class="fa fa-magic"></i> <span class="nav-label">Table</span></a>
			                </li>
			                <li>
			                    <a href="<?php echo base_url(); ?>inspinia/form"><i class="fa fa-magic"></i> <span class="nav-label">Form</span></a>
			                </li>
			                <li>
			                    <a href="<?php echo base_url(); ?>inspinia/user"><i class="fa fa-magic"></i> <span class="nav-label">Users</span></a>
			                </li>
	                        
	                    </ul>
	                </li> -->
	                <li>
	                    <a href="#"><i class="fa fa-edit"></i> <span class="nav-label">UI-Slider</span><span class="fa arrow"></span></a>
	                    <ul class="nav nav-second-level collapse">
	                    	
                            <li>
			                    <a href="<?php echo base_url(); ?>slides"><i class="fa fa-magic"></i> <span class="nav-label">Slide</span></a>
			                </li>
			               
	                        
	                    </ul>
	                </li>
	                <li>
	                    <a href="#"><i class="fa fa-edit"></i> <span class="nav-label">Ui-About Us</span><span class="fa arrow"></span></a>
	                    <ul class="nav nav-second-level collapse">
	                    	
                            <li>
			                    <a href="<?php echo base_url(); ?>aboutus"><i class="fa fa-magic"></i> <span class="nav-label">About Us</span></a>
			                </li>
	                    </ul>
	                </li>
	                <li>
	                    <a href="#"><i class="fa fa-edit"></i> <span class="nav-label">Ui-Services</span><span class="fa arrow"></span></a>
	                    <ul class="nav nav-second-level collapse">
	                    	
                        	<li>
			                    <a href="<?php echo base_url(); ?>services"><i class="fa fa-magic"></i> <span class="nav-label">Services</span></a>
			                </li>   
	                        
	                    </ul>
	                </li>
	                <li>
	                    <a href="#"><i class="fa fa-edit"></i> <span class="nav-label">Ui-Skills </span><span class="fa arrow"></span></a>
	                    <ul class="nav nav-second-level collapse">
	                    	
                               <li>
			                    <a href="<?php echo base_url(); ?>skills"><i class="fa fa-magic"></i> <span class="nav-label">Skills</span></a>
			                </li>
	                        
	                    </ul>
	                </li>
	                <li>
	                    <a href="#"><i class="fa fa-edit"></i> <span class="nav-label">Ui-Portfolio </span><span class="fa arrow"></span></a>
	                    <ul class="nav nav-second-level collapse">
	                    	
                            <li>
			                    <a href="<?php echo base_url(); ?>portfolio"><i class="fa fa-magic"></i> <span class="nav-label">Portfolio</span></a>
			                </li>
	                        
	                    </ul>
	                </li>
	                <li>
	                    <a href="#"><i class="fa fa-edit"></i> <span class="nav-label">Ui-Clients </span><span class="fa arrow"></span></a>
	                    <ul class="nav nav-second-level collapse">
	                    	
                            <li>
			                    <a href="<?php echo base_url(); ?>clients"><i class="fa fa-magic"></i> <span class="nav-label">Clients</span></a>
			                </li>
	                        
	                    </ul>
	                </li>
	                <li>
	                    <a href="#"><i class="fa fa-edit"></i> <span class="nav-label">Ui-Team </span><span class="fa arrow"></span></a>
	                    <ul class="nav nav-second-level collapse">
	                    	
                            <li>
			                    <a href="<?php echo base_url(); ?>team"><i class="fa fa-magic"></i> <span class="nav-label">Team</span></a>
			                </li>
	                        
	                    </ul>
	                </li>
	                <li>
	                    <a href="#"><i class="fa fa-edit"></i> <span class="nav-label">Ui-Settings</span><span class="fa arrow"></span></a>
	                    <ul class="nav nav-second-level collapse">
	                    	
                            <li>
			                    <a href="<?php echo base_url(); ?>settings"><i class="fa fa-magic"></i> <span class="nav-label">Settings</span></a>
			                </li>
			              
	                        
	                    </ul>
	                </li>
	                <li>
	                    <a href="#"><i class="fa fa-edit"></i> <span class="nav-label">Ui-Contactus</span><span class="fa arrow"></span></a>
	                    <ul class="nav nav-second-level collapse">
	                    	
                            <li>
			                    <a href="<?php echo base_url(); ?>contactus"><i class="fa fa-magic"></i> <span class="nav-label">Contact Us</span></a>
			                </li>
			             
	                        
	                    </ul>
	                </li>
	               
	            </ul>
	        </div>
	    </nav>

        <div id="page-wrapper" class="gray-bg">
	        <div class="row border-bottom">
		        <nav class="navbar navbar-static-top  " role="navigation" style="margin-bottom: 0">
			        <div class="navbar-header">
			            <a class="navbar-minimalize minimalize-styl-2 btn btn-primary " href="#"><i class="fa fa-bars"></i> </a>
			            <form role="search" class="navbar-form-custom" action="search_results.html">
			                <div class="form-group">
			                    <input type="text" placeholder="Search for something..." class="form-control" name="top-search" id="top-search">
			                </div>
			            </form>
			        </div>
		            <ul class="nav navbar-top-links navbar-right">
		                <li>
		                    <span class="m-r-sm text-muted welcome-message">Welcome to AIA-WMS Admin Paenl.</span>
		                </li>
		                <li class="dropdown">
		                    <a class="dropdown-toggle count-info" data-toggle="dropdown" href="#">
		                        <i class="fa fa-envelope"></i>  <span class="label label-warning">16</span>
		                    </a>
		                    <ul class="dropdown-menu dropdown-messages">
		                        <li>
		                            <div class="dropdown-messages-box">
		                                <a class="dropdown-item float-left" href="profile.html">
		                                    <img alt="image" class="rounded-circle" src="<?php echo base_url(); ?>public/inspinia/img/a7.jpg">
		                                </a>
		                                <div class="media-body">
		                                    <small class="float-right">46h ago</small>
		                                    <strong>Mike Loreipsum</strong> started following <strong>Monica Smith</strong>. <br>
		                                    <small class="text-muted">3 days ago at 7:58 pm - 10.06.2014</small>
		                                </div>
		                            </div>
		                        </li>
		                        <li class="dropdown-divider"></li>
		                        <li>
		                            <div class="dropdown-messages-box">
		                                <a class="dropdown-item float-left" href="profile.html">
		                                    <img alt="image" class="rounded-circle" src="<?php echo base_url(); ?>public/inspinia/img/a4.jpg">
		                                </a>
		                                <div class="media-body ">
		                                    <small class="float-right text-navy">5h ago</small>
		                                    <strong>Chris Johnatan Overtunk</strong> started following <strong>Monica Smith</strong>. <br>
		                                    <small class="text-muted">Yesterday 1:21 pm - 11.06.2014</small>
		                                </div>
		                            </div>
		                        </li>
		                        <li class="dropdown-divider"></li>
		                        <li>
		                            <div class="dropdown-messages-box">
		                                <a class="dropdown-item float-left" href="profile.html">
		                                    <img alt="image" class="rounded-circle" src="<?php echo base_url(); ?>public/inspinia/img/profile.jpg">
		                                </a>
		                                <div class="media-body ">
		                                    <small class="float-right">23h ago</small>
		                                    <strong>Monica Smith</strong> love <strong>Kim Smith</strong>. <br>
		                                    <small class="text-muted">2 days ago at 2:30 am - 11.06.2014</small>
		                                </div>
		                            </div>
		                        </li>
		                        <li class="dropdown-divider"></li>
		                        <li>
		                            <div class="text-center link-block">
		                                <a href="mailbox.html" class="dropdown-item">
		                                    <i class="fa fa-envelope"></i> <strong>Read All Messages</strong>
		                                </a>
		                            </div>
		                        </li>
		                    </ul>
		                </li>
		                <li class="dropdown">
		                    <a class="dropdown-toggle count-info" data-toggle="dropdown" href="#">
		                        <i class="fa fa-bell"></i>  <span class="label label-primary">8</span>
		                    </a>
		                    <ul class="dropdown-menu dropdown-alerts">
		                        <li>
		                            <a href="mailbox.html" class="dropdown-item">
		                                <div>
		                                    <i class="fa fa-envelope fa-fw"></i> You have 16 messages
		                                    <span class="float-right text-muted small">4 minutes ago</span>
		                                </div>
		                            </a>
		                        </li>
		                        <li class="dropdown-divider"></li>
		                        <li>
		                            <a href="profile.html" class="dropdown-item">
		                                <div>
		                                    <i class="fa fa-twitter fa-fw"></i> 3 New Followers
		                                    <span class="float-right text-muted small">12 minutes ago</span>
		                                </div>
		                            </a>
		                        </li>
		                        <li class="dropdown-divider"></li>
		                        <li>
		                            <a href="grid_options.html" class="dropdown-item">
		                                <div>
		                                    <i class="fa fa-upload fa-fw"></i> Server Rebooted
		                                    <span class="float-right text-muted small">4 minutes ago</span>
		                                </div>
		                            </a>
		                        </li>
		                        <li class="dropdown-divider"></li>
		                        <li>
		                            <div class="text-center link-block">
		                                <a href="notifications.html" class="dropdown-item">
		                                    <strong>See All Alerts</strong>
		                                    <i class="fa fa-angle-right"></i>
		                                </a>
		                            </div>
		                        </li>
		                    </ul>
		                </li>
		                <li>
		                    <a href="<?php echo base_url(); ?>login/logout">
		                        <i class="fa fa-sign-out"></i> Log out
		                    </a>
		                </li>
		            </ul>
		        </nav>
	        </div>
            
            <!-- Subview -->
            <?php

            	// Sub view is set in controller
		        if (!isset($subview)) {
		            $subview = '';
		        }
		        
		        if ( ($subview != '') ) {
		            $this->load->view($subview);
		        }
		    ?>
            <!-- /Subview -->

            <div class="footer">
            	<?php echo config_item('inspinia_footer_content'); ?>
            </div>
        </div>
    </div>

<?php $this->load->view('inspinia/layout/page_footer');  ?>