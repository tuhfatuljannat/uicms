<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
	<!-- Mainly scripts -->
    <script src="<?php echo base_url(); ?>public/inspinia/js/jquery-3.1.1.min.js"></script>
    <script src="<?php echo base_url(); ?>public/inspinia/js/popper.min.js"></script>
    <script src="<?php echo base_url(); ?>public/inspinia/js/bootstrap.js"></script>
    <script src="<?php echo base_url(); ?>public/inspinia/js/plugins/metisMenu/jquery.metisMenu.js"></script>
    <script src="<?php echo base_url(); ?>public/inspinia/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>
    <script src="<?php echo base_url(); ?>public/inspinia/js/plugins/toastr/toastr.min.js"></script>
    <script src="<?php echo base_url(); ?>public/inspinia/js/plugins/chosen/chosen.jquery.js"></script>

    <script src="<?php echo base_url(); ?>public/inspinia/js/plugins/iCheck/icheck.min.js"></script>
    <script src="<?php echo base_url(); ?>public/inspinia/js/plugins/datapicker/bootstrap-datepicker.js"></script>
    <script src="<?php echo base_url(); ?>public/inspinia/js/plugins/dataTables/datatables.min.js"></script>
    <script src="<?php echo base_url(); ?>public/inspinia/js/plugins/dataTables/dataTables.bootstrap4.min.js"></script>
    <script src="<?php echo base_url(); ?>public/inspinia/js/plugins/validate/jquery.validate.min.js"></script>
    <script src="<?php echo base_url(); ?>public/inspinia/js/plugins/jasny/jasny-bootstrap.min.js"></script>

    <?php echo put_lib_js(); ?>

    <!-- Custom and plugin javascript -->
    <script src="<?php echo base_url(); ?>public/inspinia/js/inspinia.js"></script>
    <script src="<?php echo base_url(); ?>public/inspinia/js/plugins/pace/pace.min.js"></script>
   <!--  <script src="<?php echo base_url(); ?>public/inspinia/js/plugins/summernote/summernote-bs4.js"></script>
      <script>
        $(document).ready(function(){

            $('.summernote').summernote();

       });
    </script> -->
    <!-- Admin Custom JS -->
    <script src="<?php echo base_url(); ?>public/js/common/function.js"></script>
    <script src="<?php echo base_url(); ?>public/inspinia/js/custom.js"></script>
    

    <?php echo put_js(); ?>

</body>

</html>