<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

	<div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2>User</h2>
            <ol class="breadcrumb">
                <li class="breadcrumb-item">
                    <a href="index.html">Home</a>
                </li>
                <li class="breadcrumb-item">
                    <a>Inspinia</a>
                </li>
                <li class="breadcrumb-item active">
                    <strong>User</strong>
                </li>
            </ol>
        </div>
        <div class="col-lg-2">

        </div>
    </div>
    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
	        <div class="col-lg-4">
	            <div class="ibox ">
                    <div class="ibox-title">
                        <h5 class="form-title">User Entry</h5>
                        <div class="ibox-tools">
                            <a class="collapse-link">
                                <i class="fa fa-chevron-up"></i>
                            </a>
                            <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                                <i class="fa fa-wrench"></i>
                            </a>
                            <ul class="dropdown-menu dropdown-user">
                                <li><a href="#" class="dropdown-item">Config option 1</a>
                                </li>
                                <li><a href="#" class="dropdown-item">Config option 2</a>
                                </li>
                            </ul>
                            <a class="close-link">
                                <i class="fa fa-times"></i>
                            </a>
                        </div>
                    </div>
                    <div class="ibox-content">
                        <form id="form-user">
                            <!-- <p>Sign in today for more expirience.</p> -->
                            <div class="form-group row">
                            	<label class="col-lg-2 col-form-label">Name</label>
                                <div class="col-lg-10">
                                	<input type="hidden" class="form-control" id="id" name="id">
                                	<input type="text" placeholder="Name" id="name" class="form-control" name="name">
                                </div>
                            </div>
                            <div class="form-group row">
                            	<label class="col-lg-2 col-form-label">Mobile</label>
                                <div class="col-lg-10">
                                	<input type="text" placeholder="Mobile" id="mobile" class="form-control" name="mobile">
                                </div>
                            </div>
                            <div class="form-group row">
                            	<label class="col-lg-2 col-form-label">Email</label>
                                <div class="col-lg-10">
                                	<input type="email" placeholder="Email" id="email" class="form-control" name="email"> 
                                	<!-- <span class="form-text m-b-none">Example block-level help text here.</span> -->
                                </div>
                            </div>
                            <!-- <div class="form-group row">
                            	<label class="col-lg-2 col-form-label">Password</label>
                                <div class="col-lg-10">
                                	<input type="password" placeholder="Password" class="form-control">
                                </div>
                            </div> -->
                            <div class="form-group row">
                            	<label class="col-lg-2 col-form-label">Address</label>
                                <div class="col-lg-10">
                                	<!-- <input type="password" placeholder="Password" class="form-control"> -->
                                	<textarea class="form-control" id="address" name="address"></textarea>
                                </div>
                            </div>
                            <div class="form-group row">
                            	<label class="col-lg-2 col-form-label" id="status">Status</label>
                                <div class="col-lg-10">
                                	<div class="i-checks"><label> <input type="checkbox" name="remember" checked><i></i> Remember me </label></div>
                                </div>
                            </div>
                            <!-- <div class="form-group row">
                                <div class="col-lg-offset-2 col-lg-10">
                                    <div class="i-checks"><label> <input type="checkbox"><i></i> Remember me </label></div>
                                </div>
                            </div> -->
                            <div class="form-group row">
                                <div class="col-lg-offset-2 col-lg-10">
                                    <button class="btn btn-sm btn-primary btn-form-submit" type="submit">SAVE</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
	        </div>
	        <div class="col-lg-8">
                <div class="ibox ">
                    <div class="ibox-title">
                        <h5>User List  </h5>
                        <div class="ibox-tools">
                            <a class="collapse-link">
                                <i class="fa fa-chevron-up"></i>
                            </a>
                            <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                                <i class="fa fa-wrench"></i>
                            </a>
                            <ul class="dropdown-menu dropdown-user">
                                <li><a href="#" class="dropdown-item">Config option 1</a>
                                </li>
                                <li><a href="#" class="dropdown-item">Config option 2</a>
                                </li>
                            </ul>
                            <a class="close-link">
                                <i class="fa fa-times"></i>
                            </a>
                        </div>
                    </div>
                    <div class="ibox-content">

                        <table class="table table-hover tbl-user">
                            <thead>
	                            <tr>
	                                <th>#</th>
	                                <th>Name</th>
	                                <th>Mobile</th>
	                                <th>Email</th>
	                                <th>Address</th>
	                                <th>Action</th>
	                            </tr>
                            </thead>
                            <tbody>
                            	<!--  
	                            <tr>
	                                <td>1</td>
	                                <td>Mr. Rahat</td>
	                                <td>019877665</td>
	                                <td>rahat@gmail.com</td>
	                                <td>Lalkhan Bazar, Ctg</td>
	                                <td>
	                                	<a href="http://localhost:85/ci-repo/inspinia/user/1" class="btn-edit" data-id="1"><i class="fa fa-edit"></i></a>
	                                	&nbsp;
	                                	<a href="http://localhost:85/ci-repo/inspinia/user/1" class="btn-edit" data-id="1"><i class="fa fa-remove"></i></a>
	                                </td>
	                            </tr>
	                            -->
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>