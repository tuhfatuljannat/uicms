<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Settings</h2>
        <ol class="breadcrumb">
            <li class="breadcrumb-item">
                <a href="index.html">Home</a>
            </li>
            <li class="breadcrumb-item">
                <a>Settings</a>
            </li>
            <li class="breadcrumb-item active">
                <strong class="form-title">Settings Create</strong>
            </li>
        </ol>
    </div>
    <div class="col-lg-2">
<!-- <div class="title-action">
<a href="<?php echo base_url(); ?>settings" class="btn btn-primary">Team List</a>
</div> -->
</div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox ">
                <div class="ibox-title">
                    <h5 class="form-title">Settings Create</h5>
                    <div class="ibox-tools">
                        <a class="collapse-link">
                            <i class="fa fa-chevron-up"></i>
                        </a>
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                            <i class="fa fa-wrench"></i>
                        </a>
                        <ul class="dropdown-menu dropdown-user">
                            <li><a href="#" class="dropdown-item">Config option 1</a>
                            </li>
                            <li><a href="#" class="dropdown-item">Config option 2</a>
                            </li>
                        </ul>
                        <a class="close-link">
                            <i class="fa fa-times"></i>
                        </a>
                    </div>
                </div>
                <div class="ibox-content">
                    <form id="form-settings" method="POST" enctype="multipart/form-data">
                        <!-- <p>Sign in today for more expirience.</p> -->
                        <div class="form-group row">
                            <div class="col-lg-3"></div>
                            <label class="col-lg-2 col-form-label"> Company Name</label>
                            <div class="col-lg-5">
                                <input type="hidden" class="form-control" id="id" name="id">
                                <input type="text" placeholder="Name" id="company_name" class="form-control" name="company_name">
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-lg-3"></div>
                            <label class="col-lg-2 col-form-label">Company Description</label>
                            <div class="col-lg-5">
                                <textarea class="form-control" id="company_desc" name="company_desc"></textarea>
                                <!-- <input type="text" placeholder="" id="company_desc" class="form-control" name="company_desc"> -->
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-lg-3"></div>
                            <label class="col-lg-2 col-form-label">Logo</label>
                            <div class="col-lg-5">
                                <div class="fileinput fileinput-new" data-provides="fileinput">
                                    <div>
                                        <span class="btn btn-default btn-file"><span class="fileinput-new"><span class="fa fa-camera fa-2x"></span> <!-- File --></span>
                                        <div id="preview" name="preview"  class="fileinput-preview fileinput-exists thumbnail" style="width: 150px; height: 100px; display:block;"></div>
                                        <img id="prevImg" name="prevImg" />
                                        <input type="file" name="file" id="file" class="ephoto-upload" accept="image/jpeg">
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-lg-3"></div>
                        <label class="col-lg-2 col-form-label">Company Address</label>
                        <div class="col-lg-5">
                            <input type="text" placeholder="" id="company_addr" class="form-control" name="company_addr">
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-lg-3"></div>
                        <label class="col-lg-2 col-form-label">Company Mobile</label>
                        <div class="col-lg-5">
                            <input type="text" placeholder="" id="company_mobile" class="form-control" name="company_mobile">
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-lg-3"></div>
                        <label class="col-lg-2 col-form-label">Company Email</label>
                        <div class="col-lg-5">
                            <input type="email" placeholder="" id="company_email" class="form-control" name="company_email">
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-lg-3"></div>
                        <label class="col-lg-2 col-form-label">About Introduction</label>
                        <div class="col-lg-5">
                            <textarea class="form-control" id="about_intro" name="about_intro"></textarea>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-lg-3"></div>
                        <label class="col-lg-2 col-form-label">Service Introduction</label>
                        <div class="col-lg-5">
                            <textarea class="form-control" id="service_intro" name="service_intro"></textarea>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-lg-3"></div>
                        <label class="col-lg-2 col-form-label">Skills Introduction</label>
                        <div class="col-lg-5">
                            <textarea class="form-control" id="skill_intro" name="skill_intro"></textarea>
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-lg-3"></div>
                        <label class="col-lg-2 col-form-label">Team Introduction</label>
                        <div class="col-lg-5">
                            <textarea class="form-control" id="team_intro" name="team_intro"></textarea>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-lg-3"></div>
                        <label class="col-lg-2 col-form-label">Contact Introduction</label>
                        <div class="col-lg-5">
                            <textarea class="form-control" id="contact_intro" name="contact_intro"></textarea>
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-lg-3"></div>
                        <label class="col-lg-2 col-form-label">Link Fb</label>
                        <div class="col-lg-5">
                            <input type="text" placeholder="" id="fb" class="form-control" name="fb">
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-lg-3"></div>
                        <label class="col-lg-2 col-form-label">Link Twt</label>
                        <div class="col-lg-5">
                            <input type="text" placeholder="" id="twt" class="form-control" name="twt">
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-lg-3"></div>
                        <label class="col-lg-2 col-form-label">Link Instagram</label>
                        <div class="col-lg-5">
                            <input type="text" placeholder="" id="inst" class="form-control" name="inst">
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-lg-3"></div>
                        <label class="col-lg-2 col-form-label">Link Gp</label>
                        <div class="col-lg-5">
                            <input type="text" placeholder="" id="gplus" class="form-control" name="gplus">
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-lg-3"></div>
                        <label class="col-lg-2 col-form-label">Link In</label>
                        <div class="col-lg-5">
                            <input type="text" placeholder="" id="link_in" class="form-control" name="link_in">
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-lg-8"></div>
                        <div class="col-lg-2">
                            <button class="btn btn-sm btn-default btn-form-reset" type="submit">RESET</button>
                            <button class="btn btn-sm btn-primary btn-form-submit pull-right" type="submit">UPDATE</button>

                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
</div>