

<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<div class="row wrapper border-bottom white-bg page-heading">
	<div class="col-lg-10">
		<h2>Type</h2>
		<ol class="breadcrumb">
			<li class="breadcrumb-item">
				<a href="index.html">Home</a>
			</li>
			<li class="breadcrumb-item">
				<a>Type</a>
			</li>
			<li class="breadcrumb-item active">
				<strong>Index</strong>
			</li>
		</ol>
	</div>
	<div class="col-lg-3">

	</div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">
	<div class="row">
		<div class="col-lg-4">
			<div class="ibox ">
				<div class="ibox-title">
					<h5 class="form-title">Type Entry</h5>
					<div class="ibox-tools">
						<a class="collapse-link">
							<i class="fa fa-chevron-up"></i>
						</a>
						<a class="dropdown-toggle" data-toggle="dropdown" href="#">
							<i class="fa fa-wrench"></i>
						</a>
						<ul class="dropdown-menu dropdown-Type">
							<li><a href="#" class="dropdown-item">Config option 1</a>
							</li>
							<li><a href="#" class="dropdown-item">Config option 2</a>
							</li>
						</ul>
						<a class="close-link">
							<i class="fa fa-times"></i>
						</a>
					</div>
				</div>
				<div class="ibox-content">
					<form id="form-type" method="post">
						<!-- <p>Sign in today for more expirience.</p> -->
						<div class="form-group row">
							<label class="col-lg-3 col-form-label">Name</label>
							<div class="col-lg-9">
								<input type="hidden" class="form-control" id="id" name="id">
								<input type="text" placeholder="Name" id="name" class="form-control" name="name">
							</div>
						</div>
						<div class="form-group row">
							<label class="col-lg-3 col-form-label">Code</label>
							<div class="col-lg-9">
								<input type="text" placeholder="Code" id="code" class="form-control" name="code">
							</div>
						</div>
						<div class="form-group row">
							<label class="col-lg-3 col-form-label">Declared for</label>
							<div class="col-lg-9">
								<select name="declared_for" id="declared_for" class="form-control" >
									<option>---Select---</option>
									<option value="1">Unit</option>
									<option value="2">Warehouse</option>
									<option value="3">Product</option>
									<option value="4">Catagory</option>
									<option value="5">Service</option>
									<option value="6">Parts</option>
									<option value="7">Parent</option>
								</select>
							</div>
						</div>
						<div class="form-group row">
							<label class="col-lg-3 col-form-label" id="status">Status</label>
							<div class="col-lg-9">
								<div class="i-checks"><label> <input type="checkbox" id="is_active" name="is_active" checked></label></div>
							</div>
						</div>

						<div class="form-group row">
							<div class="col-lg-12">
								<button class="btn btn-sm btn-primary btn-form-submit pull-right" type="submit">SAVE</button>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
		<div class="col-lg-8">
			<div class="ibox ">
				<div class="ibox-title">
					<h5>Type List  </h5>
					<div class="ibox-tools">
						<a class="collapse-link">
							<i class="fa fa-chevron-up"></i>
						</a>
						<a class="dropdown-toggle" data-toggle="dropdown" href="#">
							<i class="fa fa-wrench"></i>
						</a>
						<ul class="dropdown-menu dropdown-Type">
							<li><a href="#" class="dropdown-item">Config option 1</a>
							</li>
							<li><a href="#" class="dropdown-item">Config option 2</a>
							</li>
						</ul>
						<a class="close-link">
							<i class="fa fa-times"></i>
						</a>
					</div>
				</div>
				<div class="ibox-content">

					<table class="table table-hover dataTables-type">
						<thead>
							<tr>
								<th>#</th>
								<th>Name</th>
								<th>Code</th>
								<th>Declared For</th>
								<th>Active Status</th>
								<th>Action</th>
							</tr>
						</thead>
						<tbody>

						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>