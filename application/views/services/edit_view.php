<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Services</h2>
        <ol class="breadcrumb">
            <li class="breadcrumb-item">
                <a href="index.html">Home</a>
            </li>
            <li class="breadcrumb-item">
                <a>Services</a>
            </li>
            <li class="breadcrumb-item active">
                <strong class="form-title">Services Create</strong>
            </li>
        </ol>
    </div>
    <div class="col-lg-2">
        <div class="title-action">
            <a href="<?php echo base_url(); ?>services" class="btn btn-primary">Service List</a>
        </div>
    </div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox ">
                <div class="ibox-title">
                    <h5 class="form-title">Services Create</h5>
                    <div class="ibox-tools">
                        <a class="collapse-link">
                            <i class="fa fa-chevron-up"></i>
                        </a>
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                            <i class="fa fa-wrench"></i>
                        </a>
                        <ul class="dropdown-menu dropdown-user">
                            <li><a href="#" class="dropdown-item">Config option 1</a>
                            </li>
                            <li><a href="#" class="dropdown-item">Config option 2</a>
                            </li>
                        </ul>
                        <a class="close-link">
                            <i class="fa fa-times"></i>
                        </a>
                    </div>
                </div>
                <div class="ibox-content">
                    <form id="form-services" method="POST">
                    <div class="form-group row">
                         <div class="col-lg-2"></div>
                        <label class="col-lg-2 col-form-label">Title</label>
                        <div class="col-lg-6">
                            <input type="hidden" class="form-control" id="id" name="id">
                            <input type="text" placeholder="Title" id="title" class="form-control" name="title">
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-lg-2"></div>
                        <label class="col-lg-2 col-form-label">Description</label>
                        <div class="col-lg-6">
                            <input type="text" placeholder="Description" id="description" class="form-control" name="description">
                        </div>
                    </div>
                    <div class="form-group row">
                         <div class="col-lg-2"></div>
                        <label class="col-lg-2 col-form-label">Icon</label>
                        <div class="col-lg-6">
                            <input type="text" placeholder="Icon" id="icon" class="form-control" name="icon">
                        </div>
                    </div>

                    <div class="form-group row">
                         <div class="col-lg-2"></div>
                        <label class="col-lg-2 col-form-label" id="status">Status</label>
                        <div class="col-lg-6">
                            <div class="i-checks"><label> <input type="checkbox" id="is_active" name="is_active" checked></label></div>
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-lg-8"></div>
                            <div class="col-lg-4">
                                <button class="btn btn-sm btn-default btn-form-reset" type="submit">RESET</button>
                                <button class="btn btn-sm btn-primary btn-form-submit" type="submit">SAVE</button>
                            </div>
                    </div>
                </form> 
                </div>
            </div>
        </div>
    </div>
</div>



