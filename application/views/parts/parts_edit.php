<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Parts</h2>
        <ol class="breadcrumb">
            <li class="breadcrumb-item">
                <a href="<?php echo base_url(); ?>dashboard">Home</a>
            </li>
            <li class="breadcrumb-item">
                <a>Parts</a>
            </li>
            <li class="breadcrumb-item active">
                <strong class="form-title">Parts Entry</strong>
            </li>
        </ol>
    </div>
    <div class="col-lg-2">
        <div class="title-action">
            <a href="<?php echo base_url(); ?>customers" class="btn btn-primary"> <i class="fa fa-list"></i> &nbsp;Parts List</a>
        </div>
    </div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox">
                <div class="ibox-title">
                    <h5>Parts Entry</h5>
                    <div class="ibox-tools">
                        <a class="collapse-link">
                            <i class="fa fa-chevron-up"></i>
                        </a>
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                            <i class="fa fa-wrench"></i>
                        </a>
                        <ul class="dropdown-menu dropdown-user">
                            <li><a href="#" class="dropdown-item">Config option 1</a>
                            </li>
                            <li><a href="#" class="dropdown-item">Config option 2</a>
                            </li>
                        </ul>
                        <a class="close-link">
                            <i class="fa fa-times"></i>
                        </a>
                    </div>
                </div>
                <div class="ibox-content">
                    <!-- <h2>Parts Entry</h2> -->
                    <!-- <p>This example show how to use Steps with jQuery Validation plugin.</p> -->
                    <form id="form-parts" method="post">
                        <div class="row">
                            <div class="col-lg-8">
                                <div class="form-group  row">
                                    <label class="col-sm-2 col-form-label">Name</label>
                                    <div class="col-sm-9">
                                        <input type="hidden" class="form-control" id="id" name="id">
                                        <input type="text" placeholder="Name" id="name" class="form-control" name="name" required>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <div class="form-group  row">
                                    <label class="col-sm-4 col-form-label">Code</label>
                                    <div class="col-sm-7">
                                        <input type="text" placeholder="Code" id="code" class="form-control input-lg" name="code">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-4">
                                <div class="form-group  row">
                                    <label class="col-sm-4 col-form-label">Model</label>
                                    <div class="col-sm-7">
                                        <input type="text" placeholder="Model" id="model" class="form-control input-lg" name="model">
                                    </div>
                                    <!-- <div class="col-sm-3">
                                        <input type="text" placeholder="NUM" id="model" class="form-control input-lg" name="model">
                                    </div>
                                    <div class="col-sm-3">
                                        <input type="text" placeholder="CODE" id="model" class="form-control input-lg" name="model">
                                    </div> -->
                                </div>
                            </div>
                            <!-- <div class="col-lg-2"></div> -->
                            <div class="col-lg-4">
                                <div class="form-group  row">
                                    <label class="col-sm-4 col-form-label">Serial key</label>
                                    <div class="col-sm-7">
                                        <input type="text" placeholder="Serial key" id="serial_key" class="form-control input-lg" name="serial_key">
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <div class="form-group  row">
                                    <label class="col-sm-4 col-form-label">Is Buildin?</label>
                                    <div class="col-sm-7">
                                        <label class="i-checks"> <input type="checkbox" name="is_buildin" id="is_buildin" checked>&nbsp; </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-4">
                                <div class="form-group  row">
                                    <label class="col-sm-4 col-form-label">Parts Type</label>
                                    <div class="col-sm-7">
                                        <select name="type_id" id="type_id" class="form-control input-lg">
                                            <!-- <option value="">Select Type</option> -->
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <!-- <div class="col-lg-2"></div> -->
                            <div class="col-lg-4">
                                <div class="form-group  row">
                                    <label class="col-sm-4 col-form-label">Category</label>
                                    <div class="col-sm-7">
                                        <select name="category_id" id="category_id" class="form-control input-lg">
                                            <!-- <option value="">Select Type</option> -->
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <div class="form-group  row">
                                    <label class="col-sm-4 col-form-label">Sub Category</label>
                                    <div class="col-sm-7">
                                        <select name="sub_category_id" id="sub_category_id" class="form-control input-lg">
                                            <!-- <option value="">Select Type</option> -->
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-4">
                                <div class="form-group  row">
                                    <label class="col-sm-4 col-form-label">Brand</label>
                                    <div class="col-sm-7">
                                        <select name="brand_id" id="brand_id" class="form-control input-lg">
                                            <!-- <option value="">Select Type</option> -->
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <div class="form-group  row">
                                    <label class="col-sm-4 col-form-label">Unit</label>
                                    <div class="col-sm-7">
                                        <select name="unit_id" id="unit_id" class="form-control input-lg">
                                            <!-- <option value="">Select Type</option> -->
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <div class="form-group  row">
                                    <label class="col-sm-4 col-form-label">Buld Unit</label>
                                    <div class="col-sm-7">
                                        <select name="bulk_unit_id" id="bulk_unit_id" class="form-control input-lg">
                                            <!-- <option value="">Select Type</option> -->
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-8">
                                <div class="form-group  row">
                                    <label class="col-sm-2 col-form-label">Dimentions</label>
                                    <div class="col-sm-9">
                                        <div class="row">
                                            <div class="col-sm-2">
                                                <input type="text" placeholder="L" id="dimention_h" class="form-control input-lg" name="dimention_h" style="margin-bottom: 5px;">
                                            </div>
                                            <div class="col-sm-1"><div class="dimention-text">X</div></div>
                                            <div class="col-sm-2">
                                                <input type="text" placeholder="L" id="dimention_w" class="form-control input-lg" name="dimention_w" style="margin-bottom: 5px;">
                                            </div>
                                            <div class="col-sm-1"><div class="dimention-text">X</div></div>
                                            <div class="col-sm-2">
                                                <input type="text" placeholder="L" id="dimention_l" class="form-control input-lg" name="dimention_l" style="margin-bottom: 5px;">
                                            </div>
                                            <div class="col-sm-4">
                                                <div class="dimention-text">m [Unit]</div>
                                            </div>
                                        </div>
                                        <div class="row" style="margin-top: 10px;">
                                            <div class="col-sm-5">
                                                <input type="text" placeholder="dimention" id="dimention" class="form-control input-lg" name="dimention" style="margin-bottom: 5px;">
                                            </div>
                                            <div class="col-sm-3">
                                                <div class="dimention-text">ccm [Cubic Unit]</div>
                                            </div>
                                            <div class="col-sm-3">
                                                <select name="dimention" id="dimention" class="form-control input-lg">
                                                    <option value="">Unit</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <div class="form-group  row">
                                    <label class="col-sm-4 col-form-label">Weight</label>
                                    <div class="col-sm-7">
                                        <select name="weight_id" id="weight_id" class="form-control input-lg">
                                            <!-- <option value="">Select Type</option> -->
                                        </select>
                                    </div>
                                </div>
                            </div>
                            
                        </div>
                        <div class="row">
                            <div class="col-lg-8">
                                <div class="form-group  row">
                                    <label class="col-sm-2 col-form-label">Defaunt Packaging</label>
                                    <div class="col-sm-9">
                                        <div class="row">
                                            <div class="col-sm-2">
                                                <input type="text" placeholder="L" id="packaging_h" class="form-control input-lg" name="packaging_h" style="margin-bottom: 5px;">
                                            </div>
                                            <div class="col-sm-1"><div class="dimention-text">X</div></div>
                                            <div class="col-sm-2">
                                                <input type="text" placeholder="L" id="packaging_w" class="form-control input-lg" name="packaging_w" style="margin-bottom: 5px;">
                                            </div>
                                            <div class="col-sm-1"><div class="dimention-text">X</div></div>
                                            <div class="col-sm-2">
                                                <input type="text" placeholder="L" id="packaging_l" class="form-control input-lg" name="packaging_l" style="margin-bottom: 5px;">
                                            </div>
                                            <div class="col-sm-4">
                                                <div class="dimention-text">m [Unit]</div>
                                            </div>
                                        </div>
                                        <div class="row" style="margin-top: 10px;">
                                            <div class="col-sm-5">
                                                <input type="text" placeholder="packaging" id="packaging" class="form-control input-lg" name="packaging" style="margin-bottom: 5px;">
                                            </div>
                                            <div class="col-sm-3">
                                                <div class="dimention-text">ccm [Cubic Unit]</div>
                                            </div>
                                            <div class="col-sm-3">
                                                <select name="dimention" id="dimention" class="form-control input-lg">
                                                    <option value="">Unit</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <div class="form-group  row">
                                    <label class="col-sm-4 col-form-label">Gross Weight</label>
                                    <div class="col-sm-7">
                                        <select name="gross_weight_id" id="gross_weight_id" class="form-control input-lg">
                                            <!-- <option value="">Select Type</option> -->
                                        </select>
                                    </div>
                                </div>
                            </div>
                            
                        </div>
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="form-group  row">
                                    <label class="col-sm-3 col-form-label">Buildin Parts</label>
                                    <div class="col-sm-1">
                                        <label class="i-checks"> <input type="checkbox" name="chk_buildin_parts" id="chk_buildin_parts">&nbsp; </label>
                                    </div>
                                    <div class="col-sm-6 text-right box_buildin_parts" style="display: none">
                                        <input type="text" placeholder="Buildin Parts" id="buildin_parts" class="form-control input-lg" name="buildin_parts">

                                        <div class="add-custom-field text-right" style="margin-top: 10px; margin-bottom: 10px;">
                                            <input type="text" placeholder="Buildin Parts" id="buildin_parts" class="form-control input-lg" name="buildin_parts">
                                            <a href="#" class="btn-remove btn btn-danger btn-xs btn-outline"><i class="fa fa-remove"></i></a>
                                        </div>

                                        <a href="#" class="btn-add btn btn-primary btn-xs btn-outline"><i class="fa fa-plus">&nbsp;ADD MORE</i></a>
                                    </div>
                                </div>
                            </div>
                            <!-- <div class="col-lg-2"></div> -->
                            <div class="col-lg-6">
                                <div class="form-group  row">
                                    <label class="col-sm-4 col-form-label">Add Custom Fields</label>
                                    <div class="col-sm-1">
                                        <label class="i-checks"> <input type="checkbox" name="chk_custom_fields" id="chk_custom_fields">&nbsp; </label>
                                    </div>
                                    <div class="col-sm-6 text-right box_custom_fields" style="display: none">
                                        <textarea class="form-control input-lg" id="custom_fields" name="custom_fields"></textarea>

                                        <div class="add-custom-field text-right" style="margin-top: 10px; margin-bottom: 10px;">
                                            <textarea class="form-control input-lg" id="custom_fields" name="custom_fields"></textarea>
                                            <a href="#" class="btn-remove btn btn-danger btn-xs btn-outline"><i class="fa fa-remove"></i></a>
                                        </div>

                                        <a href="#" class="btn-add btn btn-primary btn-xs btn-outline"><i class="fa fa-plus">&nbsp;ADD MORE</i></a>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-lg-4">
                                <div class="form-group  row">
                                    <label class="col-sm-4 col-form-label">Status</label>
                                    <div class="col-sm-7">
                                        <label class="i-checks"> <input type="checkbox" name="is_active" id="is_active" checked>&nbsp; </label>
                                    </div>
                                </div>
                            </div>
                            <!-- <div class="col-lg-2"></div> -->
                            <div class="col-lg-8">
                                <div class="form-group  row">
                                    <div class="col-sm-3"></div>
                                    <div class="col-sm-4">
                                        <button class="btn btn-lg btn-default btn-block btn-form-reset" type="submit">RESET</button>
                                    </div>
                                    <div class="col-sm-4">
                                        <button class="btn btn-lg btn-primary btn-block btn-form-submit" type="submit">SAVE</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>