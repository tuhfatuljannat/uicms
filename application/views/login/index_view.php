<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>INSPINIA | Login</title>

    <link href="<?php echo base_url(); ?>public/inspinia/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>public/inspinia/font-awesome/css/font-awesome.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>public/inspinia/css/plugins/toastr/toastr.min.css" rel="stylesheet">

    <link href="<?php echo base_url(); ?>public/inspinia/css/animate.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>public/inspinia/css/style.css" rel="stylesheet">

</head>

<body class="gray-bg">

    <div class="loginColumns animated fadeInDown">
        <div class="row">

            <div class="col-md-6">
                <h2 class="font-bold">Welcome to IN+</h2>

                <p>
                    Perfectly designed and precisely prepared admin theme with over 50 pages with extra new web app views.
                </p>

                <p>
                    Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.
                </p>

                <p>
                    When an unknown printer took a galley of type and scrambled it to make a type specimen book.
                </p>

                <p>
                    <small>It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.</small>
                </p>
            </div>
            <div class="col-md-6">
                <div class="ibox-content">
                    <form id="login" role="form" method="post">
                        <div class="form-group">
                            <!-- <input type="hidden" class="form-control" id="id" name="id"> -->
                            <input type="email" name="email" id="email" class="form-control" placeholder="Email" required="">
                        </div>
                        <div class="form-group">
                            <input type="password" name="password" id="password" class="form-control" placeholder="Password" required="">
                        </div>
                        <!-- <button type="submit" class="btn btn-primary block full-width m-b">Login</button> -->
                        <button class="btn btn-sm btn-primary btn-form-submit block full-width m-b" type="submit">Login</button>

                        <a href="#">
                            <small>Forgot password?</small>
                        </a>

                        <p class="text-muted text-center">
                            <small>Do not have an account?</small>
                        </p>
                        <a class="btn btn-sm btn-white btn-block" href="register.html">Create an account</a>
                    </form>
                    <p class="m-t">
                        <small>Inspinia we app framework base on Bootstrap 3 &copy; 2014</small>
                    </p>
                </div>
            </div>
        </div>
        <hr/>
        <div class="row">
            <div class="col-md-6">
                Copyright Example Company
            </div>
            <div class="col-md-6 text-right">
               <small>© 2014-2015</small>
            </div>
        </div>
    </div>

    <script src="<?php echo base_url(); ?>public/inspinia/js/jquery-3.1.1.min.js"></script>
    <script src="<?php echo base_url(); ?>public/inspinia/js/popper.min.js"></script>
    <script src="<?php echo base_url(); ?>public/inspinia/js/bootstrap.js"></script>
    <script src="<?php echo base_url(); ?>public/inspinia/js/bootstrap.min.js"></script>
    <script src="<?php echo base_url(); ?>public/inspinia/js/plugins/toastr/toastr.min.js"></script>
    <script src="<?php echo base_url(); ?>public/inspinia/js/plugins/validate/jquery.validate.min.js"></script>
    <script src="<?php echo base_url(); ?>public/inspinia/js/plugins/jasny/jasny-bootstrap.min.js"></script>
    <script src="<?php echo base_url(); ?>public/inspinia/js/plugins/iCheck/icheck.min.js"></script>
    <script src="<?php echo base_url(); ?>public/inspinia/js/plugins/chosen/chosen.jquery.js"></script>
    <script src="<?php echo base_url(); ?>public/inspinia/js/plugins/datapicker/bootstrap-datepicker.js"></script>

    <script src="<?php  echo base_url();   ?>public/inspinia/js/custom.js"></script>
    <script src="<?php  echo base_url();   ?>public/js/admin/login.js"></script>

    <script src="<?php  echo base_url();   ?>public/js/common/function.js"></script>

</body>

</html>
