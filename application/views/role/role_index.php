<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

	<div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2>Role</h2>
            <ol class="breadcrumb">
                <li class="breadcrumb-item">
                    <a href="index.html">Home</a>
                </li>
                <li class="breadcrumb-item">
                    <a>Role</a>
                </li>
                <li class="breadcrumb-item active">
                    <strong>Index</strong>
                </li>
            </ol>
        </div>
        <div class="col-lg-2">
        	<!-- <div class="title-action">
	            <a href="<?php // echo base_url(); ?>role" class="btn btn-primary">Role List</a>
	        </div> -->
        </div>
    </div>
    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
	        <div class="col-lg-4">
	            <div class="ibox ">
                    <div class="ibox-title">
                        <h5 class="form-title">Role Entry</h5>
                        <div class="ibox-tools">
                            <a class="collapse-link">
                                <i class="fa fa-chevron-up"></i>
                            </a>
                            <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                                <i class="fa fa-wrench"></i>
                            </a>
                            <ul class="dropdown-menu dropdown-user">
                                <li><a href="#" class="dropdown-item">Config option 1</a>
                                </li>
                                <li><a href="#" class="dropdown-item">Config option 2</a>
                                </li>
                            </ul>
                            <a class="close-link">
                                <i class="fa fa-times"></i>
                            </a>
                        </div>
                    </div>
                    <div class="ibox-content">
                        <form id="form-role" method="POST" enctype="multipart/form-data">
                            <div class="form-group row">
                            	<label class="col-lg-3 col-form-label">Name</label>
                                <div class="col-lg-9">
                                	<input type="hidden" class="form-control" id="id" name="id">
                                	<input type="text" placeholder="Name" id="name" class="form-control" name="name">
                                </div>
                            </div>
                            
                            <div class="form-group row">
                            	<label class="col-lg-3 col-form-label">Permission</label>
                                <div class="col-lg-9">
                                    <select name="permission[]" id="permission" data-placeholder="Choose Permission..." class="form-control chosen-select" multiple tabindex="4">
                                        <option value="Users">Users</option>
                                        <option value="Role">Role</option>
                                        <option value="Warehouse">Warehouse</option>
                                        <option value="Block">Block</option>
                                        <option value="Shelf">Shelf</option>
                                        <option value="Currency">Currency</option>
                                        <option value="Unit">Unit</option>
                                        <option value="Brand">Brand</option>
                                        <option value="Category">Category</option>
                                        <option value="Product">Product</option>
                                    </select>
                                </div>
                            </div>
                            
                            <div class="form-group row">
                            	<label class="col-lg-3 col-form-label" id="status">Status</label>
                                <div class="col-lg-9">
                                	<div class="i-checks"><label> <input type="checkbox" id="is_active" name="is_active" checked></label></div>
                                </div>
                            </div>
                           
                            <div class="form-group row">
                                <div class="col-lg-12">
                                    <button class="btn btn-sm btn-primary btn-form-submit pull-right" type="submit">SAVE</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
	        </div>
	        <div class="col-lg-8">
                <div class="ibox ">
                    <div class="ibox-title">
                        <h5>Role List  </h5>
                        <div class="ibox-tools">
                            <a class="collapse-link">
                                <i class="fa fa-chevron-up"></i>
                            </a>
                            <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                                <i class="fa fa-wrench"></i>
                            </a>
                            <ul class="dropdown-menu dropdown-user">
                                <li><a href="#" class="dropdown-item">Config option 1</a>
                                </li>
                                <li><a href="#" class="dropdown-item">Config option 2</a>
                                </li>
                            </ul>
                            <a class="close-link">
                                <i class="fa fa-times"></i>
                            </a>
                        </div>
                    </div>
                    <div class="ibox-content">

                        <table class="table table-hover tbl-role">
                            <thead>
	                            <tr>
	                                <th>ID</th>
	                                <th>Role</th>
	                                <th>Permission</th>
	                                <th>Status</th>
	                                <th>Action</th>
	                            </tr>
                            </thead>
                            <tbody>
                            	 
	                           <!--  <tr>
	                                <td>1</td>
	                                <td>ADMIN</td>
	                                <td>N/A</td>
	                                <td><span class="label label-primary">ACTIVE</span></td>
	                                <td>
	                                	<a href="" class="btn-edit btn btn-primary btn-xs btn-outline" data-id="1"><i class="fa fa-edit"></i></a>
	                                	&nbsp;
	                                	<a href="" class="btn-edit btn btn-danger btn-xs btn-outline" data-id="1"><i class="fa fa-remove"></i></a>
	                                </td>
	                            </tr>
	                            <tr>
	                                <td>2</td>
	                                <td>Manager</td>
	                                <td>N/A</td>
	                                <td><span class="label label-danger">DEACTIVE</span></td>
	                                <td>
                                        <a href="" class="btn-edit btn btn-primary btn-xs btn-outline" data-id="1"><i class="fa fa-edit"></i></a>
                                        &nbsp;
                                        <a href="" class="btn-edit btn btn-danger btn-xs btn-outline" data-id="1"><i class="fa fa-remove"></i></a>
                                    </td>
	                            </tr>
	                            <tr>
	                                <td>3</td>
	                                <td>Sales</td>
	                                <td>N/A</td>
	                               <td><span class="label label-primary">ACTIVE</span></td>
	                                <td>
                                        <a href="" class="btn-edit btn btn-primary btn-xs btn-outline" data-id="1"><i class="fa fa-edit"></i></a>
                                        &nbsp;
                                        <a href="" class="btn-edit btn btn-danger btn-xs btn-outline" data-id="1"><i class="fa fa-remove"></i></a>
                                    </td>
	                            </tr> -->
	                           
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>