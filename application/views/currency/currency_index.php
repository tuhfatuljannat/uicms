
<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<div class="row wrapper border-bottom white-bg page-heading">
  <div class="col-lg-10">
    <h2>Currency</h2>
    <ol class="breadcrumb">
      <li class="breadcrumb-item">
        <a href="index.html">Home</a>
      </li>
      <li class="breadcrumb-item">
        <a>Currency</a>
      </li>
      <li class="breadcrumb-item active">
        <strong>Index</strong>
      </li>
    </ol>
  </div>
  <div class="col-lg-3">

  </div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">
  <div class="row">
    <div class="col-lg-4">
      <div class="ibox ">
        <div class="ibox-title">
          <h5 class="form-title">Currency Entry</h5>
          <div class="ibox-tools">
            <a class="collapse-link">
              <i class="fa fa-chevron-up"></i>
            </a>
            <a class="dropdown-toggle" data-toggle="dropdown" href="#">
              <i class="fa fa-wrench"></i>
            </a>
            <ul class="dropdown-menu dropdown-Currency">
              <li><a href="#" class="dropdown-item">Config option 1</a>
              </li>
              <li><a href="#" class="dropdown-item">Config option 2</a>
              </li>
            </ul>
            <a class="close-link">
              <i class="fa fa-times"></i>
            </a>
          </div>
        </div>
        <div class="ibox-content">
          <form id="form-currency" method="post">
            <div class="form-group row">
              <label class="col-lg-3 col-form-label">Name</label>
              <div class="col-lg-9">
                <input type="hidden" class="form-control" id="id" name="id">
                <input type="text" placeholder="Name" id="name" class="form-control" name="name">
              </div>
            </div>
            <div class="form-group row">      
              <label class="col-lg-3 col-form-label">Code</label>
              <div class="col-lg-9">
                <input type="text" placeholder="Code" id="code" class="form-control" name="code">
              </div>
            </div>
            <div class="form-group row">
              <label class="col-lg-3 col-form-label">Symbol</label>
              <div class="col-lg-9">
                <input type="text" placeholder="Symbol" id="symbol" class="form-control" name="symbol"> 
                <!-- <span class="form-text m-b-none">Example block-level help text here.</span> -->
              </div>
            </div>
            <div class="form-group row">
              <label class="col-lg-3 col-form-label">Exchange Rate</label>
              <div class="col-lg-9">
                <input type="text" placeholder="Exchange_rate" id="exchange_rate" class="form-control" name="exchange_rate">
              </div>
            </div>
            <div class="form-group row">
              <label class="col-lg-3 col-form-label" id="status">Status</label>
              <div class="col-lg-9">
                <div class="i-checks"><label> <input type="checkbox" id="is_active" name="is_active" checked></label></div>
              </div>
            </div>
            
            <div class="form-group row">
              <div class="col-lg-12">
                <button class="btn btn-sm btn-primary btn-form-submit pull-right" type="submit">SAVE</button>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
    <div class="col-lg-8">
      <div class="ibox ">
        <div class="ibox-title">
          <h5>Currency List  </h5>
          <div class="ibox-tools">
            <a class="collapse-link">
              <i class="fa fa-chevron-up"></i>
            </a>
            <a class="dropdown-toggle" data-toggle="dropdown" href="#">
              <i class="fa fa-wrench"></i>
            </a>
            <ul class="dropdown-menu dropdown-Currency">
              <li><a href="#" class="dropdown-item">Config option 1</a>
              </li>
              <li><a href="#" class="dropdown-item">Config option 2</a>
              </li>
            </ul>
            <a class="close-link">
              <i class="fa fa-times"></i>
            </a>
          </div>
        </div>
        <div class="ibox-content">

          <table class="table table-hover dataTables-currency">
            <thead>
              <tr>
                <th>#</th>
                <th>Name</th>
                <th>Code</th>
                <th>Symbol</th>
                <th>Exchange Rate</th>
                <th>Active Status</th>
                <th>Action</th>
              </tr>
            </thead>
            <tbody>

            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>