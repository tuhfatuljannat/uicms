

<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Clients</h2>
        <ol class="breadcrumb">
            <li class="breadcrumb-item">
                <a href="index.html">Clients</a>
            </li>
            <li class="breadcrumb-item">
                <a>Forms</a>
            </li>
            <li class="breadcrumb-item active">
                <strong>Clients</strong>
            </li>
        </ol>
    </div>
    <div class="col-lg-2">

    </div>
</div>

<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-4">
            <div class="ibox-content">
                <h2 style="text-align:center;">Create Clients</h2><br>
                <form id="form-clients" method="POST">
                    <div class="form-group row">
                        <label class="col-lg-3 col-form-label">Name</label>
                        <div class="col-lg-9">
                            <input type="hidden" class="form-control" id="id" name="id">
                            <input type="text" placeholder="Name" id="name" class="form-control" name="name">
                        </div>
                    </div>
                    
                    <div class="form-group row">
                                <label class="col-lg-3 col-form-label">File</label>
                                <div class="col-lg-9">
                                    <div class="fileinput fileinput-new" data-provides="fileinput">
                                        <div>
                                            <span class="btn btn-default btn-file"><span class="fileinput-new"><span class="fa fa-camera fa-2x"></span> <!-- File --></span>
                                                <div id="preview" name="preview"  class="fileinput-preview fileinput-exists thumbnail" style="width: 100px; height: 30px; display:block;"></div>
                                                <img id="prevImg" name="prevImg" />
                                                <input type="file" name="file" id="file" class="ephoto-upload" accept="image/jpeg">
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>

                    <div class="form-group row">
                        <label class="col-lg-3 col-form-label" id="status">Status</label>
                        <div class="col-lg-9">
                            <div class="i-checks"><label> <input type="checkbox" id="is_active" name="is_active" checked></label></div>
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-lg-offset-3 col-lg-9">
                            <button class="btn btn-sm btn-primary btn-form-submit" type="submit">SAVE</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox ">
                    <div class="ibox-title">
                        <h5>Clients List</h5>
                        <div class="ibox-tools">
                            <a class="collapse-link">
                                <i class="fa fa-chevron-up"></i>
                            </a>
                            <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                                <i class="fa fa-wrench"></i>
                            </a>
                            <ul class="dropdown-menu dropdown-user">
                                <li><a href="#" class="dropdown-item">Config option 1</a>
                                </li>
                                <li><a href="#" class="dropdown-item">Config option 2</a>
                                </li>
                            </ul>
                            <a class="close-link">
                                <i class="fa fa-times"></i>
                            </a>
                        </div>
                    </div>
                    <div class="ibox-content">

                        <div class="table-responsive">
                            <table class="table table-striped table-bordered table-hover dataTables-clients" >
                                <thead>
                                    <tr>
                                        <th>Id</th>
                                        <th>Name</th>
                                        <th>Picture</th>  
                                        <th>Status</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>

                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>