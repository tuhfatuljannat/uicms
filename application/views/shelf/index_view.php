<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Add W/H Shelf</h2>
        <ol class="breadcrumb">
            <li class="breadcrumb-item">
                <a href="index.html">Home</a>
            </li>
            <li class="breadcrumb-item">
                <a>Forms</a>
            </li>
            <li class="breadcrumb-item active">
                <strong>Shelf</strong>
            </li>
        </ol>
    </div>
    <div class="col-lg-2">

    </div>
</div>

<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-4">
            <div class="ibox-content">
                <h2 style="text-align:center;">Create Shelf</h2><br>
                <form id="form-shelf" method="POST">
                    <div class="form-group row">
                        <label class="col-lg-3 col-form-label"> Shelf Name</label>
                        <div class="col-lg-9">
                            <input type="hidden" class="form-control" id="id" name="id">
                            <input type="text" placeholder="Name" id="name" class="form-control" name="name">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-lg-3 col-form-label">Code</label>
                        <div class="col-lg-9">
                            <input type="text" placeholder="Code" id="code" class="form-control" name="code">
                        </div>
                    </div>
                     <div class="form-group row">
                        <label class="col-lg-3 col-form-label">Block</label>
                        <div class="col-lg-9">
                            <select name="block_id" id="block_id" class="form-control input-lg">

                                <option value="">Select Block</option>


                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-lg-3 col-form-label">Warehouse</label>
                        <div class="col-lg-9">
                            <select name="warehouse_id" id="warehouse_id" class="form-control input-lg">

                                <option value="">Select Warehouse</option>


                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-lg-3 col-form-label">Add User</label>
                        <div class="col-lg-9">
                            <select name="user_id" id="user_id" class="form-control input-lg">

                                <option value="">Select USER</option>


                            </select>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-lg-3 col-form-label" id="status">Status</label>
                        <div class="col-lg-9">
                            <div class="i-checks"><label> <input type="checkbox" id="is_active" name="is_active" checked></label></div>
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-lg-offset-3 col-lg-9">
                            <button class="btn btn-sm btn-primary btn-form-submit" type="submit">SAVE</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox ">
                    <div class="ibox-title">
                        <h5>Shelf List</h5>
                        <div class="ibox-tools">
                            <a class="collapse-link">
                                <i class="fa fa-chevron-up"></i>
                            </a>
                            <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                                <i class="fa fa-wrench"></i>
                            </a>
                            <ul class="dropdown-menu dropdown-user">
                                <li><a href="#" class="dropdown-item">Config option 1</a>
                                </li>
                                <li><a href="#" class="dropdown-item">Config option 2</a>
                                </li>
                            </ul>
                            <a class="close-link">
                                <i class="fa fa-times"></i>
                            </a>
                        </div>
                    </div>
                    <div class="ibox-content">

                        <div class="table-responsive">
                            <table class="table table-striped table-bordered table-hover dataTables-shelf" >
                                <thead>
                                    <tr>
                                        <th>Id</th>
                                        <th>Name</th>
                                        <th>Code</th>
                                        <th>Block</th>
                                        <th>Warehouse</th>
                                        <th>Add User</th>
                                        <th>Status</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>

                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>