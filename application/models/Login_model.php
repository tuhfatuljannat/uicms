<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login_model extends MY_Model {

    protected $_table_name = 'admin';
    protected $_primary_key = 'id';
    protected $_primary_filter = 'intval';
    protected $_order_by = 'id';
    protected $_timestamps = FALSE;

    // Validation Rules
    public $rules = array(
        'email' => array(
            'field' => 'email',
            // 'label' => 'Title',
            'rules' => 'trim|required|xss_clean'
        ),
        'password' => array(
            'field' => 'password',
            // 'label' => 'Description',
            'rules' => 'trim|required|xss_clean'
        )
    );

    function __construct()
    {
        parent::__construct();
    }
//check login
    public function checkLogin($email, $password)
    {
        $this->db->select('id,name,file_name');
        // $this->db->from('admin');
        $this->db->where('email',$email);
        $this->db->where('password',$password);     

        $query = $this->db->get('admin');

        if($query->num_rows() > 0)
        { 
            $data = $query->row();
            return $data;
        }
        else
        {
            return false;
        }
    }


}