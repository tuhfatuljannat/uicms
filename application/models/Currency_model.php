
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Currency_model extends MY_Model {

	protected $_table_name = 'currency';
	protected $_primary_key = 'id';
	protected $_primary_filter = 'intval';
	protected $_order_by = 'id';
	protected $_timestamps = FALSE;
	
    // Validation Rules
	public $rules = array(
		'name' => array(
			'field' => 'name',
			'label' => 'Name',
			'rules' => 'trim|required|xss_clean'
		),
		'symbol' => array(
			'field' => 'symbol',
			'label' => 'Symbol',
			'rules' => 'trim|required|xss_clean'
		)
	);


	function __construct()
	{
		parent::__construct();
	}

     // Get Currency Data
	public function get_currency($id = NULL) {
		
		if ($id) {
			$query = $this->db->where('id',$id);
			$query = $this->db->get('currency');
			$data = $query->row();
		} 
		else {
			$query = $this->db->get('currency');
			$data = $query->result();
		}
		
		return $data;
	}

}