<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Email_model extends MY_Model {

    protected $_table_name = 'send_mail';
    protected $_primary_key = 'id';
    protected $_primary_filter = 'intval';
    protected $_order_by = 'id';
    protected $_timestamps = FALSE;

    // Validation Rules
    public $rules = array(
        'email' => array(
            'field' => 'email',
            'label' => 'Email',
            'rules' => 'trim|required|xss_clean'
        ),
        
    );

    function __construct()
    {
        parent::__construct();
    }

    // Get About Data
    public function get_email($id = NULL) {

        if ($id) {
            $query = $this->db->where('id',$id);
            $query = $this->db->get('send_mail');
            $data = $query->row();
        } 
        else {

            $query = $this->db->get('send_mail');
            $data = $query->result();
        }

        return $data;
    }

}