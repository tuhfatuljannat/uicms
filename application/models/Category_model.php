<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Category_model extends MY_Model {

    protected $_table_name = 'category';
    protected $_primary_key = 'id';
    protected $_primary_filter = 'intval';
    protected $_order_by = 'id';
    protected $_timestamps = FALSE;
    
    // Validation Rules
    public $rules = array(
            'name' => array(
                'field' => 'name',
                'label' => 'Name',
                'rules' => 'trim|required|xss_clean'
            ),
            'code' => array(
                'field' => 'code',
                'label' => 'Code',
                'rules' => 'trim|required|xss_clean'
            ),
            'sort_order' => array(
                'field' => 'sort_order',
                'label' => 'Sort_order',
                'rules' => 'trim|required|xss_clean'
            ),
            'prefix' => array(
                'field' => 'prefix',
                'label' => 'Prefix',
                'rules' => 'trim|required|xss_clean'
            )
        );

	function __construct()
    {
        parent::__construct();
    }

    // Get Category Data
    public function get_with_parent($id = NULL) 
    {
        if ($id) 
        {
            $this->db->select('category.*, cat.name as parent_name');
            $this->db->join('category as cat', 'category.pid=cat.id', 'left');
            $query = $this->db->where('category.id',$id);
            $query = $this->db->get('category');
            $data = $query->row();
        } 
        else 
        {
            $this->db->select('category.*, cat.name as parent_name');
            $this->db->join('category as cat', 'category.pid=cat.id', 'left');
            $query = $this->db->get('category');
            $data = $query->result();
        }
    
        return $data;
    }

    // Get category Data
    public function get_category($id = NULL) {
         
    	
    	if ($id) {
    		$query = $this->db->where('id',$id);
            $query = $this->db->get('category');
            $data = $query->row();
    	} 
    	else {
    		$query = $this->db->get('category');
            $data = $query->result();
    	}
    	
    	return $data;
    }

    public function get_subcategory($pid = NULL){ 

            $query = $this->db->where('pid',$pid);
            $query = $this->db->get('category');
            $data = $query->result();
            
            return $data;
    }
}