<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Slide_model extends MY_Model {

    protected $_table_name = 'slides';
    protected $_primary_key = 'id';
    protected $_primary_filter = 'intval';
    protected $_order_by = 'id';
    protected $_timestamps = FALSE;

    // Validation Rules
    public $rules = array(
        'title' => array(
            'field' => 'title',
            'label' => 'Title',
            'rules' => 'trim|required|xss_clean'
        ),
        'description' => array(
            'field' => 'description',
            'label' => 'Description',
            'rules' => 'trim|required|xss_clean'
        )
    );

    function __construct()
    {
        parent::__construct();
    }

    // Get About Data
    public function get_slides($id = NULL) {

        if ($id) {
            $query = $this->db->where('id',$id);
            $query = $this->db->get('slides');
            $data = $query->row();
        } 
        else {

            $query = $this->db->get('slides');
            $data = $query->result();
        }

        return $data;
    }

     // Get Slide Us Data with status active (UI)
    public function get_slides_ui($id = NULL) {
        
        $query = $this->db->where('is_active',1);
        $query = $this->db->get('slides');
        $data = $query->result();

        return $data;
    }

}
