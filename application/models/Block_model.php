<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Block_model extends MY_Model {

    protected $_table_name = 'block';
    protected $_primary_key = 'id';
    protected $_primary_filter = 'intval';
    protected $_order_by = 'id';
    protected $_timestamps = FALSE;

    // Validation Rules
    public $rules = array(
        'name' => array(
            'field' => 'name',
            'label' => 'Block Name',
            'rules' => 'trim|required|xss_clean'
        ),
        'code' => array(
            'field' => 'code',
            'label' => 'Code',
            'rules' => 'trim|required|xss_clean'
        ),
        'warehouse_id' => array(
            'field' => 'warehouse_id',
            'label' => 'Warehouse',
            'rules' => 'trim|required|xss_clean'
        ),
        
        'user_id' => array(
            'field' => 'user_id',
            'label' => 'Add User',
            'rules' => 'trim|required|xss_clean'
        )
    );

    function __construct()
    {
        parent::__construct();
    }

    // Get Block Data
    public function get_block($id = NULL) {

        if ($id) {
            $query = $this->db->where('id',$id);
            $query = $this->db->get('block');
            $data = $query->row();
        } 
        else {

            $this->db->select('block.*,warehouse.name as w_name,users.name as u_name');

            $this->db->from('block'); 

            $this->db->join('warehouse', 'warehouse.id=block.warehouse_id', 'left');

            $this->db->join('users', 'users.id=block.user_id', 'left');

            $query = $this->db->get();

            $data = $query->result();
        }

        return $data;
    }

}