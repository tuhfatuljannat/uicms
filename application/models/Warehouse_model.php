<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Warehouse_model extends MY_Model {

    protected $_table_name = 'warehouse';
    protected $_primary_key = 'id';
    protected $_primary_filter = 'intval';
    protected $_order_by = 'id';
    protected $_timestamps = FALSE;

    // Validation Rules
    public $rules = array(
        'name' => array(
            'field' => 'name',
            'label' => 'Name',
            'rules' => 'trim|required|xss_clean'
        ),
        'code' => array(
            'field' => 'code',
            'label' => 'Code',
            'rules' => 'trim|required|xss_clean'
        ),
        'type_id' => array(
            'field' => 'type_id',
            'label' => 'Type',
            'rules' => 'trim|required|xss_clean'
        ),
        'mobile' => array(
            'field' => 'mobile',
            'label' => 'Mobile',
            'rules' => 'trim|required|xss_clean'
        ),
        'email' => array(
            'field' => 'email',
            'label' => 'Email',
            'rules' => 'trim|required|xss_clean'
        ),
        'address' => array(
            'field' => 'address',
            'label' => 'Address',
            'rules' => 'trim|required|xss_clean'
        ),
        'user_id' => array(
            'field' => 'user_id',
            'label' => 'Add User',
            'rules' => 'trim|required|xss_clean'
        )
    );

    function __construct()
    {
        parent::__construct();
    }

    // Get Warehouse Data
    public function get_warehouse($id = NULL) {

        if ($id) {
            $query = $this->db->where('id',$id);
            $query = $this->db->get('warehouse');
            $data = $query->row();
        } 
        else {

          $this->db->select('warehouse.*,users.name as u_name');

            $this->db->from('warehouse'); 

            $this->db->join('users', 'users.id=warehouse.user_id', 'left');
            $query = $this->db->get();

            $data = $query->result();

        }
        // dump($data);
        return $data;
    }

}