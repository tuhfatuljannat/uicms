<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Skills_model extends MY_Model {

    protected $_table_name = 'skills';
    protected $_primary_key = 'id';
    protected $_primary_filter = 'intval';
    protected $_order_by = 'id';
    protected $_timestamps = FALSE;

    // Validation Rules
    public $rules = array(
        'title' => array(
            'field' => 'title',
            'label' => 'Title',
            'rules' => 'trim|required|xss_clean'
        ),
        'description' => array(
            'field' => 'description',
            'label' => 'Description',
            'rules' => 'trim|required|xss_clean'
        ),
        'progress' => array(
            'field' => 'progress',
            'label' => 'Progress',
            'rules' => 'trim|required|xss_clean'
        )
    );

    function __construct()
    {
        parent::__construct();
    }

    // Get Skills Data
    public function get_skills($id = NULL) {
        if ($id) {
            $query = $this->db->where('id',$id);
            $query = $this->db->get('skills');
            $data = $query->row();
        } 
        else {

            $query = $this->db->get('skills');
            $data = $query->result();
        }
        return $data;
    }

    // Get Skills Data with status active (UI)
    public function get_skills_ui($id = NULL) {
        
        $query = $this->db->where('is_active',1);
        $query = $this->db->get('skills');
        $data = $query->result();

        return $data;
    }
}