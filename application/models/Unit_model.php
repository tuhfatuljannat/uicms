
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Unit_model extends MY_Model {

	protected $_table_name = 'unit';
	protected $_primary_key = 'id';
	protected $_primary_filter = 'intval';
	protected $_order_by = 'id';
	protected $_timestamps = FALSE;
	
    // Validation Rules
	public $rules = array(
		
		'name' => array(
			'field' => 'name',
			'label' => 'Name',
			'rules' => 'trim|required|xss_clean'
		),
		'code' => array(
			'field' => 'code',
			'label' => 'Code',
			'rules' => 'trim|required|xss_clean'
		)
	);


	function __construct()
	{
		parent::__construct();
	}

     // Get unit Data
	public function get_unit($id = NULL) {
		
		if ($id) {
			$query = $this->db->where('id',$id);
			$query = $this->db->get('unit');
			$data = $query->row();
		} 
		else {
			$this->db->select('unit.*, type.name as u_typename');
			$this->db->from('unit'); 

			$this->db->join('type', 'type.id=unit.type_id', 'left');
			$query=$this->db->get();
			$data = $query->result();

		}
		
		return $data;
	}

      // public function get_all_unit()
  //    {
  //       $this->db->select('unit.*, unittype.unittype_name as u_unittypename');
  //       $this->db->from('unit'); 

  //       $this->db->join('unittype', 'unittype.id=unit.unit_type', 'left');
  //       $query=$this->db->get();

  //       return $query->result();
  //    }



}