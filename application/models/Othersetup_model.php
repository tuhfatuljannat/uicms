

<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Othersetup_model extends MY_Model {

	protected $_table_name = 'other_setup';
	protected $_primary_key = 'id';
	protected $_primary_filter = 'intval';
	protected $_order_by = 'id';
	protected $_timestamps = FALSE;
	
    // Validation Rules
	public $rules = array(
		
		'name' => array(
			'field' => 'name',
			'label' => 'Name',
			'rules' => 'trim|required|xss_clean'
		)
	);


	function __construct()
	{
		parent::__construct();
	}

     // Get othersetup Data
	public function get_othersetup($id = NULL) {
		
		if ($id) {
			$query = $this->db->where('id',$id);
			$query = $this->db->get('other_setup');
			$data = $query->row();
		} 
		else {
			$query = $this->db->get('other_setup');
			$data = $query->result();
		}
		
		return $data;
	}

}