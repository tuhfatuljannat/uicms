
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User_model extends MY_Model {

    protected $_table_name = 'admin';
    protected $_primary_key = 'id';
    protected $_primary_filter = 'intval';
    protected $_order_by = 'id';
    protected $_timestamps = FALSE;

    // Validation Rules
    public $rules = array(
        'name' => array(
            'field' => 'name',
            'label' => 'Name',
            'rules' => 'trim|required|xss_clean'
        )
    );

    function __construct()
    {
        parent::__construct();
    }

    // Get admin Data
    public function get_user($id = NULL) {
        // dump($id,TRUE);
        if ($id) {
            $query = $this->db->where('id',$id);
            $query = $this->db->get('admin');
            $data = $query->row();
        } 
        // else {

        //     $query = $this->db->get('admin');
        //     $data = $query->result();
        // }

        return $data;
    }

}