
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Bulkunit_model extends MY_Model {

	protected $_table_name = 'bulk_unit';
	protected $_primary_key = 'id';
	protected $_primary_filter = 'intval';
	protected $_order_by = 'id';
	protected $_timestamps = FALSE;
	
    // Validation Rules
	public $rules = array(
		
		'name' => array(
			'field' => 'name',
			'label' => 'Name',
			'rules' => 'trim|required|xss_clean'
		),
		'code' => array(
			'field' => 'code',
			'label' => 'Code',
			'rules' => 'trim|required|xss_clean'
		)
	);


	function __construct()
	{
		parent::__construct();
	}

     // Get bulkunit Data
	public function get_bulkunit($id = NULL) {
		
		if ($id) {
			$query = $this->db->where('id',$id);
			$query = $this->db->get('bulk_unit');
			$data = $query->row();
		} 
		else {
			$this->db->select('bulk_unit.*, type.name as u_typename');
			$this->db->from('bulk_unit'); 

			$this->db->join('type', 'type.id=bulk_unit.type_id', 'left');
			$query=$this->db->get();
			$data = $query->result();

		}
		
		return $data;
	}

      


}