<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Shelf_model extends MY_Model {

    protected $_table_name = 'shelf';
    protected $_primary_key = 'id';
    protected $_primary_filter = 'intval';
    protected $_order_by = 'id';
    protected $_timestamps = FALSE;

    // Validation Rules
    public $rules = array(
        'name' => array(
            'field' => 'name',
            'label' => 'Block Name',
            'rules' => 'trim|required|xss_clean'
        ),
        'code' => array(
            'field' => 'code',
            'label' => 'Code',
            'rules' => 'trim|required|xss_clean'
        ),
        'block_id' => array(
            'field' => 'block_id',
            'label' => 'Block',
            'rules' => 'trim|required|xss_clean'
        ),
        'warehouse_id' => array(
            'field' => 'warehouse_id',
            'label' => 'Warehouse',
            'rules' => 'trim|required|xss_clean'
        ),
        
        'user_id' => array(
            'field' => 'user_id',
            'label' => 'Add User',
            'rules' => 'trim|required|xss_clean'
        )
    );

    function __construct()
    {
        parent::__construct();
    }

    // Get shelf Data
    public function get_shelf($id = NULL) {

        if ($id) {
            $query = $this->db->where('id',$id);
            $query = $this->db->get('shelf');
            $data = $query->row();
        } 
        else {

            $this->db->select('shelf.*,block.name as b_name,warehouse.name as w_name,users.name as u_name');

            $this->db->from('shelf'); 
             $this->db->join('block', 'block.id=shelf.block_id', 'left');
            $this->db->join('warehouse', 'warehouse.id=shelf.warehouse_id', 'left');

            $this->db->join('users', 'users.id=shelf.user_id', 'left');

            $query = $this->db->get();

            $data = $query->result();
        }

        return $data;
    }

}