<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Customer_model extends MY_Model {

    protected $_table_name = 'customers';
    protected $_primary_key = 'id';
    protected $_primary_filter = 'intval';
    protected $_order_by = 'id';
    protected $_timestamps = FALSE;
    
    // Validation Rules
        public $rules = array(
                'code' => array(
                    'field' => 'code',
                    'label' => 'Code',
                    'rules' => 'trim|required|xss_clean'
                ),
                'name' => array(
                    'field' => 'name',
                    'label' => 'Name',
                    'rules' => 'trim|required|xss_clean'
                ),
                'mobile' => array(
                    'field' => 'mobile',
                    'label' => 'Mobile',
                    'rules' => 'trim|required|xss_clean'
                ),
                'address' => array(
                    'field' => 'address',
                    'label' => 'Address',
                    'rules' => 'trim|required|xss_clean'
                )
            );

	function __construct()
        {
            parent::__construct();
        }

    // Get Customer Data
        public function get_customers($id = NULL) {
        	
        	if ($id) {
        		$query = $this->db->where('id',$id);
                $query = $this->db->get('customers');
                $data = $query->row();
        	} 
        	else {
        		$query = $this->db->get('customers');
            	$data = $query->result();
        	}
        	
        	return $data;
        }

}