
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Brand_model extends MY_Model {

	protected $_table_name = 'brand';
	protected $_primary_key = 'id';
	protected $_primary_filter = 'intval';
	protected $_order_by = 'id';
	protected $_timestamps = FALSE;
	
    // Validation Rules
	public $rules = array(
		'name' => array(
			'field' => 'name',
			'label' => 'Name',
			'rules' => 'trim|required|xss_clean'
		),
		
	);


	function __construct()
	{
		parent::__construct();
	}

     // Get brand Data
	public function get_brand($id = NULL) {
		
		if ($id) {
			$query = $this->db->where('id',$id);
			$query = $this->db->get('brand');
			$data = $query->row();
		} 
		else {
			$query = $this->db->get('brand');
			$data = $query->result();
		}
		
		return $data;
	}

}