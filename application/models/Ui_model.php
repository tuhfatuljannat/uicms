
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ui_model extends MY_Model {

    protected $_table_name = 'contactus';
    protected $_primary_key = 'id';
    protected $_primary_filter = 'intval';
    protected $_order_by = 'id';
    protected $_timestamps = FALSE;

    // Validation Rules
    public $rules = array(
        'name' => array(
            'field' => 'name',
            // 'label' => 'Name',
            'rules' => 'trim|required|xss_clean'
        ),
        'email' => array(
            'field' => 'email',
            // 'label' => 'Email',
            'rules' => 'trim|required|xss_clean'
        ),
        'subject' => array(
            'field' => 'subject',
            // 'label' => 'subject',
            'rules' => 'trim|required|xss_clean'
        ),
        'message' => array(
            'field' => 'message',
            // 'label' => 'message',
            'rules' => 'trim|required|xss_clean'
        )
    ); 

    function __construct()
    {
        parent::__construct();
    }

    // Get contactus Data
    public function get_ui($id = NULL) {

        if ($id) {
            $query = $this->db->where('id',$id);
            $query = $this->db->get('contactus');
            $data = $query->row();
        } 
        else {

            $query = $this->db->get('contactus');
            $data = $query->result();
        }

        return $data;
    }

}
