<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Settings_model extends MY_Model {

    protected $_table_name = 'apps_config';
    protected $_primary_key = 'id';
    protected $_primary_filter = 'intval';
    protected $_order_by = 'id';
    protected $_timestamps = FALSE;

    // Validation Rules
    public $rules = array(
        'company_name' => array(
            'field' => 'company_name',
            'label' => ' Company Name',
            'rules' => 'trim|required|xss_clean'
        )
    );

    function __construct()
    {
        parent::__construct();
    }

    // Get Apps Config data Data
    public function get_apps($id = NULL) {

        if ($id) {
            $query = $this->db->where('id',$id);
            $query = $this->db->get('apps_config');
            $data = $query->row();
        } 
        else {

            $query = $this->db->get('apps_config');
            $data = $query->result();
        }

        return $data;
    }

}