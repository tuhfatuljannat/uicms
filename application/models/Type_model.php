

<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Type_model extends MY_Model {

	protected $_table_name = 'type';
	protected $_primary_key = 'id';
	protected $_primary_filter = 'intval';
	protected $_order_by = 'id';
	protected $_timestamps = FALSE;
	
    // Validation Rules
	public $rules = array(
		
		'name' => array(
			'field' => 'name',
			'label' => 'Type Name',
			'rules' => 'trim|required|xss_clean'
		)
	);


	function __construct()
	{
		parent::__construct();
	}

     // Get type Data
	public function get_type($id = NULL) {
		
		if ($id) {
			$query = $this->db->where('id',$id);
			$query = $this->db->get('type');
			$data = $query->row();
		} 
		else {
			$query = $this->db->get('type');
			$data = $query->result();
		}
		
		return $data;
	}

}