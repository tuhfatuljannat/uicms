
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Services_model extends MY_Model {

    protected $_table_name = 'services';
    protected $_primary_key = 'id';
    protected $_primary_filter = 'intval';
    protected $_order_by = 'id';
    protected $_timestamps = FALSE;

    // Validation Rules
    public $rules = array(
        'title' => array(
            'field' => 'title',
            'label' => 'Title',
            'rules' => 'trim|required|xss_clean'
        ),
        'description' => array(
            'field' => 'description',
            'label' => 'Description',
            'rules' => 'trim|required|xss_clean'
        )
    );

    function __construct()
    {
        parent::__construct();
    }

    // Get services Data
    public function get_services($id = NULL) {

        if ($id) {
            $query = $this->db->where('id',$id);
            $query = $this->db->get('services');
            $data = $query->row();
        } 
        else {

            $query = $this->db->get('services');
            $data = $query->result();
        }

        return $data;
    }

    // Get Is Show Front
    public function get_service_isshow($id=null) {

        if ($id) {
            $query = $this->db->where('is_show_front',1);
            $query = $this->db->where('is_active',1);
            $this->db->from('services');
            $query = $this->db->get();
            $data = $query->row();
        } 
        else 
        {
            $query = $this->db->where('is_show_front',1);
            $query = $this->db->where('is_active',1);
            $this->db->from('services');           
            $query = $this->db->get();
            $data = $query->result();
        }
        // dump($data);
        return $data;
    }

     // Get team Us Data with status active (UI)
    public function get_service_ui($id = NULL) {
        
        $query = $this->db->where('is_active',1);
        $query = $this->db->get('services');
        $data = $query->result();

        return $data;
    }



}