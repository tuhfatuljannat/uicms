<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Service_model extends MY_Model {

    protected $_table_name = 'service';
    protected $_primary_key = 'id';
    protected $_primary_filter = 'intval';
    protected $_order_by = 'id';
    protected $_timestamps = FALSE;

    // Validation Rules
    public $rules = array(
        'name' => array(
            'field' => 'name',
            'label' => 'Service Name',
            'rules' => 'trim|required|xss_clean'
        ),
        'code' => array(
            'field' => 'code',
            'label' => 'Code',
            'rules' => 'trim|required|xss_clean'
        ),
        'type_id' => array(
            'field' => 'type_id',
            'label' => 'Type',
            'rules' => 'trim|required|xss_clean'
        ),
        'category_id' => array(
            'field' => 'category_id',
            'label' => 'Category',
            'rules' => 'trim|required|xss_clean'
        ),
        // 'sub_category_id' => array(
        //     'field' => 'sub_category_id',
        //     'label' => 'Sub Category',
        //     'rules' => 'trim|required|xss_clean'
        // ),
        'unit_id' => array(
            'field' => 'unit_id',
            'label' => 'Unit',
            'rules' => 'trim|required|xss_clean'
        )
    );

    function __construct()
    {
        parent::__construct();
    }

    // Get Service Data
    public function get_service($id = NULL) {

        if ($id) {
            $query = $this->db->where('service.id',$id);
            $this->db->select('service.*,cat.name as cat_name, subcat.name as subcat_name');

            $this->db->from('service');
            $this->db->join('category cat', 'service.category_id=cat.id', 'left');
            $this->db->join('category subcat', 'service.sub_category_id=subcat.id', 'left');
            $query = $this->db->get();
            $data = $query->row();
        } 
        else {
            

            $this->db->select('service.*,cat.name as cat_name, subcat.name as subcat_name');

            $this->db->from('service');
            $this->db->join('category cat', 'service.category_id=cat.id', 'left');
            $this->db->join('category subcat', 'service.sub_category_id=subcat.id', 'left');
            
            $query = $this->db->get();

            $data = $query->result();


        }
        // dump($data);
        return $data;
    }

   

}