

<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Clients_model extends MY_Model {

    protected $_table_name = 'clients';
    protected $_primary_key = 'id';
    protected $_primary_filter = 'intval';
    protected $_order_by = 'id';
    protected $_timestamps = FALSE;

    // Validation Rules
    public $rules = array(
        'name' => array(
            'field' => 'name',
            'label' => 'Name',
            'rules' => 'trim|required|xss_clean'
        )
    );

    function __construct()
    {
        parent::__construct();
    }

    // Get clients Data
    public function get_clients($id = NULL) {

        if ($id) {
            $query = $this->db->where('id',$id);
            $query = $this->db->get('clients');
            $data = $query->row();
        } 
        else {

            $query = $this->db->get('clients');
            $data = $query->result();
        }

        return $data;
    }

     // Get clients Us Data with status active (UI)
    public function get_clients_ui($id = NULL) {
        
        $query = $this->db->where('is_active',1);
        $query = $this->db->get('clients');
        $data = $query->result();

        return $data;
    }

}