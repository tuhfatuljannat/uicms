


<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Contactus_model extends MY_Model {

    protected $_table_name = 'contactus';
    protected $_primary_key = 'id';
    protected $_primary_filter = 'intval';
    protected $_order_by = 'id';
    protected $_timestamps = FALSE;

    // Validation Rules
    public $rules = array(
        'name' => array(
            'field' => 'name',
            'label' => 'Name',
            'rules' => 'trim|required|xss_clean'
        ),
        'email' => array(
            'field' => 'email',
            'label' => 'Email',
            'rules' => 'trim|required|xss_clean'
        )
    );

    function __construct()
    {
        parent::__construct();
    }

    // Get contactus Data
    public function get_contactus($id = NULL) {

        if ($id) {
            $query = $this->db->where('id',$id);
            $query = $this->db->get('contactus');
            $data = $query->row();
        } 
        else {

            $query = $this->db->get('contactus');
            $data = $query->result();
        }

        return $data;
    }

}