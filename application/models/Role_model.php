<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Role_model extends MY_Model {

    protected $_table_name = 'role';
    protected $_primary_key = 'id';
    protected $_primary_filter = 'intval';
    protected $_order_by = 'id';
    protected $_timestamps = FALSE;
    
    // Validation Rules
    public $rules = array(
            'name' => array(
                'field' => 'name',
                'label' => 'Name',
                'rules' => 'trim|required|xss_clean'
            )
            // 'permission' => array(
            //     'field' => 'permission',
            //     'label' => 'Permission',
            //     'rules' => 'trim|required|xss_clean'
            // )
        );

	function __construct()
    {
        parent::__construct();
    }

    // Get Role Data
    public function get_roles($id = NULL) {
    	
    	if ($id) {
    		$query = $this->db->where('id',$id);
            $query = $this->db->get('role');
            $data = $query->row();
    	} 
    	else {
    		$query = $this->db->get('role');
        	$data = $query->result();
    	}
    	
    	return $data;
    }

}