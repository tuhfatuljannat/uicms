<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Team_model extends MY_Model {

    protected $_table_name = 'team_member';
    protected $_primary_key = 'id';
    protected $_primary_filter = 'intval';
    protected $_order_by = 'id';
    protected $_timestamps = FALSE;

    // Validation Rules
    public $rules = array(
          'name' => array(
            'field' => 'name',
            'label' => 'Name',
            'rules' => 'trim|required|xss_clean'
        ),
           'designation' => array(
            'field' => 'designation',
            'label' => 'Designation',
            'rules' => 'trim|required|xss_clean'
        ),
            'name' => array(
            'field' => 'name',
            'label' => 'Name',
            'rules' => 'trim|required|xss_clean'
        )
    );

    function __construct()
    {
        parent::__construct();
    }

    // Get About Data
    public function get_team($id = NULL) {
        if ($id) {
            $query = $this->db->where('id',$id);
            $query = $this->db->get('team_member');
            $data = $query->row();
        } 
        else {

            $query = $this->db->get('team_member');
            $data = $query->result();
        }
        return $data;
    }

      // Get team Us Data with status active (UI)
    public function get_team_ui($id = NULL) {
        
        $query = $this->db->where('is_active',1);
        $query = $this->db->get('team_member');
        $data = $query->result();

        return $data;
    }

}