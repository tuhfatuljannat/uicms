<?php
defined('BASEPATH') OR exit('No direct script access allowed');

	$config['meta_title'] = 'AIA-WMS | Admin';

	$config['site_name'] = 'AIA-WMS';
	$config['site_address'] = 'Magnolia 2<sup>nd</sup> Floor, 168 Lalkhan Bazar, Chittagong. <br>

							Email: info@appteum.com, Contact: 031-623422, +88 019 77 55 33 22';
	
	$config['site_footer_content'] = ' &copy 2020 AIA-WMS. Some Rights Reserved. An <a target="_blank" href="http://www.appteum.com" style="color:#009688;">APPTEUM</a> Family.';
	
	$config['inspinia_footer_content'] = '<div class="float-right">Version: <strong>v1.0.0</strong>.</div><div><strong>Copyright</strong> AIA-WMS &copy; 2019-2020</div>';

/*
	$config['header_css'] = array('style.css','prettyPhoto.css','nivo-slider.css');
	$config['header_js']  = array(
								'core.js','core.js',
                              	'jquery-1.4.1.min.js',
                              	'jquery-slidedeck.pack.lite.js',
                              	'jquery-prettyPhoto.js',
                              	'jquery.nivo.slider.js'
                         	);
*/
	$config['header_css'] = array();
	$config['header_js'] = array();

	$config['header_lib_css'] = array();
	$config['header_lib_js'] = array();