<?php
defined('BASEPATH') OR exit('No direct script access allowed');

	/**
	* MY Model
	*/
	class MY_Model extends CI_Model
	{
		protected $_table_name = '';
		protected $_primary_key = 'id';
		protected $_primary_filter = 'intval';
		protected $_order_by = '';
		public $rules = array();
		protected $_timestamps = FALSE;

		function __construct()
		{
			parent::__construct();
		}

		// load data from post array to data 
		public function array_from_post($fields) {
			$data = array();

			foreach ($fields as $field) {
				$data[$field] = $this->input->post($field);
			}

			return $data;
		}

		// Fetch data from db 
		public function get($id = NULL, $single = FALSE) {

			if($id != NULL) {
				$filter = $this->_primary_filter;
				$id = $filter($id);
				// $this->db->where($this->_primary_key, $id);
				$this->db->where(array($this->_table_name.'.is_active'=>1, $this->_primary_key=>$id));
				$method = 'row';
			}
			else if($single == TRUE) {
				$this->db->where($this->_table_name.'.is_active',1);
				$method = 'row';
			}
			else {
				$this->db->where($this->_table_name.'.is_active',1);
				$method = 'result';
			}


			if(!count($this->db->order_by($this->_order_by))) {
				$this->db->order_by($this->_order_by);
			}

			return $this->db->get($this->_table_name)->$method();
		}

		// Fetch data from db by WHERE clause
		public function get_by($where, $single = FALSE) {
			
			$this->db->where($where);

			return $this->get(NULL, $single);
		}

		// Get data for specific value
	    public function get_where($tbl, $col, $value) {
	    	$this->db->where($col, $value);
	    	$query = $this->db->get($tbl);
	    	return $query->result();
	    }

	    public function get_where_files($tbl, $col, $value) {
	    	$arr = array( $col => $value, 'file_ext !=' => 'default' );
	    	$this->db->where($arr);
	    	$query = $this->db->get($tbl);
	    	return $query->result();
	    }
		
		// INSERT / UPDATE into DB
		public function save($data, $id = NULL) {
			// Set timestamps
			if($this->_timestamps == TRUE) {
				$now = date('Y-m-d H:i:s');
				$id || $data['created'] = $now;
				$data['modified'] = $now;
			}

			// Insert
			if($id === NULL || $id == "") {
				!isset($data[$this->_primary_key]) || $data[$this->_primary_key] = NULL;
				$this->db->set($data);
				$this->db->insert($this->_table_name);
				$id = $this->db->insert_id();
			}
			// Update
			else {
				$filter = $this->_primary_filter;
				$id = $filter($id);
				$this->db->set($data);
				$this->db->where($this->_primary_key, $id);
				$this->db->update($this->_table_name);
			}

			return $id;
		}
		
		// DELETE form DB
		public function delete($id) {
			$filter = $this->_primary_filter;
			$id = $filter($id);

			if(!$id) {
				return FALSE;
			}

			$this->db->where($this->_primary_key, $id);
			$this->db->limit(1);
			$this->db->delete($this->_table_name);

			return TRUE;
		}

		// DeActive in
		public function de_active($id) {
			$filter = $this->_primary_filter;
			$id = $filter($id);

			if(!$id) {
				return FALSE;
			}

			$data = array(
					'is_active' => 0
				);

			$this->db->set($data);
			$this->db->where($this->_primary_key, $id);
			$this->db->limit(1);
			$this->db->update($this->_table_name);
		}

		// Check user lodgin or Not
	    public function loggedin() {
	        return (bool) $this->session->userdata('loggedin');
	    }

	}