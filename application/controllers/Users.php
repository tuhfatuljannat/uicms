<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Users extends CI_Controller {

	/**
	 * Users Controler 
	 */
	
	function __construct()
	{
		parent::__construct();

        // Load Stuff
		// $this->load->model('user_model');
	}

	// Users - index() - List
	public function index()
	{
		// Load css and js file
		// add_js('admin/user.js');

		// Load view file
		$data['subview'] = 'users/user_index';
		$this->load->view('inspinia/layout/_layout_main',$data);
	}

	// Get All/id User json data 
	
	// public function ajax_get_users($id = NULL)
	// {
	// 	// required headers
	// 	header("Access-Control-Allow-Origin: *");
	// 	header("Content-Type: application/json; charset=UTF-8");

	// 	// Get User data
	// 	$users['data'] = $this->user_model->get_users($id);

	// 	// Check ID null or Not
	// 	if (!empty($id)) {
			
	// 		$response = array(
 //        		'status'=>200,
 //        		'msg'=>'User Find by ID!',
 //        		'data'=> $users['data']
 //        	);
	// 		echo json_encode($response);
	// 	} 
	// 	else {

	// 	    $response = array(
 //        		'status'=>200,
 //        		'msg'=>'User List Find!',
 //        		'data'=> $users['data']
 //        	);
	// 		echo json_encode($response);
	// 	}
	// }
	
	// Users - edit(id) - Edit User
	
	public function edit($id = NULL)
	{
		// Load css and js file
		// add_js('admin/user.js');

		// Load view file
		$data['subview'] = 'users/user_edit';
		$this->load->view('inspinia/layout/_layout_main',$data);
	}
	
	// Store Data
	/*
	public function store($id = NULL)
	{
		// required headers
		header("Content-Type: application/json; charset=UTF-8");
		
		// Set up the form
        $rules = $this->user_model->rules;
        $this->form_validation->set_rules($rules);

		// Process the form
        if($this->form_validation->run() == TRUE) {

            $data = $this->user_model->array_from_post(array('id','name','username','password','mobile','email','skills','gender','dob','address'));
            $data['skills'] = implode(",", $data['skills']);
			$data['dob'] = date_sql($data['dob']);
            
            // Check File Empty or Not
			$defaultImg = FALSE;
			if (!empty($_FILES['file']['name'])) 
			{
				// File upload library loaded
				$this->load->library('upload');
	            $config['upload_path'] = './public/file_manager';
	            $config['allowed_types'] = 'gif|jpg|jpeg|png';
	            // $config['allowed_types'] = 'gif|jpg|jpeg|png|iso|dmg|zip|rar|doc|docx|xls|xlsx|ppt|pptx|csv|ods|odt|odp|pdf|rtf|sxc|sxi|txt|exe|avi|mpeg|mp3|mp4|3gp';
	            $config['encrypt_name'] = TRUE;
	            $config['max_size'] = 1024 * 8;
	            $config['overwrite'] = FALSE;
	            $this->upload->initialize($config);

	            // File upload
				if(!$this->upload->do_upload('file')) {
	            	$response = array(
	            		'status'=>400,
	            		'msg'=>$this->upload->display_errors()
	            	);
					echo json_encode($response);
	            }
	            else {
	            	$user['fileinfo'] = $this->upload->data();

	            	// $data['file_name'] =  $this->upload->data('file_name');
	            	$data['file_name'] =  $user['fileinfo']['file_name'];
	            	$data['file_ext'] =  $user['fileinfo']['file_ext'];
	            }
			} 
			else {
				$defaultImg = TRUE;
			}
			
			// Set Default Image
			if ($defaultImg == TRUE && empty($data['id'])) 
			{
				$data['file_name'] =  'user.png';
            	$data['file_ext'] =  'default';
			} 

        	// ------------------------------------------------------------------
        	// Get ID 
        	if (isset($data['id'])) { $id = $data['id']; } else { $id = NULL; }

            // store file that will be delated
			$files = $this->user_model->get_where_files('users','id',$id);
			
			// save - update
            if ($this->user_model->save($data, $id)) {
            	if (empty($id)) {
            		$response = array(
	            		'status'=>200,
	            		'msg'=>'User added successfully!'
	            	);
					echo json_encode($response);
            	} 
            	else {
					// delete file from directory
					if ($defaultImg == FALSE) {
						unlink_file($files);
					}
					
            		$response = array(
	            		'status'=>201,
	            		'msg'=>'User updated successfully!'
	            	);
					echo json_encode($response);
            	}
            }
            else {
            	$response = array(
            		'status'=>503,
            		'msg'=>'Unable to create user!'
            	);
				echo json_encode($response);
            }
            // ------------------------------------------------------------------
        }
        else {
        	$response = array(
            		'status'=>400,
            		'msg'=>'Unable to create user. Data is incomplete!'
            	);
			echo json_encode($response);
        }
	}
	*/
	// Delete data
	/*
	public function delete() {

		// Get ID
		$id = $this->input->post('id');

		// store file that will be delated
		$files = $this->user_model->get_where_files('users','id',$id);

		if ($this->user_model->delete($id)) {

			// delete file from directory
			unlink_file($files);
        	
        	$response = array(
        		'status'=>200,
        		'msg'=>'User deleted successfully!'
        	);
			echo json_encode($response);
        }
        else {
        	$response = array(
        		'status'=>503,
        		'msg'=>'Unable to delete user!'
        	);
			echo json_encode($response);
        }
	}
	*/
}