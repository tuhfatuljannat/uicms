<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Block extends CI_Controller {
	function __construct()
	{
		parent::__construct();

        // Load Stuff
        $this->load->model('block_model');
		$this->load->model('warehouse_model');
		$this->load->model('user_model');
		$this->load->helper('form');
		$this->load->library('form_validation');	
	}

	public function index()
	{
		// Load css and js file
		add_lib_css(array('inspinia/css/plugins/iCheck/custom.css',
			'inspinia/css/plugins/chosen/bootstrap-chosen.css',
			'inspinia/css/plugins/datapicker/datepicker3.css',
			'inspinia/css/plugins/dataTables/datatables.min.css'
			));
		add_lib_js(array(
			'inspinia/js/plugins/iCheck/icheck.min.js',
			'inspinia/js/plugins/chosen/chosen.jquery.js',
			'inspinia/js/plugins/datapicker/bootstrap-datepicker.js',
			'inspinia/js/plugins/dataTables/datatables.min.js',
			'inspinia/js/plugins/dataTables/dataTables.bootstrap4.min.js',
			'inspinia/js/plugins/validate/jquery.validate.min.js',
			'inspinia/js/plugins/jasny/jasny-bootstrap.min.js'
			));
		add_js(array(
					// 'admin/inspinia/form.js',
					'admin/block.js'
				));
		
		// Load view file
		$data['subview'] = 'block/block_index';
		$this->load->view('inspinia/layout/_layout_main',$data);
	}
	// Get All/id block  json data 
	public function ajax_get_block($id = NULL)
	{
		// required headers
		header("Access-Control-Allow-Origin: *");
		header("Content-Type: application/json; charset=UTF-8");

		$block['data'] = $this->block_model->get_block($id);

		if (!empty($id)) {
			
			$response = array(
        		'status'=>200,
        		'msg'=>'Block Find by ID!',
        		'data'=> $block['data']
        	);
			echo json_encode($response);
		} 
		else {

		    $response = array(
        		'status'=>200,
        		'msg'=>'Block List Find!',
        		'data'=> $block['data']
        	);
			echo json_encode($response);
		}
	}


	public function ajax_get_users($id = NULL)
	{
		// required headers
		header("Access-Control-Allow-Origin: *");
		header("Content-Type: application/json; charset=UTF-8");

		$users['data'] = $this->user_model->get_users($id);

		if (!empty($id)) {
			
			$response = array(
        		'status'=>200,
        		'msg'=>'User Find by ID!',
        		'data'=> $users['data']
        	);
			echo json_encode($response);
		} 
		else {

		    $response = array(
        		'status'=>200,
        		'msg'=>'User List Find!',
        		'data'=> $users['data']
        	);
			echo json_encode($response);
		}
	}

//Store
	public function store($id = NULL)
	{

		// dump($_POST,true);
		// required headers
		header("Content-Type: application/json; charset=UTF-8");

        // Set up the form
        $rules = $this->block_model->rules;
        $this->form_validation->set_rules($rules);
		
		// Process the form
        if($this->form_validation->run() == TRUE) {
            $data = $this->block_model->array_from_post(array('id','name','code','warehouse_id','user_id','is_active'));
         
             $data['is_active'] = isset($data['is_active'])? 1 : 0;
            // dump($data,true);

            if (isset($data['id'])) {
            	$id = $data['id'];
            } 
            else {
            	$id = NULL;
            }

             
            if ($this->block_model->save($data, $id)) {
            	if (empty($id)) 
            	{
            		$response = array(
	            		'status'=>200,
	            		'msg'=>'Block added successfully!'
	            	);
					echo json_encode($response);
            	} 
            	else 
            	{
            		$response = array(
	            		'status'=>201,
	            		'msg'=>'Block updated successfully!'
	            	);
					echo json_encode($response);
            	}
            }
            else
             {
            	$response = array(
            		'status'=>503,
            		'msg'=>'Block to create Customer!'
            	);
				echo json_encode($response);
            }
        }
        else
         {
        	$response = array(
            		'status'=>400,
            		'msg'=>'Unable to create Block. Data is incomplete!'
            	);
			echo json_encode($response);
        }
    }

// store end




	// Delete data
	public function delete() {

		$id = $this->input->post('id');

		if ($this->block_model->delete($id)) {
        	$response = array(
        		'status'=>200,
        		'msg'=>'Block deleted successfully!'
        	);
			echo json_encode($response);
        	
        }
        else {
        	$response = array(
        		'status'=>503,
        		'msg'=>'Block to delete Customer!'
        	);
			echo json_encode($response);
        }
	}


}