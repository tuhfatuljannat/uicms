<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ui extends CI_Controller {

	function __construct()
	{
		parent::__construct();

        // Load Stuff
		$this->load->model('ui_model');
		
		$this->load->model('services_model');
		$this->load->model('clients_model');
		$this->load->model('skills_model');
		$this->load->model('team_model');
		$this->load->model('settings_model');
		$this->load->model('portfolio_model');
		$this->load->model('aboutus_model');
		$this->load->model('slide_model');
	}


	public function index()
	{   
		// add_js('BizPage/js/ui.js');
		$this->load->view('ui/ui_index_view');
	}


	// Team Start
	public function ajax_get_team($id = NULL)
	{
		// required headers
		header("Access-Control-Allow-Origin: *");
		header("Content-Type: application/json; charset=UTF-8");

		$team['data'] = $this->team_model->get_team_ui($id);

		if (!empty($id)) {
			
			$response = array(
				'status'=>200,
				'msg'=>'Team Find by ID!',
				'data'=> $team['data']
			);
			echo json_encode($response);
		} 
		else {

			$response = array(
				'status'=>200,
				'msg'=>'Team List Find!',
				'data'=> $team['data']
			);
			echo json_encode($response);
		}
	}

	// Settings Start
	public function ajax_get_settings($id = NULL)
	{
		// required headers
		header("Access-Control-Allow-Origin: *");
		header("Content-Type: application/json; charset=UTF-8");

		$settings['data'] = $this->settings_model->get_apps($id);

		if (!empty($id)) {
			
			$response = array(
				'status'=>200,
				'msg'=>'Settings Find by ID!',
				'data'=> $settings['data']
			);
			echo json_encode($response);
		} 
		else {

			$response = array(
				'status'=>200,
				'msg'=>'Settings List Find!',
				'data'=> $settings['data']
			);
			echo json_encode($response);
		}
	}

	// Portfolio Start
	public function ajax_get_portfolio($id = NULL)
	{
		// required headers
		header("Access-Control-Allow-Origin: *");
		header("Content-Type: application/json; charset=UTF-8");

		$portfolio['data'] = $this->portfolio_model->get_portfolio($id);

		if (!empty($id)) {
			
			$response = array(
				'status'=>200,
				'msg'=>'Portfolio Find by ID!',
				'data'=> $portfolio['data']
			);
			echo json_encode($response);
		} 
		else {

			$response = array(
				'status'=>200,
				'msg'=>'Portfolio List Find!',
				'data'=> $portfolio['data']
			);
			echo json_encode($response);
		}
	}

	// ui slides
	public function ajax_get_ui_slide($id = NULL)
	{
	 	// required headers
		header("Access-Control-Allow-Origin: *");
		header("Content-Type: application/json; charset=UTF-8");



		// Get Type data
		// $ui['data'] = $this->ui_model->get_ui($id);
		$slide['data'] = $this->slide_model->get_slides_ui($id);

		// Check ID null or Not
		if (!empty($id)) {
			
			$response = array(
				'status'=>200,
				'msg'=>'Type Find by ID!',
				'data'=> $slide['data']
			);
			echo json_encode($response);
		} 
		else {

			$response = array(
				'status'=>200,
				'msg'=>'Type List Find!',
				'data'=> $slide['data']
			);
			echo json_encode($response);
		}
	}

	//// about us 
	public function ajax_get_aboutus($id = NULL)
	{
		// required headers
		header("Access-Control-Allow-Origin: *");
		header("Content-Type: application/json; charset=UTF-8");

		// Get Type data
		// $ui['data'] = $this->ui_model->get_ui($id);
		$about['data'] = $this->aboutus_model->get_aboutus_ui($id);

		// Check ID null or Not
		if (!empty($id)) {
			
			$response = array(
				'status'=>200,
				'msg'=>'Type Find by ID!',
				'data'=> $about['data']
			);
			echo json_encode($response);
		} 
		else {

			$response = array(
				'status'=>200,
				'msg'=>'Type List Find!',
				'data'=> $about['data']
			);
			echo json_encode($response);
		}
	}

	public function ajax_get_services($id = NULL)
	{
		// required headers
		header("Access-Control-Allow-Origin: *");
		header("Content-Type: application/json; charset=UTF-8");

		$services['data'] = $this->services_model->get_service_ui($id);

		if (!empty($id)) {
			
			$response = array(
				'status'=>200,
				'msg'=>'User Find by ID!',
				'data'=> $services['data']
			);
			echo json_encode($response);
		} 
		else {

			$response = array(
				'status'=>200,
				'msg'=>'User List Find!',
				'data'=> $services['data']
			);
			echo json_encode($response);
		}
	}

	// Get Is Shown From Service
	public function ajax_get_service_is_shown($id=null)
	{
		// required headers
		header("Access-Control-Allow-Origin: *");
		header("Content-Type: application/json; charset=UTF-8");

		$services['data'] = $this->services_model->get_service_isshow($id);

		if (!empty($id)) {
			
			$response = array(
				'status'=>200,
				'msg'=>'Service Find by ID!',
				'data'=> $services['data']
			);
			echo json_encode($response);
		} 
		else {

			$response = array(
				'status'=>200,
				'msg'=>'Service List Find!',
				'data'=> $services['data']
			);
			echo json_encode($response);
		}
	}

	public function ajax_get_clients($id = NULL)
	{
		// required headers
		header("Access-Control-Allow-Origin: *");
		header("Content-Type: application/json; charset=UTF-8");

		$clients['data'] = $this->clients_model->get_clients_ui($id);

		if (!empty($id)) {
			
			$response = array(
				'status'=>200,
				'msg'=>'User Find by ID!',
				'data'=> $clients['data']
			);
			echo json_encode($response);
		} 
		else {

			$response = array(
				'status'=>200,
				'msg'=>'User List Find!',
				'data'=> $clients['data']
			);
			echo json_encode($response);
		}
	}

	public function ajax_get_skills($id = NULL)
	{
		// required headers
		header("Access-Control-Allow-Origin: *");
		header("Content-Type: application/json; charset=UTF-8");

		$skills['data'] = $this->skills_model->get_skills_ui($id);

		if (!empty($id)) {
			
			$response = array(
				'status'=>200,
				'msg'=>'Skills Find by ID!',
				'data'=> $skills['data']
			);
			echo json_encode($response);
		} 
		else {

			$response = array(
				'status'=>200,
				'msg'=>'Skills List Find!',
				'data'=> $skills['data']
			);
			echo json_encode($response);
		}
	}


	// Store Data
	public function store($id = NULL)
	{
		// required headers
		header("Content-Type: application/json; charset=UTF-8");
		
		// Set up the form
		$rules = $this->ui_model->rules;
		$this->form_validation->set_rules($rules);
		// Process the form
		if($this->form_validation->run() == TRUE) {
			$data = $this->ui_model->array_from_post(array('id','name','email','subject','message','is_active'));
			$data['is_active'] = isset($data['is_active'])? 1 : 0;

	        	// Get ID 
			if (isset($data['id'])) { $id = $data['id']; } else { $id = NULL; }
			
				// save - update
			if ($this->ui_model->save($data, $id)) {
				if (empty($id)) {
					$response = array(
						'status'=>200,
						'msg'=>'Contactus added successfully!'
					);
					echo json_encode($response);
				} 
				else {
					$response = array(
						'status'=>201,
						'msg'=>'Contactus updated successfully!'
					);
					echo json_encode($response);
				}
			}
			else {
				$response = array(
					'status'=>503,
					'msg'=>'Unable to create Contactus!'
				);
				echo json_encode($response);
			}
		}
		else {
			$response = array(
				'status'=>400,
				'msg'=>'Unable to create Contactus. Data is incomplete!'
			);
			echo json_encode($response);
		}
	}

	// Delete data
		// public function delete() {

		// 	// Get ID
		// 	$id = $this->input->post('id');

		// 	if ($this->ui_model->delete($id)) {
		// 		$response = array(
		// 			'status'=>200,
		// 			'msg'=>'Contactus deleted successfully!'
		// 		);
		// 		echo json_encode($response);
	
		// 	}
		// 	else {
		// 		$response = array(
		// 			'status'=>503,
		// 			'msg'=>'Unable to delete Contactus!'
		// 		);
		// 		echo json_encode($response);
		// 	}
		// }

	
	

}