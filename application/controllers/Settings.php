<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Settings extends CI_Controller {

	function __construct()
	{
		parent::__construct();

        // Load Stuff
        $this->load->model('settings_model');
		$this->load->helper('form');
		$this->load->library('form_validation');	
	}
	public function index(){
		
		if (!$this->settings_model->loggedin()) {
			redirect('login');
		}
		// Load css and js file
		add_js(array(
					// 'admin/inspinia/form.js',
					'admin/settings.js'
				));
		
		// Load view file
		$data['subview'] = 'settings/settings_index';
		$this->load->view('inspinia/layout/_layout_main',$data);
	}

	// Get All/id Settings  json data 
	public function ajax_get_settings($id =1){
		// required headers
		header("Access-Control-Allow-Origin: *");
		header("Content-Type: application/json; charset=UTF-8");
		$apps['data'] = $this->settings_model->get_apps($id);
		if (!empty($id)) {
			$response = array(
        		'status'=>200,
        		'msg'=>'Apps Find by ID!',
        		'data'=> $apps['data']
        	);
			echo json_encode($response);
		} 
		else {

		    $response = array(
        		'status'=>200,
        		'msg'=>'Apps List Find!',
        		'data'=> $apps['data']
        	);
			echo json_encode($response);
		}
	}

	//Store
	public function store($id = NULL){
		// dump($_POST,true);
		// required headers
		header("Content-Type: application/json; charset=UTF-8");

        // Set up the form
        $rules = $this->settings_model->rules;
        $this->form_validation->set_rules($rules);
		// Process the form
        if($this->form_validation->run() == TRUE) {
            $data = $this->settings_model->array_from_post(array('id','company_name','company_desc','company_addr','company_mobile','company_email','about_intro','service_intro','skill_intro','team_intro','contact_intro','fb','twt','inst','gplus','link_in'));
             // $data['is_active'] = isset($data['is_active'])? 1 : 0;
            // dump($data,true);

            // Check File Empty or Not
			$defaultImg = FALSE;
			if (!empty($_FILES['file']['name'])) 
			{
				// File upload library loaded
				$this->load->library('upload');
	            $config['upload_path'] = './public/file_manager/';
	            $config['allowed_types'] = 'gif|jpg|jpeg|png';
	            // $config['allowed_types'] = 'gif|jpg|jpeg|png|iso|dmg|zip|rar|doc|docx|xls|xlsx|ppt|pptx|csv|ods|odt|odp|pdf|rtf|sxc|sxi|txt|exe|avi|mpeg|mp3|mp4|3gp';
	            $config['encrypt_name'] = TRUE;
	            $config['max_size'] = 1024 * 8;
	            $config['overwrite'] = FALSE;
	            $this->upload->initialize($config);

	            // File upload
				if(!$this->upload->do_upload('file')) {
	            	$response = array(
	            		'status'=>400,
	            		'msg'=>$this->upload->display_errors()
	            	);
					echo json_encode($response);
	            }
	            else {
	            	$about['fileinfo'] = $this->upload->data();

	            	// $data['file_name'] =  $this->upload->data('file_name');
	            	$data['file_name'] =  $about['fileinfo']['file_name'];
	            	$data['file_ext'] =  $about['fileinfo']['file_ext'];
	            }
			} 
			else {
				$defaultImg = TRUE;
			}
			
			// Set Default Image
			if ($defaultImg == TRUE && empty($data['id'])) 
			{
				$data['file_name'] =  'user.png';
            	$data['file_ext'] =  'default';
			} 

        	// ------------------------------------------------------------------
        	// Get ID 
        	if (isset($data['id'])) { $id = $data['id']; } else { $id = NULL; }

            // store file that will be delated
			$files = $this->settings_model->get_where_files('apps_config','id',$id);

            if (isset($data['id'])) {
            	$id = $data['id'];
            } 
            else {
            	$id = NULL;
            }
            // save data
            if ($this->settings_model->save($data, $id)) {
            	if (empty($id)) 
            	{
            		$response = array(
	            		'status'=>200,
	            		'msg'=>'Apps added successfully!'
	            	);
					echo json_encode($response);
            	} 
            	else 
            	{
            		$response = array(
	            		'status'=>201,
	            		'msg'=>'Apps updated successfully!'
	            	);
					echo json_encode($response);
            	}
            }
            else
             {
            	$response = array(
            		'status'=>503,
            		'msg'=>'Apps to create Customer!'
            	);
				echo json_encode($response);
            }
        }
        else
         {
        	$response = array(
            		'status'=>400,
            		'msg'=>'Unable to create Apps. Data is incomplete!'
            	);
			echo json_encode($response);
        }
    }

	// store end

	// Delete data
	// public function delete() {

	// 	// Get ID
	// 	$id = $this->input->post('id');

	// 	// store file that will be delated
	// 	$files = $this->aboutus_model->get_where_files('about_us','id',$id);

	// 	if ($this->aboutus_model->delete($id)) {

	// 		// delete file from directory
	// 		unlink_file($files);
        	
 //        	$response = array(
 //        		'status'=>200,
 //        		'msg'=>'About deleted successfully!'
 //        	);
	// 		echo json_encode($response);
 //        }
 //        else {
 //        	$response = array(
 //        		'status'=>503,
 //        		'msg'=>'Unable to delete About!'
 //        	);
	// 		echo json_encode($response);
 //        }
	// }

}