<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Service extends CI_Controller {
	function __construct()
	{
		parent::__construct();

        // Load Stuff
		$this->load->model('service_model');
		$this->load->model('category_model');
		$this->load->helper('form');
		$this->load->library('form_validation');	
	}

	public function index()
	{
		// Load css and js file
		add_js(array(
					// 'admin/inspinia/form.js',
					'admin/service.js'
				));
		
		// Load view file
		$data['subview'] = 'service/index_view';
		$this->load->view('inspinia/layout/_layout_main',$data);
	}
	// Get All/id serviced  json data 
	public function ajax_get_service($id = NULL)
	{
		// required headers
		header("Access-Control-Allow-Origin: *");
		header("Content-Type: application/json; charset=UTF-8");

		$service['data'] = $this->service_model->get_service($id);

		if (!empty($id)) {
			
			$response = array(
        		'status'=>200,
        		'msg'=>'Service Find by ID!',
        		'data'=> $service['data']
        	);
			echo json_encode($response);
		} 
		else {

		    $response = array(
        		'status'=>200,
        		'msg'=>'Service List Find!',
        		'data'=> $service['data']
        	);
			echo json_encode($response);
		}
	}

	//geting data from category table
	 public function ajax_get_category($id = NULL)
	{
		// required headers
		header("Access-Control-Allow-Origin: *");
		header("Content-Type: application/json; charset=UTF-8");

		$cat['data'] = $this->category_model->get_category($id);

		if (!empty($id)) {
			
			$response = array(
        		'status'=>200,
        		'msg'=>'Category Find by ID!',
        		'data'=> $cat['data']
        	);
			echo json_encode($response);
		} 
		else {

		    $response = array(
        		'status'=>200,
        		'msg'=>'Category List Find!',
        		'data'=> $cat['data']
        	);
			echo json_encode($response);
		}
	}
	
	//geting data from subcategory table
	 public function ajax_get_subcategory($pid=NULL)
	{
		// required headers
		header("Access-Control-Allow-Origin: *");
		header("Content-Type: application/json; charset=UTF-8");

		$subcat['data'] = $this->category_model->get_subcategory($pid);
		// dump($subcat,TRUE);
		if (!empty($pid)) {
			
			$response = array(
        		'status'=>200,
        		'msg'=>'Subcategory Find by ID!',
        		'data'=> $subcat['data']
        	);
			echo json_encode($response);
		} 
		else {

		    $response = array(
        		'status'=>200,
        		'msg'=>'Subcategory List Find!',
        		'data'=> $subcat['data']
        	);
			echo json_encode($response);
		}
	}
	// Servicw - edit(id) - Service
	public function edit($id = NULL)
	{
		// Load css and js file
		
		add_js('admin/service.js');

		// Load view file
		$data['subview'] = 'service/edit_view';
		$this->load->view('inspinia/layout/_layout_main',$data);
	}


	//Store
	public function store($id = NULL)
	{

		// dump($_POST,true);
		// required headers
		header("Content-Type: application/json; charset=UTF-8");

        // Set up the form
        $rules = $this->service_model->rules;
        $this->form_validation->set_rules($rules);
		
		// Process the form
        if($this->form_validation->run() == TRUE) {
            $data = $this->service_model->array_from_post(array('id','name','code','type_id','category_id','sub_category_id','unit_id','is_active'));
         
            $data['is_active'] = isset($data['is_active'])? 1 : 0;
            $data['sub_category_id'] = isset($data['sub_category_id'])? $data['sub_category_id'] : 0;
            // dump($data,true);

            if (isset($data['id'])) {
            	$id = $data['id'];
            } 
            else {
            	$id = NULL;
            }

             
            if ($this->service_model->save($data, $id)) {
            	if (empty($id)) 
            	{
            		$response = array(
	            		'status'=>200,
	            		'msg'=>'Service added successfully!'
	            	);
					echo json_encode($response);
            	} 
            	else 
            	{
            		$response = array(
	            		'status'=>201,
	            		'msg'=>'Service updated successfully!'
	            	);
					echo json_encode($response);
            	}
            }
            else
             {
            	$response = array(
            		'status'=>503,
            		'msg'=>'Service to create Customer!'
            	);
				echo json_encode($response);
            }
        }
        else
         {
        	$response = array(
            		'status'=>400,
            		'msg'=>'Unable to create Service. Data is incomplete!'
            	);
			echo json_encode($response);
        }
    }

// store end

	// Delete data
	public function delete() {

		$id = $this->input->post('id');

		if ($this->service_model->delete($id)) {
        	$response = array(
        		'status'=>200,
        		'msg'=>'Service deleted successfully!'
        	);
			echo json_encode($response);
        	
        }
        else {
        	$response = array(
        		'status'=>503,
        		'msg'=>'Service to delete Customer!'
        	);
			echo json_encode($response);
        }
	}


}