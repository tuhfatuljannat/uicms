<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Warehouse extends CI_Controller {
	function __construct()
	{
		parent::__construct();

        // Load Stuff
		$this->load->model('warehouse_model');
		$this->load->model('user_model');	
	}

	public function index()
	{
		// Load css and js file
		add_js(array(
					// 'admin/inspinia/form.js',
					'admin/warehouse.js'
				));
		
		// Load view file
		$data['subview'] = 'warehouse/index_view';
		$this->load->view('inspinia/layout/_layout_main',$data);
	}
	// Get All/id warehouse  json data 
	public function ajax_get_warehouse($id = NULL)
	{
		// required headers
		header("Access-Control-Allow-Origin: *");
		header("Content-Type: application/json; charset=UTF-8");

		$warehouse['data'] = $this->warehouse_model->get_warehouse($id);

		if (!empty($id)) {
			
			$response = array(
        		'status'=>200,
        		'msg'=>'Warehouse Find by ID!',
        		'data'=> $warehouse['data']
        	);
			echo json_encode($response);
		} 
		else {

		    $response = array(
        		'status'=>200,
        		'msg'=>'Warehouse List Find!',
        		'data'=> $warehouse['data']
        	);
			echo json_encode($response);
		}
	}


	// warehouse - edit(id) - warehouse
	public function edit($id = NULL)
	{
		// Load css and js file
		
		add_js('admin/warehouse.js');

		// Load view file
		$data['subview'] = 'warehouse/edit_view';
		$this->load->view('inspinia/layout/_layout_main',$data);
	}

	public function ajax_get_users($id = NULL)
	{
		// required headers
		header("Access-Control-Allow-Origin: *");
		header("Content-Type: application/json; charset=UTF-8");

		$users['data'] = $this->user_model->get_users($id);

		if (!empty($id)) {
			
			$response = array(
        		'status'=>200,
        		'msg'=>'User Find by ID!',
        		'data'=> $users['data']
        	);
			echo json_encode($response);
		} 
		else {

		    $response = array(
        		'status'=>200,
        		'msg'=>'User List Find!',
        		'data'=> $users['data']
        	);
			echo json_encode($response);
		}
	}

	//Store
	public function store($id = NULL)
	{

		// dump($_POST,true);
		// required headers
		header("Content-Type: application/json; charset=UTF-8");

        // Set up the form
        $rules = $this->warehouse_model->rules;
        $this->form_validation->set_rules($rules);
		
		// Process the form
        if($this->form_validation->run() == TRUE) {
            $data = $this->warehouse_model->array_from_post(array('id','name','code','type_id','mobile','email','address','user_id','is_active'));
         
            $data['is_active'] = isset($data['is_active'])? 1 : 0;
            // dump($data,true);

            if (isset($data['id'])) {
            	$id = $data['id'];
            } 
            else {
            	$id = NULL;
            }

             
            if ($this->warehouse_model->save($data, $id)) {
            	if (empty($id)) 
            	{
            		$response = array(
	            		'status'=>200,
	            		'msg'=>'Warehouse added successfully!'
	            	);
					echo json_encode($response);
            	} 
            	else 
            	{
            		$response = array(
	            		'status'=>201,
	            		'msg'=>'Warehouse updated successfully!'
	            	);
					echo json_encode($response);
            	}
            }
            else
             {
            	$response = array(
            		'status'=>503,
            		'msg'=>'Warehouse to create Customer!'
            	);
				echo json_encode($response);
            }
        }
        else
         {
        	$response = array(
            		'status'=>400,
            		'msg'=>'Unable to create Warehouse. Data is incomplete!'
            	);
			echo json_encode($response);
        }
    }

// store end

	// Delete data
	public function delete() {

		$id = $this->input->post('id');

		if ($this->warehouse_model->delete($id)) {
        	$response = array(
        		'status'=>200,
        		'msg'=>'Warehouse deleted successfully!'
        	);
			echo json_encode($response);
        	
        }
        else {
        	$response = array(
        		'status'=>503,
        		'msg'=>'Warehouse to delete Customer!'
        	);
			echo json_encode($response);
        }
	}


}