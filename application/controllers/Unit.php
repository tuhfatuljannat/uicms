

<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Unit extends CI_Controller {

	/**
	 *  unit Controller
	 */
	
	function __construct()
	{
		parent::__construct();

        // Load Stuff
		$this->load->model('unit_model');
		$this->load->model('type_model');
	}

	// unit - index() - List
	public function index()
	{
		// Load css and js file
		add_js('admin/unit.js');

		// Load view file
		$data['subview'] = 'unit/unit_index';
		$this->load->view('inspinia/layout/_layout_main',$data);
	}

	
	// Get All/id unit json data 
	public function ajax_get_unit($id = NULL)
		{
			// required headers
			header("Access-Control-Allow-Origin: *");
			header("Content-Type: application/json; charset=UTF-8");

			// Get unit data
			$unit['data'] = $this->unit_model->get_unit($id);

			// Check ID null or Not
			if (!empty($id)) {
				$response = array(
	        		'status'=>200,
	        		'msg'=>'Unit Find by ID!',
	        		'data'=> $unit['data']
	        	);
				echo json_encode($response);
			} 
			else {
			    $response = array(
	        		'status'=>200,
	        		'msg'=>'Unit List Find!',
	        		'data'=> $unit['data']
	        	);
				echo json_encode($response);
			}
		}

	
	// Store Data
	public function store($id = NULL)
	{
		// required headers
		header("Content-unit: application/json; charset=UTF-8");
		
		// Set up the form
        $rules = $this->unit_model->rules;
        $this->form_validation->set_rules($rules);

		// Process the form
        if($this->form_validation->run() == TRUE) {
        	$data = $this->unit_model->array_from_post(array('id','name','code','type_id','is_active'));

            $data['is_active'] = isset($data['is_active'])? 1 : 0;


            // dump($data, TRUE);
        	// Get ID 
        	if (isset($data['id'])) { $id = $data['id']; } else { $id = NULL; }
			
			// save - update
            if ($this->unit_model->save($data, $id)) {
            	if (empty($id)) {
            		$response = array(
	            		'status'=>200,
	            		'msg'=>'Unit added successfully!'
	            	);
					echo json_encode($response);
            	} 
            	else {
            		$response = array(
	            		'status'=>201,
	            		'msg'=>'Unit updated successfully!'
	            	);
					echo json_encode($response);
            	}
            }
            else {
            	$response = array(
            		'status'=>503,
            		'msg'=>'Unable to creat Unit!'
            	);
				echo json_encode($response);
            }
        }
        else {
        	$response = array(
            		'status'=>400,
            		'msg'=>'Unable to create unit. Data is incomplete!'
            	);
			echo json_encode($response);
        }
	}
	// Delete data
	public function delete() {

		// Get ID
		$id = $this->input->post('id');

		if ($this->unit_model->delete($id)) {
			$response = array(
				'status'=>200,
				'msg'=>'Unit deleted successfully!'
			);
			echo json_encode($response);
			
		}
		else {
			$response = array(
				'status'=>503,
				'msg'=>'Unable to delete Unit!'
			);
			echo json_encode($response);
		}
	}

}