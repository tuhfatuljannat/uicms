
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Type extends CI_Controller {

	/**
	 *  Type Controller
	 */
	
	function __construct()
	{
		parent::__construct();

        // Load Stuff
		$this->load->model('type_model');
	}

	// Type - index() - List
		public function index()
		{
			// Load css and js file
			add_js('admin/type.js');

			// Load view file
			$data['subview'] = 'type/type_index';
			$this->load->view('inspinia/layout/_layout_main',$data);
		}
	
	// Get All/id Type json data 
		public function ajax_get_type($id = NULL)
		{
			// required headers
			header("Access-Control-Allow-Origin: *");
			header("Content-Type: application/json; charset=UTF-8");

			// Get Type data
			$type['data'] = $this->type_model->get_type($id);

			// Check ID null or Not
			if (!empty($id)) {
				
				$response = array(
	        		'status'=>200,
	        		'msg'=>'Type Find by ID!',
	        		'data'=> $type['data']
	        	);
				echo json_encode($response);
			} 
			else {

			    $response = array(
	        		'status'=>200,
	        		'msg'=>'Type List Find!',
	        		'data'=> $type['data']
	        	);
				echo json_encode($response);
			}
		}

	// Store Data
		public function store($id = NULL)
		{
			// required headers
			header("Content-Type: application/json; charset=UTF-8");
			
			// Set up the form
	        $rules = $this->type_model->rules;
	        $this->form_validation->set_rules($rules);

			// Process the form
	        if($this->form_validation->run() == TRUE) {
	        	$data = $this->type_model->array_from_post(array('id','name','code','declared_for','is_active'));
	            $data['is_active'] = isset($data['is_active'])? 1 : 0;

	        	// Get ID 
	        	if (isset($data['id'])) { $id = $data['id']; } else { $id = NULL; }
				
				// save - update
	            if ($this->type_model->save($data, $id)) {
	            	if (empty($id)) {
	            		$response = array(
		            		'status'=>200,
		            		'msg'=>'Type added successfully!'
		            	);
						echo json_encode($response);
	            	} 
	            	else {
	            		$response = array(
		            		'status'=>201,
		            		'msg'=>'Type updated successfully!'
		            	);
						echo json_encode($response);
	            	}
	            }
	            else {
	            	$response = array(
	            		'status'=>503,
	            		'msg'=>'Unable to create Type!'
	            	);
					echo json_encode($response);
	            }
	        }
	        else {
	        	$response = array(
	            		'status'=>400,
	            		'msg'=>'Unable to create type. Data is incomplete!'
	            	);
				echo json_encode($response);
	        }
		}

	// Delete data
		public function delete() {

			// Get ID
			$id = $this->input->post('id');

			if ($this->type_model->delete($id)) {
				$response = array(
					'status'=>200,
					'msg'=>'Type deleted successfully!'
				);
				echo json_encode($response);
				
			}
			else {
				$response = array(
					'status'=>503,
					'msg'=>'Unable to delete Type!'
				);
				echo json_encode($response);
			}
		}

}