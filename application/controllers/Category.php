<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Category extends CI_Controller {

	/**
	 * Category Controler 
	 */
	
	// Autoload
		function __construct()
		{
			parent::__construct();

	        // Load Stuff
			$this->load->model('category_model');
			$this->load->helper('form');
			$this->load->library('form_validation');
		}

	// Category - index() - List
		public function index()
		{
			// Load css and js file
			add_js(array(
						// 'admin/inspinia/form.js',
						'admin/category.js'
					));
			
			// Load view file
			$data['subview'] = 'category/category_index';
			$this->load->view('inspinia/layout/_layout_main',$data);
		}

	// Get All/id Category json data 
		public function ajax_get_category($id = NULL)
		{
			// required headers
			header("Access-Control-Allow-Origin: *");
			header("Content-Type: application/json; charset=UTF-8");

			// Get category data
			// $category['data'] = $this->category_model->get_category($id);
			$category['data'] = $this->category_model->get_with_parent($id);
			// dump($category, TRUE);

			// Check ID null or Not
			if (!empty($id)) {
				$response = array(
	        		'status'=>200,
	        		'msg'=>'Category Find by ID!',
	        		'data'=> $category['data']
	        	);
				echo json_encode($response);
			} 
			else {
			    $response = array(
	        		'status'=>200,
	        		'msg'=>'Category List Find!',
	        		'data'=> $category['data']
	        	);
				echo json_encode($response);
			}
		}

	// Category - edit(id) - Edit User
		public function edit($id = NULL)
		{
			// Load css and js file
			add_js('admin/category.js');

			// Load view file
			$data['subview'] = 'category/category_edit';
			$this->load->view('inspinia/layout/_layout_main',$data);
		}

	//Store
		public function store($id = NULL)
		{

			// dump($_POST,true);
			// required headers
			header("Content-Type: application/json; charset=UTF-8");

	        // Set up the form
	        $rules = $this->category_model->rules;
	        $this->form_validation->set_rules($rules);
			
			// Process the form
	        if($this->form_validation->run() == TRUE) {
	            $data = $this->category_model->array_from_post(array('id','name','code','pid','sort_order','prefix','type_id','is_active'));
	            
	           $data['is_active'] = isset($data['is_active'])? 1 : 0;

	            // dump($data,true);

	            if (isset($data['id'])) {
	            	$id = $data['id'];
	            } 
	            else {
	            	$id = NULL;
	            }

	             
	            if ($this->category_model->save($data, $id)) {
	            	if (empty($id)) {
	            		$response = array(
		            		'status'=>200,
		            		'msg'=>'Category added successfully!'
		            	);
						echo json_encode($response);
	            	} 
	            	else {
	            		$response = array(
		            		'status'=>201,
		            		'msg'=>'Category updated successfully!'
		            	);
						echo json_encode($response);
	            	}
	            }
	            else {
	            	$response = array(
	            		'status'=>503,
	            		'msg'=>'Unable to create Category!'
	            	);
					echo json_encode($response);
	            }
	        }
	        else {
	        	$response = array(
	            		'status'=>400,
	            		'msg'=>'Unable to create Category. Data is incomplete!'
	            	);
				echo json_encode($response);
	        }
	    }

	// Delete data
		public function delete() {

			// Get ID
			$id = $this->input->post('id');


			if ($this->category_model->delete($id)) {
	        	$response = array(
	        		'status'=>200,
	        		'msg'=>'Category deleted successfully!'
	        	);
				echo json_encode($response);
	        }
	        else {
	        	$response = array(
	        		'status'=>503,
	        		'msg'=>'Unable to delete Category!'
	        	);
				echo json_encode($response);
	        }
		}
}