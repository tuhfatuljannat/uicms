
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Othersetup extends CI_Controller {

	/**
	 *  othersetup Controller
	 */
	
	function __construct()
	{
		parent::__construct();

        // Load Stuff
		$this->load->model('othersetup_model');
	}

	// othersetup - index() - List
	public function index()
	{
		// Load css and js file
		add_js('admin/othersetup.js');

		// Load view file
		$data['subview'] = 'other_setup/othersetup_index';
		$this->load->view('inspinia/layout/_layout_main',$data);
	}
	
	// Get All/id othersetup json data 
	public function ajax_get_othersetup($id = NULL)
	{
		// required headers
		header("Access-Control-Allow-Origin: *");
		header("Content-othersetup: application/json; charset=UTF-8");

		// Get othersetup data
		$othersetup['data'] = $this->othersetup_model->get_othersetup($id);

		// Check ID null or Not
		if (!empty($id)) {
			
			$response = array(
				'status'=>200,
				'msg'=>'othersetup Find by ID!',
				'data'=> $othersetup['data']
			);
			echo json_encode($response);
		} 
		else {

			$response = array(
				'status'=>200,
				'msg'=>'othersetup List Find!',
				'data'=> $othersetup['data']
			);
			echo json_encode($response);
		}
	}


	// Store Data
	public function store($id = NULL)
	{
		// required headers
		header("Content-Othersetup: application/json; charset=UTF-8");
		
		// Set up the form
		$rules = $this->othersetup_model->rules;
		$this->form_validation->set_rules($rules);

		// Process the form
		if($this->form_validation->run() == TRUE) {
			$data = $this->othersetup_model->array_from_post(array('id','name','declared_for','is_active'));

            // $data['permission'] = implode(",", $data['permission']);
			$data['is_active'] = isset($data['is_active'])? 1 : 0;


            // dump($data, TRUE);
        	// Get ID 
			if (isset($data['id'])) { $id = $data['id']; } else { $id = NULL; }
			
			// save - update
			if ($this->othersetup_model->save($data, $id)) {
				if (empty($id)) {
					$response = array(
						'status'=>200,
						'msg'=>'Othersetup added successfully!'
					);
					echo json_encode($response);
				} 
				else {
					$response = array(
						'status'=>201,
						'msg'=>'Othersetup updated successfully!'
					);
					echo json_encode($response);
				}
			}
			else {
				$response = array(
					'status'=>503,
					'msg'=>'Unable to create Othersetup!'
				);
				echo json_encode($response);
			}
		}
		else {
			$response = array(
				'status'=>400,
				'msg'=>'Unable to create Othersetup. Data is incomplete!'
			);
			echo json_encode($response);
		}
	}
	// Delete data
	public function delete() {

		// Get ID
		$id = $this->input->post('id');

		if ($this->othersetup_model->delete($id)) {
			$response = array(
				'status'=>200,
				'msg'=>'Othersetup deleted successfully!'
			);
			echo json_encode($response);
			
		}
		else {
			$response = array(
				'status'=>503,
				'msg'=>'Unable to delete Othersetup!'
			);
			echo json_encode($response);
		}
	}

}