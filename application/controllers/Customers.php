<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Customers extends CI_Controller {

	/**
	 * Customer Controler 
	 */
	
	// Autoload
		function __construct()
		{
			parent::__construct();

	        // Load Stuff
			$this->load->model('customer_model');
			$this->load->helper('form');
			$this->load->library('form_validation');
		}

	// Customer - index() - List
		public function index()
		{
			// Load css and js file
			add_js(array(
						// 'admin/inspinia/form.js',
						'admin/customer.js'
					));
			
			// Load view file
			$data['subview'] = 'customer/customer_index';
			$this->load->view('inspinia/layout/_layout_main',$data);
		}

	// Get All/id customer json data 
		public function ajax_get_customers($id = NULL)
		{
			// required headers
			header("Access-Control-Allow-Origin: *");
			header("Content-Type: application/json; charset=UTF-8");

			// Get Customer data
			$customer['data'] = $this->customer_model->get_customers($id);

			// Check ID null or Not
			if (!empty($id)) {
				$response = array(
	        		'status'=>200,
	        		'msg'=>'Customer Find by ID!',
	        		'data'=> $customer['data']
	        	);
				echo json_encode($response);
			} 
			else {
			    $response = array(
	        		'status'=>200,
	        		'msg'=>'Customer List Find!',
	        		'data'=> $customer['data']
	        	);
				echo json_encode($response);
			}
		}

	// Customer - edit(id) - Edit User
		public function edit($id = NULL)
		{
			// Load css and js file
			add_js('admin/customer.js');

			// Load view file
			$data['subview'] = 'customer/customer_edit';
			$this->load->view('inspinia/layout/_layout_main',$data);
		}

	//Store
		public function store($id = NULL)
		{
			// dump($_POST,true);
			// required headers
			header("Content-Type: application/json; charset=UTF-8");

	        // Set up the form
	        $rules = $this->customer_model->rules;
	        $this->form_validation->set_rules($rules);
			
			// Process the form
	        if($this->form_validation->run() == TRUE) {
	            $data = $this->customer_model->array_from_post(array('id','code','name','mobile','email','address','product_service','is_active'));
	            $data['product_service'] = implode(",", $data['product_service']);
	            
	           $data['is_active'] = isset($data['is_active'])? 1 : 0;

	            // dump($data,true);

	            if (isset($data['id'])) {
	            	$id = $data['id'];
	            } 
	            else {
	            	$id = NULL;
	            }

	             
	            if ($this->customer_model->save($data, $id)) {
	            	if (empty($id)) {
	            		$response = array(
		            		'status'=>200,
		            		'msg'=>'Customer added successfully!'
		            	);
						echo json_encode($response);
	            	} 
	            	else {
	            		$response = array(
		            		'status'=>201,
		            		'msg'=>'Customer updated successfully!'
		            	);
						echo json_encode($response);
	            	}
	            }
	            else {
	            	$response = array(
	            		'status'=>503,
	            		'msg'=>'Unable to create Customer!'
	            	);
					echo json_encode($response);
	            }
	        }
	        else {
	        	$response = array(
	            		'status'=>400,
	            		'msg'=>'Unable to create Customer. Data is incomplete!'
	            	);
				echo json_encode($response);
	        }
	    }

	// Delete data
		public function delete() {

			// Get ID
			$id = $this->input->post('id');


			if ($this->customer_model->delete($id)) {
	        	$response = array(
	        		'status'=>200,
	        		'msg'=>'Customer deleted successfully!'
	        	);
				echo json_encode($response);
	        }
	        else {
	        	$response = array(
	        		'status'=>503,
	        		'msg'=>'Unable to delete Customer!'
	        	);
				echo json_encode($response);
	        }
		}
}