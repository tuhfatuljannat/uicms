<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Repo extends CI_Controller {

	/**
	 * Test Repo for config, helper, library and others ..
	 */
	
	public function index()
	{
		$this->load->view('repo/repo_index');
	}
}