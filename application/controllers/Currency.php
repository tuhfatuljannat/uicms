<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Currency extends CI_Controller {

	/**
	 * Currency Controller
	 */
	
	function __construct()
	{
		parent::__construct();

        // Load Stuff
		$this->load->model('currency_model');
	}

	// currency - index() - List
	public function index()
	{
		// Load css and js file
		add_js('admin/currency.js');

		// Load view file
		$data['subview'] = 'currency/currency_index';
		$this->load->view('inspinia/layout/_layout_main',$data);
	}
	
	// Get All/id currency json data 
	public function ajax_get_currency($id = NULL)
	{
		// required headers
		header("Access-Control-Allow-Origin: *");
		header("Content-Type: application/json; charset=UTF-8");

		// Get currency data
		$currency['data'] = $this->currency_model->get_currency($id);

		// Check ID null or Not
		if (!empty($id)) {
			
			$response = array(
        		'status'=>200,
        		'msg'=>'Currency Find by ID!',
        		'data'=> $currency['data']
        	);
			echo json_encode($response);
		} 
		else {

		    $response = array(
        		'status'=>200,
        		'msg'=>'Currency List Find!',
        		'data'=> $currency['data']
        	);
			echo json_encode($response);
		}
	}


	// Store Data
	public function store($id = NULL)
	{
		// required headers
		header("Content-Type: application/json; charset=UTF-8");
		
		// Set up the form
        $rules = $this->currency_model->rules;
        $this->form_validation->set_rules($rules);

		// Process the form
        if($this->form_validation->run() == TRUE) {
        	$data = $this->currency_model->array_from_post(array('id','name','code','symbol','exchange_rate','is_active'));
            $data['is_active'] = isset($data['is_active'])? 1 : 0;

        	// Get ID 
        	if (isset($data['id'])) { $id = $data['id']; } else { $id = NULL; }
			
			// save - update
            if ($this->currency_model->save($data, $id)) {
            	if (empty($id)) {
            		$response = array(
	            		'status'=>200,
	            		'msg'=>'Currency added successfully!'
	            	);
					echo json_encode($response);
            	} 
            	else {
            		$response = array(
	            		'status'=>201,
	            		'msg'=>'Currency updated successfully!'
	            	);
					echo json_encode($response);
            	}
            }
            else {
            	$response = array(
            		'status'=>503,
            		'msg'=>'Unable to create Currency!'
            	);
				echo json_encode($response);
            }
        }
        else {
        	$response = array(
            		'status'=>400,
            		'msg'=>'Unable to create Currency. Data is incomplete!'
            	);
			echo json_encode($response);
        }
	}

	// Delete data
	public function delete() {

		// Get ID
		$id = $this->input->post('id');

		if ($this->currency_model->delete($id)) {
			$response = array(
				'status'=>200,
				'msg'=>'Currency deleted successfully!'
			);
			echo json_encode($response);
			
		}
		else {
			$response = array(
				'status'=>503,
				'msg'=>'Unable to delete Currency!'
			);
			echo json_encode($response);
		}
	}

}