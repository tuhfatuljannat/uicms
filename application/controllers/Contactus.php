


<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Contactus extends CI_Controller {

	/**
	 *  contactus Controller
	 */
	
	function __construct()
	{
		parent::__construct();

        // Load Stuff
		$this->load->model('contactus_model');
	}

	// contactus - index() - List
		public function index()
		{
			// Load css and js file
			add_js('admin/contactus.js');

			// Load view file
			$data['subview'] = 'contactus/index_view';
			$this->load->view('inspinia/layout/_layout_main',$data);
		}
	
	// Get All/id contactus json data 
		public function ajax_get_contactus($id = NULL)
		{
			// required headers
			header("Access-Control-Allow-Origin: *");
			header("Content-contactus: application/json; charset=UTF-8");

			// Get contactus data
			$contactus['data'] = $this->contactus_model->get_contactus($id);

			// Check ID null or Not
			if (!empty($id)) {
				
				$response = array(
	        		'status'=>200,
	        		'msg'=>'contactus Find by ID!',
	        		'data'=> $contactus['data']
	        	);
				echo json_encode($response);
			} 
			else {

			    $response = array(
	        		'status'=>200,
	        		'msg'=>'contactus List Find!',
	        		'data'=> $contactus['data']
	        	);
				echo json_encode($response);
			}
		}

	// Store Data
		// public function store($id = NULL)
		// {
		// 	// required headers
		// 	header("Content-contactus: application/json; charset=UTF-8");
			
		// 	// Set up the form
	 //        $rules = $this->contactus_model->rules;
	 //        $this->form_validation->set_rules($rules);

		// 	// Process the form
	 //        if($this->form_validation->run() == TRUE) {
	 //        	$data = $this->contactus_model->array_from_post(array('id','name','email','declared_for','is_active'));
	 //            $data['is_active'] = isset($data['is_active'])? 1 : 0;

	 //        	// Get ID 
	 //        	if (isset($data['id'])) { $id = $data['id']; } else { $id = NULL; }
				
		// 		// save - update
	 //            if ($this->contactus_model->save($data, $id)) {
	 //            	if (empty($id)) {
	 //            		$response = array(
		//             		'status'=>200,
		//             		'msg'=>'contactus added successfully!'
		//             	);
		// 				echo json_encode($response);
	 //            	} 
	 //            	else {
	 //            		$response = array(
		//             		'status'=>201,
		//             		'msg'=>'contactus updated successfully!'
		//             	);
		// 				echo json_encode($response);
	 //            	}
	 //            }
	 //            else {
	 //            	$response = array(
	 //            		'status'=>503,
	 //            		'msg'=>'Unable to create contactus!'
	 //            	);
		// 			echo json_encode($response);
	 //            }
	 //        }
	 //        else {
	 //        	$response = array(
	 //            		'status'=>400,
	 //            		'msg'=>'Unable to create contactus. Data is incomplete!'
	 //            	);
		// 		echo json_encode($response);
	 //        }
		// }

	// Delete data
		// public function delete() {

		// 	// Get ID
		// 	$id = $this->input->post('id');

		// 	if ($this->contactus_model->delete($id)) {
		// 		$response = array(
		// 			'status'=>200,
		// 			'msg'=>'contactus deleted successfully!'
		// 		);
		// 		echo json_encode($response);
				
		// 	}
		// 	else {
		// 		$response = array(
		// 			'status'=>503,
		// 			'msg'=>'Unable to delete contactus!'
		// 		);
		// 		echo json_encode($response);
		// 	}
		// }

}