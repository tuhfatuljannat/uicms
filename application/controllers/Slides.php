<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Slides extends CI_Controller {

//autoload
	function __construct()
	{
		parent::__construct();

        // Load Stuff
        $this->load->model('slide_model');
		$this->load->helper('form');
		$this->load->library('form_validation');	
	}

// slides index
	public function index()
	{
		if (!$this->slide_model->loggedin()) {
			redirect('login');
		}
		
		// Load css and js file
		add_js(array(
					// 'admin/inspinia/form.js',
					'admin/slide.js'
				));
		
		// Load view file
		$data['subview'] = 'slide/index_view';
		$this->load->view('inspinia/layout/_layout_main',$data);
	}

// Get All/id Slides  json data 
	public function ajax_get_slides($id = NULL)
	{
		// required headers
		header("Access-Control-Allow-Origin: *");
		header("Content-Type: application/json; charset=UTF-8");

		$slide['data'] = $this->slide_model->get_slides($id);

		if (!empty($id)) {
			
			$response = array(
        		'status'=>200,
        		'msg'=>'Slide  Find by ID!',
        		'data'=> $slide['data']
        	);
			echo json_encode($response);
		} 
		else {

		    $response = array(
        		'status'=>200,
        		'msg'=>'Slide List Find!',
        		'data'=> $slide['data']
        	);
			echo json_encode($response);
		}
	}


// Edit Slides
	public function edit($id = NULL)
	{
		// Load css and js file
		add_js('admin/slide.js');
		
		// Load view file
		$data['subview'] = 'slide/edit_view';
		$this->load->view('inspinia/layout/_layout_main',$data);
	}

//Store
	public function store($id = NULL)
	{

		// dump($_POST,true);
		// required headers
		header("Content-Type: application/json; charset=UTF-8");

        // Set up the form
        $rules = $this->slide_model->rules;
        $this->form_validation->set_rules($rules);
		
		// Process the form
        if($this->form_validation->run() == TRUE) {
            $data = $this->slide_model->array_from_post(array('id','title','description','is_active'));

	        $data['is_active'] = isset($data['is_active'])? 1 : 0;

         
           
            // dump($data,true);

            // Check File Empty or Not
			$defaultImg = FALSE;
			if (!empty($_FILES['file']['name'])) 
			{
				// File upload library loaded
				$this->load->library('upload');
	            $config['upload_path'] = './public/file_manager/';
	            $config['allowed_types'] = 'gif|jpg|jpeg|png';
	            // $config['allowed_types'] = 'gif|jpg|jpeg|png|iso|dmg|zip|rar|doc|docx|xls|xlsx|ppt|pptx|csv|ods|odt|odp|pdf|rtf|sxc|sxi|txt|exe|avi|mpeg|mp3|mp4|3gp';
	            $config['encrypt_name'] = TRUE;
	            $config['max_size'] = 1024 * 8;
	            $config['overwrite'] = FALSE;
	            $this->upload->initialize($config);

	            // File upload
				if(!$this->upload->do_upload('file')) {
	            	$response = array(
	            		'status'=>400,
	            		'msg'=>$this->upload->display_errors()
	            	);
					echo json_encode($response);
	            }
	            else {
	            	$about['fileinfo'] = $this->upload->data();

	            	// $data['file_name'] =  $this->upload->data('file_name');
	            	$data['file_name'] =  $about['fileinfo']['file_name'];
	            	$data['file_ext'] =  $about['fileinfo']['file_ext'];
	            }
			} 
			else {
				$defaultImg = TRUE;
			}
			
			// Set Default Image
			if ($defaultImg == TRUE && empty($data['id'])) 
			{
				$data['file_name'] =  'slide.png';
            	$data['file_ext'] =  'default';
			} 

        	// ------------------------------------------------------------------
        	// Get ID 
        	if (isset($data['id'])) { $id = $data['id']; } else { $id = NULL; }

            // store file that will be delated
			$files = $this->slide_model->get_where_files('slides','id',$id);

            if (isset($data['id'])) {
            	$id = $data['id'];
            } 
            else {
            	$id = NULL;
            }
            // save data
            if ($this->slide_model->save($data, $id)) {
            	if (empty($id)) 
            	{
            		$response = array(
	            		'status'=>200,
	            		'msg'=>'Slide added successfully!'
	            	);
					echo json_encode($response);
            	} 
            	else 
            	{
            		$response = array(
	            		'status'=>201,
	            		'msg'=>'Slide updated successfully!'
	            	);
					echo json_encode($response);
            	}
            }
            else
             {
            	$response = array(
            		'status'=>503,
            		'msg'=>'Slide to create Customer!'
            	);
				echo json_encode($response);
            }
        }
        else
         {
        	$response = array(
            		'status'=>400,
            		'msg'=>'Unable to create Slide. Data is incomplete!'
            	);
			echo json_encode($response);
        }
    }
	
// Delete
	public function delete()
	{

		// Get ID
		$id = $this->input->post('id');

		// store file that will be delated
		$files = $this->slide_model->get_where_files('slides','id',$id);

		if ($this->slide_model->delete($id)) {

			// delete file from directory
			unlink_file($files);
        	
        	$response = array(
        		'status'=>200,
        		'msg'=>'Slide deleted successfully!'
        	);
			echo json_encode($response);
        }
        else {
        	$response = array(
        		'status'=>503,
        		'msg'=>'Unable to delete Slide!'
        	);
			echo json_encode($response);
        }
	}
	
}