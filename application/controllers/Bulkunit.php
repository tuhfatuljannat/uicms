

<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Bulkunit extends CI_Controller {

	/**
	 *  bulkunit Controller
	 */
	
	function __construct()
	{
		parent::__construct();

        // Load Stuff
		$this->load->model('bulkunit_model');
		$this->load->model('type_model');
	}

	// bulkunit - index() - List
	public function index()
	{
		// Load css and js file
		add_js('admin/bulkunit.js');

		// Load view file
		$data['subview'] = 'bulk_unit/bulkunit_index';
		$this->load->view('inspinia/layout/_layout_main',$data);
	}

	
	// Get All/id bulkunit json data 
	public function ajax_get_bulkunit($id = NULL)
		{
			// required headers
			header("Access-Control-Allow-Origin: *");
			header("Content-Type: application/json; charset=UTF-8");

			// Get bulkunit data
			$bulkunit['data'] = $this->bulkunit_model->get_bulkunit($id);

			// Check ID null or Not
			if (!empty($id)) {
				$response = array(
	        		'status'=>200,
	        		'msg'=>'bulkUnit Find by ID!',
	        		'data'=> $bulkunit['data']
	        	);
				echo json_encode($response);
			} 
			else {
			    $response = array(
	        		'status'=>200,
	        		'msg'=>'bulkUnit List Find!',
	        		'data'=> $bulkunit['data']
	        	);
				echo json_encode($response);
			}
		}

	
	// Store Data
	public function store($id = NULL)
	{
		// required headers
		header("Content-bulkunit: application/json; charset=UTF-8");
		
		// Set up the form
        $rules = $this->bulkunit_model->rules;
        $this->form_validation->set_rules($rules);

		// Process the form
        if($this->form_validation->run() == TRUE) {
        	$data = $this->bulkunit_model->array_from_post(array('id','name','code','type_id','is_active'));

            // $data['permission'] = implode(",", $data['permission']);
            $data['is_active'] = isset($data['is_active'])? 1 : 0;


            // dump($data, TRUE);
        	// Get ID 
        	if (isset($data['id'])) { $id = $data['id']; } else { $id = NULL; }
			
			// save - update
            if ($this->bulkunit_model->save($data, $id)) {
            	if (empty($id)) {
            		$response = array(
	            		'status'=>200,
	            		'msg'=>'Bulk_Unit added successfully!'
	            	);
					echo json_encode($response);
            	} 
            	else {
            		$response = array(
	            		'status'=>201,
	            		'msg'=>'Bulk_Unit updated successfully!'
	            	);
					echo json_encode($response);
            	}
            }
            else {
            	$response = array(
            		'status'=>503,
            		'msg'=>'Unable to creat BulkUnit!'
            	);
				echo json_encode($response);
            }
        }
        else {
        	$response = array(
            		'status'=>400,
            		'msg'=>'Unable to create bulkunit. Data is incomplete!'
            	);
			echo json_encode($response);
        }
	}
	// Delete data
	public function delete() {

		// Get ID
		$id = $this->input->post('id');

		if ($this->bulkunit_model->delete($id)) {
			$response = array(
				'status'=>200,
				'msg'=>'BulkUnit deleted successfully!'
			);
			echo json_encode($response);
			
		}
		else {
			$response = array(
				'status'=>503,
				'msg'=>'Unable to delete BulkUnit!'
			);
			echo json_encode($response);
		}
	}

}