<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Shelf extends CI_Controller {
	function __construct()
	{
		parent::__construct();

        // Load Stuff
         $this->load->model('shelf_model');
        $this->load->model('block_model');
		$this->load->model('warehouse_model');
		$this->load->model('user_model');
		$this->load->helper('form');
		$this->load->library('form_validation');	
	}

	public function index()
	{
		// Load css and js file
		
		add_js(array(
					// 'admin/inspinia/form.js',
					'admin/shelf.js'
				));
		
		// Load view file
		$data['subview'] = 'shelf/index_view';
		$this->load->view('inspinia/layout/_layout_main',$data);
	}

	// Get All/id block  json data 
	public function ajax_get_shelf($id = NULL)
	{
		// required headers
		header("Access-Control-Allow-Origin: *");
		header("Content-Type: application/json; charset=UTF-8");

		$shelf['data'] = $this->shelf_model->get_shelf($id);

		if (!empty($id)) {
			
			$response = array(
        		'status'=>200,
        		'msg'=>'Shelf Find by ID!',
        		'data'=> $shelf['data']
        	);
			echo json_encode($response);
		} 
		else {

		    $response = array(
        		'status'=>200,
        		'msg'=>'Shelf List Find!',
        		'data'=> $shelf['data']
        	);
			echo json_encode($response);
		}
	}

	


	public function ajax_get_users($id = NULL)
	{
		// required headers
		header("Access-Control-Allow-Origin: *");
		header("Content-Type: application/json; charset=UTF-8");

		$users['data'] = $this->user_model->get_users($id);

		if (!empty($id)) {
			
			$response = array(
        		'status'=>200,
        		'msg'=>'User Find by ID!',
        		'data'=> $users['data']
        	);
			echo json_encode($response);
		} 
		else {

		    $response = array(
        		'status'=>200,
        		'msg'=>'User List Find!',
        		'data'=> $users['data']
        	);
			echo json_encode($response);
		}
	}

//Store
	public function store($id = NULL)
	{

		// dump($_POST,true);
		// required headers
		header("Content-Type: application/json; charset=UTF-8");

        // Set up the form
        $rules = $this->shelf_model->rules;
        $this->form_validation->set_rules($rules);
		
		// Process the form
        if($this->form_validation->run() == TRUE) {
            $data = $this->shelf_model->array_from_post(array('id','name','code','block_id','warehouse_id','user_id','is_active'));
         
             $data['is_active'] = isset($data['is_active'])? 1 : 0;
            // dump($data,true);

            if (isset($data['id'])) {
            	$id = $data['id'];
            } 
            else {
            	$id = NULL;
            }

             
            if ($this->shelf_model->save($data, $id)) {
            	if (empty($id)) 
            	{
            		$response = array(
	            		'status'=>200,
	            		'msg'=>'Shelf added successfully!'
	            	);
					echo json_encode($response);
            	} 
            	else 
            	{
            		$response = array(
	            		'status'=>201,
	            		'msg'=>'Shelf updated successfully!'
	            	);
					echo json_encode($response);
            	}
            }
            else
             {
            	$response = array(
            		'status'=>503,
            		'msg'=>'Shelf to create Customer!'
            	);
				echo json_encode($response);
            }
        }
        else
         {
        	$response = array(
            		'status'=>400,
            		'msg'=>'Unable to create Shelf. Data is incomplete!'
            	);
			echo json_encode($response);
        }
    }

// store end




	// Delete data
	public function delete() {

		$id = $this->input->post('id');

		if ($this->shelf_model->delete($id)) {
        	$response = array(
        		'status'=>200,
        		'msg'=>'Shelf deleted successfully!'
        	);
			echo json_encode($response);
        	
        }
        else {
        	$response = array(
        		'status'=>503,
        		'msg'=>'Shelf to delete Customer!'
        	);
			echo json_encode($response);
        }
	}


}