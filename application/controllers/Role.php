<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Role extends CI_Controller {

	/**
	 * Role Controler 
	 */
	
	function __construct()
	{
		parent::__construct();

        // Load Stuff
		$this->load->model('role_model');
	}

	// Role - index() - List
	public function index()
	{
		// Load css and js file
		add_js('admin/role.js');

		// Load view file
		$data['subview'] = 'role/role_index';
		$this->load->view('inspinia/layout/_layout_main',$data);
	}

	// Get All/id Role json data 
	public function ajax_get_roles($id = NULL)
	{
		// required headers
		header("Access-Control-Allow-Origin: *");
		header("Content-Type: application/json; charset=UTF-8");

		// Get Role data
		$role['data'] = $this->role_model->get_roles($id);

		// Check ID null or Not
		if (!empty($id)) {
			
			$response = array(
        		'status'=>200,
        		'msg'=>'Role Find by ID!',
        		'data'=> $role['data']
        	);
			echo json_encode($response);
		} 
		else {

		    $response = array(
        		'status'=>200,
        		'msg'=>'Role List Find!',
        		'data'=> $role['data']
        	);
			echo json_encode($response);
		}
	}

	// Store Data
	public function store($id = NULL)
	{
		// required headers
		header("Content-Type: application/json; charset=UTF-8");
		
		// Set up the form
        $rules = $this->role_model->rules;
        $this->form_validation->set_rules($rules);

		// Process the form
        if($this->form_validation->run() == TRUE) {
        	$data = $this->role_model->array_from_post(array('id','name','permission','is_active'));

            $data['permission'] = implode(",", $data['permission']);
            $data['is_active'] = isset($data['is_active'])? 1 : 0;


            // dump($data, TRUE);
        	// Get ID 
        	if (isset($data['id'])) { $id = $data['id']; } else { $id = NULL; }
			
			// save - update
            if ($this->role_model->save($data, $id)) {
            	if (empty($id)) {
            		$response = array(
	            		'status'=>200,
	            		'msg'=>'Role added successfully!'
	            	);
					echo json_encode($response);
            	} 
            	else {
            		$response = array(
	            		'status'=>201,
	            		'msg'=>'Role updated successfully!'
	            	);
					echo json_encode($response);
            	}
            }
            else {
            	$response = array(
            		'status'=>503,
            		'msg'=>'Unable to create Role!'
            	);
				echo json_encode($response);
            }
        }
        else {
        	$response = array(
            		'status'=>400,
            		'msg'=>'Unable to create Role. Data is incomplete!'
            	);
			echo json_encode($response);
        }
	}

	// Delete data
	public function delete() {

		// Get ID
		$id = $this->input->post('id');

		if ($this->role_model->delete($id)) {
        	$response = array(
        		'status'=>200,
        		'msg'=>'Role deleted successfully!'
        	);
			echo json_encode($response);
        }
        else {
        	$response = array(
        		'status'=>503,
        		'msg'=>'Unable to delete Role!'
        	);
			echo json_encode($response);
        }
	}
}