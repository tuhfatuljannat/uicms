
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Services extends CI_Controller {

	function __construct()
	{
		parent::__construct();

        // Load Stuff
		$this->load->model('services_model');
		
		$this->load->helper('form');
		$this->load->library('form_validation');	
	}

	public function index(){
		if (!$this->services_model->loggedin()) {
			redirect('login');
		}
		
		add_js('admin/services.js');
		// Load view file
		$data['subview'] = 'services/index_view';
		$this->load->view('inspinia/layout/_layout_main',$data);
	}

	// Get All/id services  json data 
	public function ajax_get_services($id = NULL){
		// required headers
		header("Access-Control-Allow-Origin: *");
		header("Content-Type: application/json; charset=UTF-8");

		$services['data'] = $this->services_model->get_services($id);

		if (!empty($id)) {
			
			$response = array(
				'status'=>200,
				'msg'=>'Services Find by ID!',
				'data'=> $services['data']
			);
			echo json_encode($response);
		} 
		else {

			$response = array(
				'status'=>200,
				'msg'=>'Services List Find!',
				'data'=> $services['data']
			);
			echo json_encode($response);
		}
	}

	// Edit Service
	public function edit($id = NULL)
	{
		add_js('admin/services.js');

		// Load view file
		$data['subview'] = 'services/edit_view';
		$this->load->view('inspinia/layout/_layout_main',$data);
	}

	//Store
	public function store($id = NULL){

		// dump($_POST,true);
		// required headers
		header("Content-Type: application/json; charset=UTF-8");

        // Set up the form
		$rules = $this->services_model->rules;
		$this->form_validation->set_rules($rules);
		
		// Process the form
		if($this->form_validation->run() == TRUE) {
			$data = $this->services_model->array_from_post(array('id','title','description','icon','is_active'));

			$data['is_active'] = isset($data['is_active'])? 1 : 0;
            // dump($data,true);

			if (isset($data['id'])) {
				$id = $data['id'];
			} 
			else {
				$id = NULL;
			}


			if ($this->services_model->save($data, $id)) {
				if (empty($id)) 
				{
					$response = array(
						'status'=>200,
						'msg'=>'Services added successfully!'
					);
					echo json_encode($response);
				} 
				else 
				{
					$response = array(
						'status'=>201,
						'msg'=>'Services updated successfully!'
					);
					echo json_encode($response);
				}
			}
			else
			{
				$response = array(
					'status'=>503,
					'msg'=>'Services to create Customer!'
				);
				echo json_encode($response);
			}
		}
		else
		{
			$response = array(
				'status'=>400,
				'msg'=>'Unable to create services. Data is incomplete!'
			);
			echo json_encode($response);
		}
	}

	// store end

	// Delete data
	public function delete() {

		$id = $this->input->post('id');

		if ($this->services_model->delete($id)) {
			$response = array(
				'status'=>200,
				'msg'=>'Services deleted successfully!'
			);
			echo json_encode($response);

		}
		else {
			$response = array(
				'status'=>503,
				'msg'=>'Services to delete Customer!'
			);
			echo json_encode($response);
		}
	}


}