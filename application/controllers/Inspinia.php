<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Inspinia extends CI_Controller {

	/**
	 * Inspinia Admin Theme Customization 
	 */
	
	function __construct()
	{
		parent::__construct();

        // Load Stuff
		$this->load->model('user_model');
	}

	public function index()
	{
		$data['subview'] = 'inspinia/components/empty_page';
		$this->load->view('inspinia/layout/_layout_main',$data);
	}

	public function table()
	{
		
		// Load css and js file
		add_lib_css('inspinia/css/plugins/dataTables/datatables.min.css');
		add_lib_js(array(
					'inspinia/js/plugins/dataTables/datatables.min.js',
					'inspinia/js/plugins/dataTables/dataTables.bootstrap4.min.js'
				));
		add_js('admin/inspinia/table.js');

		// Load view file
		$data['subview'] = 'inspinia/components/data_table';
		$this->load->view('inspinia/layout/_layout_main',$data);
	}

	public function form()
	{
		
		// Load css and js file
		add_lib_css(array(
					'inspinia/css/plugins/iCheck/custom.css',
					'inspinia/css/plugins/awesome-bootstrap-checkbox/awesome-bootstrap-checkbox.css'
				));
		
		add_lib_js('inspinia/js/plugins/iCheck/icheck.min.js');
		add_js('admin/inspinia/form.js');

		// Load view file
		$data['subview'] = 'inspinia/components/form';
		$this->load->view('inspinia/layout/_layout_main',$data);
	}

	public function user()
	{
		// Load css and js file
		add_lib_css('inspinia/css/plugins/iCheck/custom.css');
		add_lib_js('inspinia/js/plugins/iCheck/icheck.min.js');
		add_css('admin/admin.css');
		add_js(array(
					'admin/inspinia/form.js',
					'admin/inspinia/user.js'
				));
		
		// Load view file
		$data['subview'] = 'inspinia/users/user_index';
		$this->load->view('inspinia/layout/_layout_main',$data);
	}

	public function store() {

		// dump($_POST, TRUE);
		
        // Set up the form
        // $rules = $this->user_model->rules;

        // $this->form_validation->set_rules($rules);

        // Process the form
        // if($this->form_validation->run() == TRUE) {
            $data = $this->user_model->array_from_post(array('id','name','mobile','email','address'));
            dump($data);
            // $this->user_model->save($data, $id);
            // $this->session->set_flashdata('msg', $msg);
            // redirect('inspinia/user');
        // }

        // Load view
        // $data['subview'] = 'inspinia/users/user_index';
		// $this->load->view('inspinia/layout/_layout_main',$data);
    }

	public function ajax_get_users($id = NULL)
	{
		// $users = $this->users_model->get_users();
		$users['data'] = $this->user_model->get_users($id);

		echo json_encode($users);
	}

	public function ajax_del_user()
	{
		// $users = $this->users_model->get_users();
		$users['data'] = $this->user_model->delete($id);

		echo json_encode($users);
	}
}