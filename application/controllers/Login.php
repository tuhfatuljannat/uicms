<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

	function __construct()
	{
		parent::__construct();
        // Load Stuff
        $this->load->model('login_model');
	}

	public function index()
	{
		// add_js('admin/login.js');
		$this->load->view('login/index_view');
	}


    public function validate_login()
    {
        // dump($_POST,True);

        header("Content-bulkunit: application/json; charset=UTF-8");

        $rules = $this->login_model->rules;
        $this->form_validation->set_rules($rules);
 
        if ($this->form_validation->run() === TRUE)
        {  

            $data = $this->login_model->array_from_post(array('email','password'));
            // dump($data,true);
   
            $check = $this->login_model->checkLogin($data['email'], $data['password']);
            // dump($check,true);

            if($check)
            {
                $user = array(
                    'id' => $check->id,
                    'name' => $check->name,
                    'file_name' => $check->file_name,
                    'email' =>$data['email'],
                    'loggedin' => TRUE
                );
                // dump($user,true);
                $this->session->set_userdata($user);
                $response = array(
                        'status'=>200,
                        'msg'=>'Logged in Successfully.'

                    );
                echo json_encode($response);
            }
            else{
                $response = array(
                        'status'=>503,
                        'msg'=>'Invalide Email or Password'

                    );
                echo json_encode($response);
            }
        }
    } 

    public function logout() {
        $this->session->sess_destroy();
        redirect('login');
    }

    
	

	
}