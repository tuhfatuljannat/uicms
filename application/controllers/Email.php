<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Email extends CI_Controller {
	function __construct()
	{
		parent::__construct();

        // Load Stuff
        // $this->load->model('email_model');
        $this->load->library('email');
		$this->load->library('session');
		$this->email->from('email@example.com', 'Identification');
    	$this->email->to('emailto@example.com');
    	$this->email->subject('Send Email Codeigniter');
    	$this->email->message('The email send using codeigniter library');
		$this->load->helper('form');
		$this->load->library('form_validation');	
	}

	public function index()
	{
		// Load css and js file
		
		add_js(array(
					// 'admin/inspinia/form.js',
					'admin/email.js'
				));
		
		// Load view file
		$data['subview'] = 'email/index_view';
		$this->load->view('inspinia/layout/_layout_main',$data);
		// $this->load->library('email');
		$config = array();
		$config['protocol'] = 'smtp';
		$config['smtp_host'] = 'xxx';
		$config['smtp_user'] = 'xxx';
		$config['smtp_pass'] = 'xxx';
		$config['smtp_port'] = 25;
		$this->email->initialize($config);
		$this->email->set_newline("\r\n");
	}
	public function send_mail() {
        $from_email = "email@example.com";
        $to_email = $this->input->post('email');
        //Load email library
        $this->load->library('email');
        $this->email->from($from_email, 'Identification');
        $this->email->to($to_email);
        $this->email->subject('Send Email Codeigniter');
        $this->email->message('The email send using codeigniter library');
        //Send mail
        if($this->email->send())
            $this->session->set_flashdata("email_sent","Congragulation Email Send Successfully.");
        else
            $this->session->set_flashdata("email_sent","You have encountered an error");
        $this->load->view('contact_email_form');
    }


	








}