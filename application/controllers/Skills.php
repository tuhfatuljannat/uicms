<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Skills extends CI_Controller {

	function __construct()
	{
		parent::__construct();

        // Load Stuff
        $this->load->model('skills_model');
		$this->load->helper('form');
		$this->load->library('form_validation');	
	}

	public function index(){
		if (!$this->skills_model->loggedin()) {
			redirect('login');
		}
		add_js(array(
					// 'admin/inspinia/form.js',
					'admin/skills.js'
				));
		
		// Load view file
		$data['subview'] = 'skills/index_view';
		$this->load->view('inspinia/layout/_layout_main',$data);
	}

	// Get All/id skills  json data 
	public function ajax_get_skills($id = NULL){
		// required headers
		header("Access-Control-Allow-Origin: *");
		header("Content-Type: application/json; charset=UTF-8");
		$skills['data'] = $this->skills_model->get_skills($id);
		if (!empty($id)) {
			
			$response = array(
        		'status'=>200,
        		'msg'=>'Skills Find by ID!',
        		'data'=> $skills['data']
        	);
			echo json_encode($response);
		} 
		else {

		    $response = array(
        		'status'=>200,
        		'msg'=>'Skills List Find!',
        		'data'=> $skills['data']
        	);
			echo json_encode($response);
		}
	}

	// Edit skills
	public function edit($id = NULL)
	{
		// Load css and js file
		add_js('admin/skills.js');

		// Load view file
		$data['subview'] = 'skills/edit_view';
		$this->load->view('inspinia/layout/_layout_main',$data);
	}


	//Store
	public function store($id = NULL){
		// dump($_POST,true);
		// required headers
		header("Content-Type: application/json; charset=UTF-8");

        // Set up the form
        $rules = $this->skills_model->rules;
        $this->form_validation->set_rules($rules);
		
		// Process the form
        if($this->form_validation->run() == TRUE) {
            $data = $this->skills_model->array_from_post(array('id','title','description','progress','bg_color','is_active'));
            $data['is_active'] = isset($data['is_active'])? 1 : 0;

            // dump($data,true);

            if (isset($data['id'])) {
            	$id = $data['id'];
            } 
            else {
            	$id = NULL;
            }
            if ($this->skills_model->save($data, $id)) {
            	if (empty($id)) 
            	{
            		$response = array(
	            		'status'=>200,
	            		'msg'=>'Skills added successfully!'
	            	);
					echo json_encode($response);
            	} 
            	else 
            	{
            		$response = array(
	            		'status'=>201,
	            		'msg'=>'Skills updated successfully!'
	            	);
					echo json_encode($response);
            	}
            }
            else
             {
            	$response = array(
            		'status'=>503,
            		'msg'=>'Skills to create Customer!'
            	);
				echo json_encode($response);
            }
        }
        else
         {
        	$response = array(
            		'status'=>400,
            		'msg'=>'Unable to create Skills. Data is incomplete!'
            	);
			echo json_encode($response);
        }
    }

	// store end

	// Delete data
	public function delete() {
		$id = $this->input->post('id');
		if ($this->skills_model->delete($id)) {
        	$response = array(
        		'status'=>200,
        		'msg'=>'Skills deleted successfully!'
        	);
			echo json_encode($response);	
        }
        else {
        	$response = array(
        		'status'=>503,
        		'msg'=>'Skills to delete Customer!'
        	);
			echo json_encode($response);
        }
	}
}