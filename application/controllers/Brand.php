<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Brand extends CI_Controller {

	/**
	 * brand Controller
	 */
	
	function __construct()
	{
		parent::__construct();

        // Load Stuff
		$this->load->model('brand_model');
	}

	// brand - index() - List
	public function index()
	{
		// Load css and js file
		add_js('admin/brand.js');

		// Load view file
		$data['subview'] = 'brand/brand_index';
		$this->load->view('inspinia/layout/_layout_main',$data);
	}
	
	// Get All/id brand json data 
	public function ajax_get_brand($id = NULL)
	{
		// required headers
		header("Access-Control-Allow-Origin: *");
		header("Content-Type: application/json; charset=UTF-8");

		// Get brand data
		$brand['data'] = $this->brand_model->get_brand($id);

		// Check ID null or Not
		if (!empty($id)) {
			
			$response = array(
        		'status'=>200,
        		'msg'=>'Brand Find by ID!',
        		'data'=> $brand['data']
        	);
			echo json_encode($response);
		} 
		else {

		    $response = array(
        		'status'=>200,
        		'msg'=>'Brand List Find!',
        		'data'=> $brand['data']
        	);
			echo json_encode($response);
		}
	}


	// Store Data
	public function store($id = NULL)
	{
		// required headers
		header("Content-Type: application/json; charset=UTF-8");
		
		// Set up the form
        $rules = $this->brand_model->rules;
        $this->form_validation->set_rules($rules);

		// Process the form
        if($this->form_validation->run() == TRUE) {
        	$data = $this->brand_model->array_from_post(array('id','name','description','is_active'));

            // $data['permission'] = implode(",", $data['permission']);
            $data['is_active'] = isset($data['is_active'])? 1 : 0;


            // dump($data, TRUE);
        	// Get ID 
        	if (isset($data['id'])) { $id = $data['id']; } else { $id = NULL; }
			
			// save - update
            if ($this->brand_model->save($data, $id)) {
            	if (empty($id)) {
            		$response = array(
	            		'status'=>200,
	            		'msg'=>'Brand added successfully!'
	            	);
					echo json_encode($response);
            	} 
            	else {
            		$response = array(
	            		'status'=>201,
	            		'msg'=>'Brand updated successfully!'
	            	);
					echo json_encode($response);
            	}
            }
            else {
            	$response = array(
            		'status'=>503,
            		'msg'=>'Unable to create Brand!'
            	);
				echo json_encode($response);
            }
        }
        else {
        	$response = array(
            		'status'=>400,
            		'msg'=>'Unable to create brand. Data is incomplete!'
            	);
			echo json_encode($response);
        }
	}

	// Delete data
	public function delete() {

		// Get ID
		$id = $this->input->post('id');

		if ($this->brand_model->delete($id)) {
			$response = array(
				'status'=>200,
				'msg'=>'Brand deleted successfully!'
			);
			echo json_encode($response);
			
		}
		else {
			$response = array(
				'status'=>503,
				'msg'=>'Unable to delete Brand!'
			);
			echo json_encode($response);
		}
	}

}