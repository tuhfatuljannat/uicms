<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Product extends CI_Controller {

	/**
	 * Product Controler 
	 */
	
	// Autoload
		function __construct()
		{
			parent::__construct();

	        // Load Stuff
	        $this->load->model('product_model');
		}

	// Product - index() - List
		public function index()
		{
			// Load css and js file
			add_css('admin/product.css');
			add_js(array(
						// 'admin/inspinia/form.js',
						'admin/product.js'
					));
			
			// Load view file
			$data['subview'] = 'product/product_index';
			$this->load->view('inspinia/layout/_layout_main',$data);
		}

	//Store
		public function store($id = NULL)
		{
			dump($_POST,true);
			// required headers
			header("Content-Type: application/json; charset=UTF-8");

	        // Set up the form
	        $rules = $this->product_model->rules;
	        $this->form_validation->set_rules($rules);
			
			// Process the form
	        if($this->form_validation->run() == TRUE) {
	            $data = $this->product_model->array_from_post(array('id','name','code','type_id','product_type','category_id','sub_category_id',
	            	'brand_id','unit_id','bulk_unit_id','dim_in','dim_op','weight_id','pack_in','pack_op','gross_weight_id','building_parts','add_custom_field',
	            	'add_parts','add_service'));

	            $data['code'] = implode(",", $data['code']);
	            
	            $data['dim_in'] = implode(",", $data['dim_in']);
	            $data['pack_in'] = implode(",", $data['pack_in']);
	            $data['building_parts'] = implode(",", $data['building_parts']);

	            $data['add_parts'] = isset($data['add_parts']) ? implode(",", $data['add_parts']) : '';
	            $data['add_service'] = isset($data['add_parts']) ? implode(",", $data['add_service']) : '';
	            $data['add_custom_field'] = implode(",", $data['add_custom_field']);
	            
	           // $data['is_active'] = isset($data['is_active'])? 1 : 0;

	            // dump($data,true);

	            if (isset($data['id'])) {
	            	$id = $data['id'];
	            } 
	            else {
	            	$id = NULL;
	            }

	             
	            if ($this->product_model->save($data, $id)) {
	            	if (empty($id)) {
	            		$response = array(
		            		'status'=>200,
		            		'msg'=>'Product added successfully!'
		            	);
						echo json_encode($response);
	            	} 
	            	else {
	            		$response = array(
		            		'status'=>201,
		            		'msg'=>'Product updated successfully!'
		            	);
						echo json_encode($response);
	            	}
	            }
	            else {
	            	$response = array(
	            		'status'=>503,
	            		'msg'=>'Unable to create Product!'
	            	);
					echo json_encode($response);
	            }
	        }
	        else {
	        	$response = array(
	            		'status'=>400,
	            		'msg'=>'Unable to create Product. Data is incomplete!'
	            	);
				echo json_encode($response);
	        }
	    }

	// Get All/id product json data 
		public function ajax_get_products($id = NULL)
		{
			// required headers
			header("Access-Control-Allow-Origin: *");
			header("Content-Type: application/json; charset=UTF-8");

			// Get Product data
			$product['data'] = $this->product_model->get_products($id);

			// Check ID null or Not
			if (!empty($id)) {
				$response = array(
	        		'status'=>200,
	        		'msg'=>'Product Find by ID!',
	        		'data'=> $product['data']
	        	);
				echo json_encode($response);
			} 
			else {
			    $response = array(
	        		'status'=>200,
	        		'msg'=>'Product List Find!',
	        		'data'=> $product['data']
	        	);
				echo json_encode($response);
			}
		}

	// Product - edit(id) - Edit User
		public function edit($id = NULL)
		{
			// Load css and js file
			add_css('admin/product.css');
			add_js('admin/product.js');

			// Load view file
			$data['subview'] = 'product/product_edit';
			$this->load->view('inspinia/layout/_layout_main',$data);
		}






}