-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: May 14, 2020 at 06:50 PM
-- Server version: 10.1.9-MariaDB
-- PHP Version: 5.6.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `cms_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `portfolio`
--

CREATE TABLE `portfolio` (
  `id` int(100) NOT NULL,
  `title` varchar(100) NOT NULL,
  `description` text NOT NULL,
  `category` varchar(100) NOT NULL,
  `file_name` varchar(255) NOT NULL,
  `file_ext` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `portfolio`
--

INSERT INTO `portfolio` (`id`, `title`, `description`, `category`, `file_name`, `file_ext`) VALUES
(1, 'App 1', 'APP', 'app', '5e50fdb92d25beb8c930f95d6504690f.jpg', '.jpg'),
(2, 'Web 3', 'WEB', 'web', '9b7b955830d7adca9d8de495c9b0ebdf.jpg', '.jpg'),
(4, 'App 2', 'APP', 'app', 'e2516cac22b61289bf73ba075f58ef38.jpg', '.jpg'),
(5, 'Card 2', 'CARD', 'card', '6fa8e9b6e8d8c53fd25c3313680fd79a.jpg', '.jpg'),
(6, 'Web 2', 'WEB', 'web', '30bea2ae6f5de5090f1504d4074885e0.jpg', '.jpg'),
(7, 'App 3', 'APP', 'app', 'fbd1a27e2b01254c6d101db2c3199376.jpg', '.jpg'),
(8, 'Card 1', 'CARD', 'card', '7cc526b017e126c5516e73b4eb284588.jpg', '.jpg'),
(9, 'Card 3', 'CARD', 'card', 'a20fb41d105ee8b1b9a1bbd7f83b991e.jpg', '.jpg'),
(12, 'Web 1', 'WEB', 'web', 'aefd1cecd501c62d73c754b5c81596ef.jpg', '.jpg');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `portfolio`
--
ALTER TABLE `portfolio`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `portfolio`
--
ALTER TABLE `portfolio`
  MODIFY `id` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
