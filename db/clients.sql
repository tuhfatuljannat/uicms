-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: May 14, 2020 at 06:49 PM
-- Server version: 10.1.9-MariaDB
-- PHP Version: 5.6.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `cms_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `clients`
--

CREATE TABLE `clients` (
  `id` int(11) NOT NULL,
  `name` varchar(30) NOT NULL,
  `file_name` varchar(255) NOT NULL,
  `file_ext` varchar(255) NOT NULL,
  `is_active` int(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `clients`
--

INSERT INTO `clients` (`id`, `name`, `file_name`, `file_ext`, `is_active`) VALUES
(22, 'ggh', '046d37c989f47092a990b4c4bf81c4a3.png', '.png', 1),
(23, 'ul3', 'e55c4ed31ff3362bff5a8c5a5b38222c.png', '.png', 1),
(24, 'ul4', '6a5d330f295310c06d0212b01668dade.png', '.png', 1),
(25, 'ul5', '739fe9fe255c0d8a3f1809c923c69791.png', '.png', 1),
(26, 'uk', 'fc610409bb2d50a82cfb93ec0bc1b5e8.png', '.png', 1),
(27, 'tt', '7387c6e0c9e02f67860947616c4cc4b0.png', '.png', 1),
(28, 'ff', '42ab9cc59644019d28d4b2d376955617.png', '.png', 0),
(29, 'aa', '94a40678ea6e94b725aedfd68730fa88.png', '.png', 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `clients`
--
ALTER TABLE `clients`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `clients`
--
ALTER TABLE `clients`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
