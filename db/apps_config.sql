-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: May 14, 2020 at 06:49 PM
-- Server version: 10.1.9-MariaDB
-- PHP Version: 5.6.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `cms_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `apps_config`
--

CREATE TABLE `apps_config` (
  `id` int(50) NOT NULL,
  `company_name` varchar(100) NOT NULL,
  `company_desc` text NOT NULL,
  `file_name` varchar(255) NOT NULL,
  `file_ext` varchar(255) NOT NULL,
  `company_addr` varchar(100) NOT NULL,
  `company_mobile` varchar(100) NOT NULL,
  `company_email` varchar(100) NOT NULL,
  `about_intro` text NOT NULL,
  `service_intro` text NOT NULL,
  `skill_intro` text NOT NULL,
  `team_intro` text NOT NULL,
  `contact_intro` text NOT NULL,
  `fb` text NOT NULL,
  `twt` text NOT NULL,
  `inst` text NOT NULL,
  `gplus` text NOT NULL,
  `link_in` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `apps_config`
--

INSERT INTO `apps_config` (`id`, `company_name`, `company_desc`, `file_name`, `file_ext`, `company_addr`, `company_mobile`, `company_email`, `about_intro`, `service_intro`, `skill_intro`, `team_intro`, `contact_intro`, `fb`, `twt`, `inst`, `gplus`, `link_in`) VALUES
(1, 'BizPage', 'Minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat tarad limino ata', '7cbcead90e10cde428489580be576e26.jpg', '.jpg', 'A108 Adam Street, NY 535022, USA', '+1 5589 55488 55', 'info@example.com', ' ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.', ' sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.', '\r\n\r\nSed perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque', 'Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque', 'bispage', 'bizpage', 'bizpage', 'Bizpage', 'Bizpage');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `apps_config`
--
ALTER TABLE `apps_config`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `apps_config`
--
ALTER TABLE `apps_config`
  MODIFY `id` int(50) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
