-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: May 14, 2020 at 06:50 PM
-- Server version: 10.1.9-MariaDB
-- PHP Version: 5.6.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `cms_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `team_member`
--

CREATE TABLE `team_member` (
  `id` int(100) NOT NULL,
  `name` varchar(100) NOT NULL,
  `designation` varchar(100) NOT NULL,
  `file_name` varchar(255) NOT NULL,
  `file_ext` varchar(255) NOT NULL,
  `link_fb` text NOT NULL,
  `link_twt` text NOT NULL,
  `link_in` text NOT NULL,
  `link_gp` text NOT NULL,
  `is_active` int(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `team_member`
--

INSERT INTO `team_member` (`id`, `name`, `designation`, `file_name`, `file_ext`, `link_fb`, `link_twt`, `link_in`, `link_gp`, `is_active`) VALUES
(7, 'Walter White', 'Chief Executive Officer', '236f604f62cbce4ec281ab7e42644d03.jpg', '.jpg', 'hgfh', 'fhgfjg', 'gfjhgj', 'hjc', 0),
(10, 'Sarah Jhonson', 'Product Manager', '630ee0c9456fa00879391aacb8198253.jpg', '.jpg', 'fdsfds', 'dfds', 'dsfd', 'jmjhk', 1),
(13, 'William Anderson', 'CTO', '1199e61c2088f9ddc60e753149c93f92.jpg', '.jpg', 'reshmi.fb', 'reshmi.twt', 'reshmi.linkdin', 'reshmi.gp', 0),
(15, 'Amanda Jepson', 'Accountant', '6c0ba16cbc85d4eb79638cc358bffba6.jpg', '.jpg', 'ffd', 'fdgfgfklill', 'fdgfd', 'fdgfd', 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `team_member`
--
ALTER TABLE `team_member`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `team_member`
--
ALTER TABLE `team_member`
  MODIFY `id` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
