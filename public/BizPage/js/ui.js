$(document).ready(function() {

    console.log('Checked! [BizPage/js/custom.js]');

    // Datatable Load - Users - index()
    slideList();
    aboutusList();
    teamList();
    settingsList();
    portfolioList();
    servicesList();
    clientsList();
    skillsList();
    services_is_shownList();

    // Edit - Show user data by ID
       // $(document).on('click', '.btn-edit', function(e) {

       //      e.preventDefault();

       //      var id = $(this).data("id");
       //      get_ui_by_id(id);
       //  });

    // SAVE
    $(document).on('click', '#form-contactus', function() {

            // e.preventDefault();
            // console.log('prevented!');
            var form = $("#form-contactus");
            $(form).validate({
                rules: {

                    'name': {
                        required: true,
                        minlength: 4
                    },
                    'email': {
                        required: true,   
                    },
                    'subject': {
                        required: true,
                        minlength: 4

                    },
                    'message': {
                        required: true,

                    }
                },
                submitHandler: function(form) {

                    var url = baseUrl+"/ui/store";
                    
                    $.ajax({
                        url: url,
                        type: "POST",
                        dataType: "JSON",
                        data: new FormData(form),
                        processData: false,
                        contentType: false,
                        cache: false,
                        async: false,
                        success: function (res)
                        {
                            // console.log(res);
                            if (res.status === 200 || res.status === 201) {
                                toast_notify('success',res.msg);
                            } 
                            else if (res.status === 503 || res.status === 400) {
                                toast_notify('error',res.msg);
                            }
                            $('#form-contactus').trigger("reset");
                            // $(".dataTables-ui").empty();
                            //  uiList(); 
                        },
                        error: function (xhr, textStatus, thrownError) {
                            var msg = 'Sorry but there was an error: ';
                            $('#error').html(msg + xhr.status + ' ' + xhr.statusText); 
                            console.log(xhr.status);
                            console.log(textStatus);
                            console.log(thrownError);
                        }
                    });
                }
            });   
        });
    
    // Delete
        // $(document).on('click', '.btn-del', function(e){

        //     e.preventDefault();
        //     var id = $(this).data("id");
        //     var url = baseUrl+"/ui/delete";

        //     $.ajax({
        //         url: url,
        //         type: "POST",
        //         dataType: "JSON",
        //         data: {id:id},
        //         success: function (res)
        //         {
        //             // console.log(res);
        //             toast_notify('error',res.msg);
        //              $(".dataTables-ui").empty();
        //                 uiList();

        //         },
        //         error: function (xhr, textStatus, thrownError) {
        //             var msg = 'Sorry but there was an error: ';
        //             $('#error').html(msg + xhr.status + ' ' + xhr.statusText); 
        //             console.log(xhr.status);
        //             console.log(textStatus);
        //             console.log(thrownError);
        //         }
        //     });
        // });
    });

//slides list
function slideList() {
    $.ajax({
        type: 'GET',
        // url: baseUrl+'/ui/ajax_get_ui_slide',
        url: baseUrl+'/ui/ajax_get_ui_slide',  
        dataType: 'json',
        async: false,
        cache: false,
        success: function (data, textStatus, xhr) 
        {
         // console.log(data.data[0].title);
         // console.log(data);
         // console.log(data.data[0].mobile);
         // var html = "";
         // $('#show').empty();   
         $.each(data.data, function(entryIndex, entry)
         { 
                // console.log(entryIndex);
                var active  = (entryIndex==0)?'active' : '';
                var html = "";
                html +="<div class=\"carousel-item "+active+"\">";
                html +="<div class=\"carousel-background\"><img src=\""+baseUrl+"/public/file_manager/"+entry.file_name+"\" alt=\"\"></div>";
                html +="<div class=\"carousel-container\">";
                html +="<div class=\"carousel-content\">";
                html +="<h2>"+entry.title+"</h2>";
                html +="<p>"+entry.description+"</p>";
                html +="<a href=\"#featured-services\" class=\"btn-get-started scrollto\">Get Started</a>";
                html +="</div>";
                html +="</div>";
                html +="</div>";
                html +="</div>";
                $('.show').append(html);
            });

            // Intro carousel
            var introCarousel = $(".carousel");
            var introCarouselIndicators = $(".carousel-indicators");
            introCarousel.find(".carousel-inner").children(".carousel-item").each(function(index) {
                (index === 0) ?
                introCarouselIndicators.append("<li data-target='#introCarousel' data-slide-to='" + index + "' class='active'></li>") :
                introCarouselIndicators.append("<li data-target='#introCarousel' data-slide-to='" + index + "'></li>");

                $(this).css("background-image", "url('" + $(this).children('.carousel-background').children('img').attr('src') +"')");
                $(this).children('.carousel-background').remove();
            });

            $(".carousel").swipe({
                swipe: function(event, direction, distance, duration, fingerCount, fingerData) {
                  if (direction == 'left') $(this).carousel('next');
                  if (direction == 'right') $(this).carousel('prev');
              },
              allowPageScroll:"vertical"
          });
        },
        error: function (xhr, textStatus, thrownError) 
        {
            var msg = 'Sorry but there was an error: ';
            $('#error').html(msg + xhr.status + ' ' + xhr.statusText); 
            console.log(xhr.status);
            console.log(textStatus);
            console.log(thrownError);
            // $('#box p').empty();    

        }
    }); 
}


//about us list
function aboutusList() {
    $.ajax({
        type: 'GET',
        url: baseUrl+'/ui/ajax_get_aboutus',  
        dataType: 'json',
        async: false,
        cache: false,
        success: function (data, textStatus, xhr) 
        {
            // console.log(data.data[2].file_name);
            // console.log(data);
            // console.log(data.data[0].mobile);
            $.each(data.data, function(entryIndex, entry)
            {  
                // console.log(entryIndex); 
                // console.log(entry['name']);
                var html="";
                html += "<div class=\"col-md-4 wow fadeInUp\">";
                html += "<div class=\"about-col\">";
                html +="<div class=\"img\">";
                html +="<img src=\""+baseUrl+"/public/file_manager/"+entry.file_name+"\" class=\"img-fluid\">";
                html +="<div class=\"icon\"><i class=\""+entry.icon+"\"></i></div>";
                html +="</div>";
                html +="<h2 class=\"title\"><a href="+entry.title+">"+entry.title+"</a></h2>";
                html +="<p>"+entry.description+"</p>";
                html +="</div>";
                html+="</div>"; 
                $('#about1').append(html);
            });     
        },
        error: function (xhr, textStatus, thrownError) 
        {
            var msg = 'Sorry but there was an error: ';
            $('#error').html(msg + xhr.status + ' ' + xhr.statusText); 
            console.log(xhr.status);
            console.log(textStatus);
            console.log(thrownError);
            // $('#box p').empty();    

        }
    }); 
}

//Team list
function teamList(){
    $.ajax({

        type: 'GET',
        url: baseUrl+'/ui/ajax_get_team',  
        dataType: 'json',
        async: false,
        cache: false,
        success: function (data, textStatus, xhr) {
            // $('#team1').empty();
             // var html = '';
            $.each(data.data, function(entryIndex, entry){  
                var html = '';
                // console.log(entryIndex); 
                // console.log(entry['name']);
                html += "<div class=\"col-lg-3 col-md-6 wow \" data-wow-delay=\"0.2s\" >";
                    html += "<div class=\"member\">" ;
                        html +="<img src=\""+baseUrl+"/public/file_manager/"+entry.file_name+"\" class=\"img-fluid\">";
                            html += " <div class=\"member-info\">"
                                html +="<div class=\"member-info-content\">"
                                    html += "<h4>" + entry.name + "</h4>";
                                        html += "<span>" +entry.designation + "</span>"; 
                                            html += "<div class=\"social\">";
                                                html+= "<a href="+entry.link_fb+">"+entry.link_fb+"</a>";
                                            html+="</div>";
                                html+="</div>" ;
                            html+="</div>";   
                    html+="</div>"; 
                html+="</div>";  
                $('#team1').append(html);   
            });
         },

         error: function (xhr, textStatus, thrownError) {
            var msg = 'Sorry but there was an error: ';
            $('#error').html(msg + xhr.status + ' ' + xhr.statusText); 
            console.log(xhr.status);
            console.log(textStatus);
            console.log(thrownError);
            // $('#box p').empty();    

        }

    }); 

}
function servicesList(){

    $.ajax({

        type: 'GET',
        // url: 'http://localhost/cms-app/ui/ajax_get_services',  
        url: baseUrl+'/ui/ajax_get_services',  

        dataType: 'json',
        async: false,
        cache: false,
        success: function (data, textStatus, xhr) {
                 // console.log(data.data[0].title);
                 // console.log(data);
                 // console.log(data.data[0].mobile);
                 
                 $.each(data.data, function(entryIndex, entry){   
                 // console.log(entry);
                 var html = "";
                 html +="<div class=\"col-lg-4 col-md-6 box wow bounceInUp\" data-wow-duration=\"1.4s\">";
                    // html +="<div class=\"icon\">"+entry.icon+"</div>";
                     html +="<div class=\"icon\"><i class=\""+entry.icon+"\"></i></div>";
                    html +="<h4 class=\"title\">"+entry.title+"</h4>";
                    html +="<p class=\"description\">"+entry.description+"</p>";
                 html +="</div>";

                 $('#service1').append(html); 

                 });

                 
             },
              

             error: function (xhr, textStatus, thrownError) {

                var msg = 'Sorry but there was an error: ';

                $('#error').html(msg + xhr.status + ' ' + xhr.statusText); 

                console.log(xhr.status);

                console.log(textStatus);
                console.log(thrownError);

                $('#box p').empty();    

            }

        }); 

}

// Get is Shown from Service
function services_is_shownList(){

    $.ajax({
        type: 'GET',
        // url: 'http://localhost/cms-app/ui/ajax_get_services',  
        url: baseUrl+'/ui/ajax_get_service_is_shown',  
        dataType: 'json',
        async: false,
        cache: false,
        success: function (data, textStatus, xhr) {
            // console.log(data.data[0].title);
            // console.log(data);
            // console.log(data.data[0].mobile);
            $.each(data.data, function(entryIndex, entry){   
                // console.log(entry);
                var html = "";
                html +="<div class=\"col-lg-4 box box-bg\">";
                    html += "<i class=\""+entry.icon+"\">"+"</i>";
                   html+= "<h4><a href="+entry.title+">"+entry.title+"</h4></a>";

                   html +="<p class=\"description\">"+entry.description+"</p>";
                html +="</div>";
                $('#service_isshown').append(html); 

            });      
        },
        error: function (xhr, textStatus, thrownError) {

             var msg = 'Sorry but there was an error: ';
             $('#error').html(msg + xhr.status + ' ' + xhr.statusText); 
             console.log(xhr.status);
             console.log(textStatus);
             console.log(thrownError);
             // $('#box p').empty();    

        }

    }); 

}
//Clientlist
function clientsList(){

    $.ajax({

        type: 'GET',
        url: baseUrl+'/ui/ajax_get_clients',  

        dataType: 'json',
        async: false,
        cache: false,

        success: function (data, textStatus, xhr) {
                 // console.log(data.data[0].title);
                 // console.log(data);
                 // console.log(data.data[0].mobile);
                 // var html = "";
                $.each(data.data, function(entryIndex, entry){   
                    // console.log(entry);
                    // <img src="<?php  echo base_url();   ?>public/BizPage/img/clients/client-1.png" alt="">
                    // html += "<div class=\"owl-carousel clients-carousel\">";
                    var html = "";
                    html +="<img src=\"http://localhost/cms-app/public/file_manager/"+entry.file_name+"\" alt=\"\" width=\"300px\" height=\"70px\">";
                    // html +="</div>";
                    $('.client1').append(html);
                });

                $(".clients-carousel").owlCarousel({
                    autoplay: true,
                    dots: true,
                    loop: true,
                    responsive: { 0: { items: 2 }, 768: { items: 4 }, 900: { items: 6 }
                 }
               });

            },       

             error: function (xhr, textStatus, thrownError) {

                var msg = 'Sorry but there was an error: ';

                $('#error').html(msg + xhr.status + ' ' + xhr.statusText); 

                console.log(xhr.status);

                console.log(textStatus);
                console.log(thrownError);

                $('#box p').empty();    

            }

        }); 

}

//Skillslist
function skillsList(){

    $.ajax({

        type: 'GET',
        // url: 'http://localhost/cms-app/ui/ajax_get_skills',  
        url: baseUrl+'/ui/ajax_get_skills',  

        dataType: 'json',
        async: false,
        cache: false,

        success: function (data, textStatus, xhr) {
                 // console.log(data.data[0].title);
                 // console.log(data);
                 // console.log(data.data[0].mobile);
                
                 
                 $.each(data.data, function(entryIndex, entry){   
                //  // console.log(entry);
                 var html = "";
                
                // var active = entrentry.progress;
                // console.log(active);
                 html +="<div class=\"progress\">";
                    html += "<div class=\"progress-bar bg-"+entry.bg_color+"\" role=\"progressbar\" aria-valuenow=\""+entry.progress+"\" aria-valuemin=\"0\" aria-valuemax=\"100\">"; 
                    html +="<span class=\"skill\">"+entry.description+"<i class=\"val\">"+entry.progress+"</i></span>";
                    html +="</div>";
                 html +="</div>";
               
                $('#skill1').append(html); 
                        
               
                 });
               
                 $('.skills-content').waypoint(function() {
                    $('.progress .progress-bar').each(function() {
                      $(this).css("width", $(this).attr("aria-valuenow"));
                  });
                }, { offset: '80%'} );

           
             },
              

             error: function (xhr, textStatus, thrownError) {

                var msg = 'Sorry but there was an error: ';

                $('#error').html(msg + xhr.status + ' ' + xhr.statusText); 

                console.log(xhr.status);

                console.log(textStatus);
                console.log(thrownError);

                $('#box p').empty();    

            }

        }); 

}

//Settings 
function settingsList(){
    $.ajax({

        type: 'GET',
        url: baseUrl+'/ui/ajax_get_settings',  
        dataType: 'json',
        async: false,
        cache: false,
        success: function (data, textStatus, xhr) {

            // console.log(data.data[0].name);
            // console.log(data);
            // console.log(data.data[0].mobile);   
            $('#headings').text(data.data[0].company_name);
            $('#company_description').text(data.data[0].company_desc);   
            $('#about_des').text(data.data[0].about_intro); 
            $('#service-desc').text(data.data[0].service_intro);
            $('#tem_intro').text(data.data[0].team_intro);
            $('#contact_intro').text(data.data[0].contact_intro);  
            $('#address').text(data.data[0].company_addr); 
            $('#company_ad').text(data.data[0].company_addr);
            $('#company_m').text("Phone : "+data.data[0].company_mobile);
            $('#company_e').text("Email :"+data.data[0].company_email);
            $("#company_mobile").prop("href",data.data[0].company_mobile).text(data.data[0].company_mobile);
            $("#company_email").prop("href",+"mailto:"+data.data[0].company_email).text(data.data[0].company_email);
        },
        error: function (xhr, textStatus, thrownError) {

            var msg = 'Sorry but there was an error: ';
            $('#error').html(msg + xhr.status + ' ' + xhr.statusText); 
            console.log(xhr.status);
            console.log(textStatus);
            console.log(thrownError);
            // $('#box p').empty();    

        }

    }); 

}

// Portfolio
function portfolioList(){
    $.ajax({

        type: 'GET',
        url: baseUrl+'/ui/ajax_get_portfolio',  
        dataType: 'json',
        async: false,
        cache: false,
        success: function (data, textStatus, xhr) {
            // console.log(data);
            $.each(data.data, function(entryIndex, entry){  
                // console.log(entryIndex); 
                // console.log(entry['name']);
                var html = '';
                html += "<div class=\"col-lg-4 col-md-6 portfolio-item filter-"+entry.category+" wow fadeInUp\">";
                    html +="<div class=\"portfolio-wrap\">";
                        html +="<figure>"
                        +"<img src=\""+baseUrl+"/public/file_manager/"+entry.file_name+"\" class=\"img-fluid\">"
                        +"<a href=\"http://localhost/cms-app/public/file_manager/"+entry.file_name+"\" data-lightbox=\"portfolio\" data-title=\"App 1\" class=\"link-preview\" title=\"Preview\"><i class=\"ion ion-eye\" ></i></a>"
                        +"<a href=\"#\" class=\"link-details\" title=\"More Details\"><i class=\"ion ion-android-open\"></i></a>"
                        +"</figure>";
                        html +="<div class=\"portfolio-info\">";
                            html+= "<h4><a href="+entry.title+">"+entry.title+"</h4></a>";
                            html+="<p>"+entry.description+"</p>";
                        html+="</div>"; 
                    html+="</div>"; 
             html+="</div>";
             $('#portfolio1').append(html);  
            });
            var portfolioIsotope = $('.portfolio-container').isotope({
                itemSelector: '.portfolio-item',
                layoutMode: 'fitRows'
            });
            $('#portfolio-flters li').on( 'click', function() {
                $("#portfolio-flters li").removeClass('filter-active');
                $(this).addClass('filter-active');
                portfolioIsotope.isotope({ filter: $(this).data('filter') });
            });
        },
        error: function (xhr, textStatus, thrownError) {
            var msg = 'Sorry but there was an error: ';
            $('#error').html(msg + xhr.status + ' ' + xhr.statusText); 
            console.log(xhr.status);
            console.log(textStatus);
            console.log(thrownError);
            // $('#box p').empty();
        }
    }); 
}

