$(document).ready(function() {
    
    // Datatable Load - Category - index()
        
        categoryList();
        pidList();

    // Edit - Show Category data by ID
        var id = window.location.href.substring(window.location.href.lastIndexOf('/') + 1);
        // console.log('Here am I Findng...!'+id);
        if(typeof(id) != "undefined" && id !== null && (/^[0-9]+(?:\.[0-9]+)?$/.test(id))) 
        {
            // console.log('SUCCESS..!'+id);
            get_category_by_id();
        }

        // $(document).on('click', '.btn-edit', function(e) {

        //     e.preventDefault();

        //     var id = $(this).data("id");
        //     get_customer_by_id(id);
        // });
    
    // SAVE
        $(document).on('click', '#form-category', function(e) {

            var form = $("#form-category");

            // $.validator.setDefaults({ ignore: ":hidden:not(.chosen-select)" });
            $(form).validate({
                rules: {
                    'name': {
                        required: true,
                    },
                     'code': {
                        required: true,
                    },
                    //  'pid': {
                    //     required: true,
                    // },
                     'sort_order': {
                        required: true,
                    },
                     'prefix': {
                        required: true,
                    },
                    'type_id': {
                        required: true,
                    }
                },
                submitHandler: function(form) {

                    e.preventDefault();
                    var url = baseUrl+"/category/store";
                    
                    $.ajax({
                        url: url,
                        type: "POST",
                        dataType: "JSON",
                        data: new FormData(form),
                        processData: false,
                        contentType: false,
                        cache: false,
                        async: false,
                        success: function (res)
                        {
                            if (res.status === 200 || res.status === 201) {
                                toast_notify('success',res.msg);
                            } 
                            else if (res.status === 503 || res.status === 400) {
                                toast_notify('error',res.msg);
                            } 

                        },
                        error: function (xhr, textStatus, thrownError) {
                            var msg = 'Sorry but there was an error: ';
                            $('#error').html(msg + xhr.status + ' ' + xhr.statusText); 
                            console.log(xhr.status);
                            console.log(textStatus);
                            console.log(thrownError);
                        }
                    });
                }
            });   
        });
    
    // Delete
        $(document).on('click', '.btn-del', function(e){

            e.preventDefault();
            var id = $(this).data("id");
            var url = baseUrl+"/category/delete";
                        
            $.ajax({
                url: url,
                type: "POST",
                dataType: "JSON",
                data: {id:id},
                success: function (res)
                {
                    // console.log(res);
                    toast_notify('error',res.msg);
                    categoryList();
                },
                error: function (xhr, textStatus, thrownError) {
                    var msg = 'Sorry but there was an error: ';
                    $('#error').html(msg + xhr.status + ' ' + xhr.statusText); 
                    console.log(xhr.status);
                    console.log(textStatus);
                    console.log(thrownError);
                }
            });
        });
});



// Fn - get category by id
    function get_category_by_id() {

        var id = window.location.href.substring(window.location.href.lastIndexOf('/') + 1);
        var url = baseUrl+"/category/ajax_get_category/"+id;

        $.ajax({
            type: 'GET',
            url: url,  
            dataType: 'json',
            success: function (data, textStatus, xhr) {
                $('#id').val(data.data.id);
                $('#name').val(data.data.name);
                $('#code').val(data.data.code);
                $('#pid').val(data.data.pid);
                $('#sort_order').val(data.data.sort_order);
                $('#prefix').val(data.data.prefix);
                $('#type_id').val(data.data.type_id);

                //status cheak
                $('#is_active').val(data.data.is_active);
                if (data.data.is_active == 0) {
                    $("input[name='is_active']").prop('checked', false);
                }
                else {
                    $("input[name='is_active']").prop('checked', true);
                }

                // iCheck data update
                $('.i-checks').iCheck('update');
                
                $('.form-title').text('Category Edit');
                $('.btn-form-submit').text('UPDATE');
            },
            error: function (xhr, textStatus, thrownError) {
                var msg = 'Sorry but there was an error: ';
                $('#error').html(msg + xhr.status + ' ' + xhr.statusText); 
                console.log(xhr.status);
                console.log(textStatus);
                console.log(thrownError);  
            }
        });
    }

// categoryList
    function categoryList() {
        $('.dataTables-category').DataTable({
            "ajax": baseUrl+"/category/ajax_get_category",
            "columns": [
                { "data": "id" },
                { "data": "name" },
                { "data": "code" },
                { 
                    data: "parent_name",
                    render: function (data, type, full, meta) 
                    {
                        var p_name = (full.pid == 0) ? 'Main Category' : data;
                        return p_name;
                    }
                },
                { "data": "sort_order" },
                { "data": "prefix" },
                { "data": "type_id" },
                {
                    data: "is_active",
                    render: function (data, type, full, meta) 
                    {
                        var is_active = (data == 1) ? '<span class="label label-primary">ACTIVE</span>' : '<span class="label label-danger">DEACTIVE</span>';
                        return is_active;
                    }
                },
                {
                    data: "id",
                    render: function (data, type, full, meta) {
                        return "<a href=\""+baseUrl+"/category/edit/"+data+"\" class=\"btn-edit btn btn-primary btn-xs btn-outline\" data-id="+data+"><i class=\"fa fa-edit\"></i></a>"
                        +"&nbsp;"+
                        "<a href=\""+baseUrl+"/category/del/"+data+"\" class=\"btn-del btn btn-danger btn-xs btn-outline\" data-id="+data+" onclick=\"return confirm('You are about to delete a record. This cannot be undone. Are you sure?');\"><i class=\"fa fa-remove\"></i></a>";
                    }
                }
            ],
            pageLength: 10,
            responsive: true,
            dom: '<"html5buttons"B>lTfgitp',
            buttons: [
                { extend: 'copy'},
                {extend: 'csv'},
                {extend: 'excel', title: 'ExampleFile'},
                {extend: 'pdf', title: 'ExampleFile'},

                {extend: 'print',
                 customize: function (win){
                        $(win.document.body).addClass('white-bg');
                        $(win.document.body).css('font-size', '10px');

                        $(win.document.body).find('table')
                                .addClass('compact')
                                .css('font-size', 'inherit');
                }
                }
            ],
            destroy: true   // https://datatables.net/manual/tech-notes/3 [bugfix - call in fn and realaod]
        });
    }

//joining    
    function pidList() {
        $.ajax({
            type: 'GET',
            url: baseUrl+'/category/ajax_get_category',  
            dataType: 'json',
            async: false,
            cache: false,
                success: function (data, textStatus, xhr) {
                 // console.log(data.data[0].name);
                 // console.log(data);
                 // console.log(data.data[0].mobile);
                 $.each(data.data, function(entryIndex, entry){                 
                     var html = '';
                         // console.log(entryIndex); 
                         // console.log(entry['name']);  

                         html += "<option value=\""+entry.id+"\">" + entry.name + "</option>";  
                          $('#pid').append(html);
                 }); 

            },

            error: function (xhr, textStatus, thrownError) 
            {
                    var msg = 'Sorry but there was an error: ';
                    $('#error').html(msg + xhr.status + ' ' + xhr.statusText); 
                    console.log(xhr.status);
                    console.log(textStatus);
                    console.log(thrownError);
                    $('#box p').empty();    

            }
        }); 
    }