$(document).ready(function() {
	
	console.log('Checked! [admin/role.js]');

    // Datatable Load - Role - index()
        
        roleList();
    
    // Edit - Show Role data by ID
        $(document).on('click', '.btn-edit', function(e) {

            e.preventDefault();

            var id = $(this).data("id");
            get_role_by_id(id);
        });
    
    // SAVE
        $(document).on('click', '#form-role', function(e) {

            var form = $("#form-role");

            $(form).validate({
                rules: {
                    'name': {
                        required: true,
                    }
                    // 'permission[]': {
                    //     required: true
                    // }
                },
                submitHandler: function(form) {

                    e.preventDefault();
                    var url = baseUrl+"/role/store";
                    
                    $.ajax({
                        url: url,
                        type: "POST",
                        dataType: "JSON",
                        data: new FormData(form),
                        processData: false,
                        contentType: false,
                        cache: false,
                        async: false,
                        success: function (res)
                        {
                            if (res.status === 200 || res.status === 201) {
                                roleList();
                                toast_notify('success',res.msg);
                            } 
                            else if (res.status === 503 || res.status === 400) {
                                toast_notify('error',res.msg);
                            } 

                        },
                        error: function (xhr, textStatus, thrownError) {
                            var msg = 'Sorry but there was an error: ';
                            $('#error').html(msg + xhr.status + ' ' + xhr.statusText); 
                            console.log(xhr.status);
                            console.log(textStatus);
                            console.log(thrownError);
                        }
                    });
                }
            });   
        });
    
    // Delete
        $(document).on('click', '.btn-del', function(e){

            e.preventDefault();
            var id = $(this).data("id");
            var url = baseUrl+"/role/delete";
                        
            $.ajax({
                url: url,
                type: "POST",
                dataType: "JSON",
                data: {id:id},
                success: function (res)
                {
                    // console.log(res);
                    toast_notify('error',res.msg);
                    roleList();
                },
                error: function (xhr, textStatus, thrownError) {
                    var msg = 'Sorry but there was an error: ';
                    $('#error').html(msg + xhr.status + ' ' + xhr.statusText); 
                    console.log(xhr.status);
                    console.log(textStatus);
                    console.log(thrownError);
                }
            });
        });
});


// Fn - get userdata by id
function get_role_by_id(id) {

    var url = baseUrl+"/role/ajax_get_roles/"+id;

    $.ajax({
        type: 'GET',
        url: url,  
        dataType: 'json',
        success: function (data, textStatus, xhr) {
            
            $('#id').val(data.data.id);
            $('#name').val(data.data.name);

            var permission = data.data.permission;
            var permissionArr = permission.split(",");
            // Selected TRUE
            $('#permission').chosen().val(permissionArr).trigger('chosen:updated');

            $('#is_active').val(data.data.is_active);
            if (data.data.is_active == 0) {
                $("input[name='is_active']").prop('checked', false);
            }
            else {
                $("input[name='is_active']").prop('checked', true);
            }

            // iCheck data update
            $('.i-checks').iCheck('update');
            

            $('.form-title').text('User Edit');
            $('.btn-form-submit').text('UPDATE');
        },
        error: function (xhr, textStatus, thrownError) {
            var msg = 'Sorry but there was an error: ';
            $('#error').html(msg + xhr.status + ' ' + xhr.statusText); 
            console.log(xhr.status);
            console.log(textStatus);
            console.log(thrownError);  
        }
    });
}

// roleList
function roleList() {
    $.ajax({
        url: baseUrl+"/role/ajax_get_roles",
        type: 'GET',
        dataType: 'json',
        async: false,
        cache: false,
        success: function (data, textStatus, xhr) {
            $('.tbl-role tbody').empty();
            $.each(data.data, function(entryIndex, entry){       
                var html = "";
                html += "<tr>";
                    html += "<td>"+entry['id']+"</td>";
                    html += "<td>"+entry['name']+"</td>";

                    // Permission split 
                    var permissionStr = entry['permission'];
                    var permissionArr = permissionStr.split(",");
                    var dom = "";

                    $.each(permissionArr, function(key, val){
                        dom += "<span class=\"label label-primary\">"+val+"</span>&nbsp;";
                    });

                    html += "<td>"+dom+"</td>";

                    // Show Status
                    var is_active = (entry['is_active'] == 1) ? '<span class="label label-primary">ACTIVE</span>' : '<span class="label label-danger">DEACTIVE</span>';
                    
                    html += "<td>"+is_active+"</td>";
                    
                    html += "<td>";
                        html += "<a href=\""+baseUrl+"/role/"+entry['id']+"\" class=\"btn-edit btn btn-primary btn-xs btn-outline\" data-id="+entry['id']+"><i class=\"fa fa-edit\"></i></a>";
                        html += "&nbsp;&nbsp;";
                        html += "<a href=\"/role/"+entry['id']+"\" class=\"btn-del btn btn-danger btn-xs btn-outline\" data-id="+entry['id']+" onclick=\"return confirm('You are about to delete a record. This cannot be undone. Are you sure?');\"><i class=\"fa fa-remove\"></i></a>";
                    html += "</td>";
                html += "</tr>";

                $('.tbl-role tbody').append(html);  
            }); 
        },
        error: function (xhr, textStatus, thrownError) {
            var msg = 'Sorry but there was an error: ';
            $('#error').html(msg + xhr.status + ' ' + xhr.statusText); 
            console.log(xhr.status);
            console.log(textStatus);
            console.log(thrownError);
            $('#box p').empty();    
        }
    }); 
}