$(document).ready(function() {

    var id = $(this).data("id");
    var url = baseUrl+"/settings/ajax_get_settings/";

    $.ajax({
        type: 'GET',
        url: url,  
        dataType: 'json',
        success: function (data, textStatus, xhr) {
            
            console.log(data);

            $('#id').val(data.data.id);
            $('#company_name').val(data.data.company_name);
            $('#company_desc').val(data.data.company_desc);
            // $('#company_logo').val(data.data.company_logo);
            $('#company_addr').val(data.data.company_addr);
            $('#company_mobile').val(data.data.company_mobile);
            $('#company_email').val(data.data.company_email);
            $('#about_intro').val(data.data.about_intro);
            $('#service_intro').val(data.data.service_intro);
            $('#skill_intro').val(data.data.skill_intro);
            $('#team_intro').val(data.data.team_intro);
            $('#contact_intro').val(data.data.contact_intro);
            $('#fb').val(data.data.fb);
            $('#twt').val(data.data.twt);
            $('#inst').val(data.data.inst);
            $('#gplus').val(data.data.gplus);
            $('#link_in').val(data.data.link_in);
            // Selected TRUE
            // $('#is_active').val(data.data.is_active);
            // if (data.data.is_active == 0) {
            //     $("input[name='is_active']").prop('checked', false);
            // }
            // else {
            //     $("input[name='is_active']").prop('checked', true);
            // }

            // // iCheck data update
            // $('.i-checks').iCheck('update');
            var src = baseUrl+"/public/file_manager/"+data.data.file_name; 
            $('#preview').html("<img src=\""+src+"\">");  
            // $('.form-title').text('About Us Edit');
            $('.btn-form-submit').text('UPDATE');
        },
        error: function (xhr, textStatus, thrownError) {
            var msg = 'Sorry but there was an error: ';
            $('#error').html(msg + xhr.status + ' ' + xhr.statusText); 
            console.log(xhr.status);
            console.log(textStatus);
            console.log(thrownError);  
        }
    });
        
         

    // SAVE
        $(document).on('click', '#form-settings', function() {

            // e.preventDefault();
            // console.log('prevented!');
            var form = $("#form-settings");

            $(form).validate({
                rules: {
                    'company_name': {
                        required: true,
                    }
                },
                submitHandler: function(form) {

                    var url = baseUrl+"/settings/store";
                    
                    $.ajax({
                        url: url,
                        type: "POST",
                        dataType: "JSON",
                        data: new FormData(form),
                        processData: false,
                        contentType: false,
                        cache: false,
                        async: false,
                        success: function (res)
                        {
                            // console.log(res);
                            if (res.status === 200 || res.status === 201) {
                                toast_notify('success',res.msg);
                            } 
                            else if (res.status === 503 || res.status === 400) {
                                toast_notify('error',res.msg);
                            }
                            // $('#form-about').trigger("reset");
                            // $(".dataTables-about").empty();
                             // aboutList(); 
                        },
                        error: function (xhr, textStatus, thrownError) {
                            var msg = 'Sorry but there was an error: ';
                            $('#error').html(msg + xhr.status + ' ' + xhr.statusText); 
                            console.log(xhr.status);
                            console.log(textStatus);
                            console.log(thrownError);
                        }
                    });
                }
            });   
        });
    
    
});








