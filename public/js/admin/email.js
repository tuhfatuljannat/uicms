$(document).ready(function() {
    
    console.log('Checked! [admin/parts.js]');

 
    // SAVE
    
        $(document).on('click', '#send-mail', function(e) {
            var form = $("#send-mail");

            // $.validator.setDefaults({ ignore: ":hidden:not(.chosen-select)" });
            $(form).validate({
                // rules: {
                //     'name': {
                //         required: true,
                //     },
                //     'mobile': {
                //         required: true,
                //         maxlength: 11,
                //         number: true
                //     },
                //     'product_service[]': {
                //         required: true,
                //     }
                // },
                submitHandler: function(form) {

                    e.preventDefault();
                    var url = baseUrl+"/sendemail/send";
                    
                    $.ajax({
                        url: url,
                        type: "POST",
                        dataType: "JSON",
                        data: new FormData(form),
                        processData: false,
                        contentType: false,
                        cache: false,
                        async: false,
                        success: function (res)
                        {
                            if (res.status === 200 || res.status === 201) {
                                toast_notify('success',res.msg);
                            } 
                            else if (res.status === 503 || res.status === 400) {
                                toast_notify('error',res.msg);
                            } 

                        },
                        error: function (xhr, textStatus, thrownError) {
                            var msg = 'Sorry but there was an error: '+ xhr.status + ' ' + xhr.statusText;
                            toast_notify('error',msg);
                        }
                    });
                }
            });   
        }); 
});