$(document).ready(function() {
    
    console.log('Checked! [admin/parts.js]');

    // Datatable Load - Customer - index()
        
        // customerList();

    // Content SHoW/HIDE - FROM
    
        $(document).on('ifToggled', '#chk_buildin_parts', function(e) {

            e.preventDefault();

            // iCheck data update
            $('.i-checks').iCheck('update');

            if ($(this).is(":checked")) {
                $(".box_buildin_parts").show();
            } 
            else {
                $(".box_buildin_parts").hide();
            }
        });


        $(document).on('ifToggled', '#chk_custom_fields', function(e) {

            e.preventDefault();

            // iCheck data update
            $('.i-checks').iCheck('update');

            if ($(this).is(":checked")) {
                $(".box_custom_fields").show();
            } 
            else {
                $(".box_custom_fields").hide();
            }
        });


    // RESET - FROM
    /*
        $(document).on('click', '.btn-form-reset', function(e) {

            e.preventDefault();
            
            // Reset Form
            reset_form('#form-customer','Customer');
        });
    */
    // Edit - Show Customer data by ID
    /*
        var id = window.location.href.substring(window.location.href.lastIndexOf('/') + 1);

        if(typeof(id) != "undefined" && id !== null && (/^[0-9]+(?:\.[0-9]+)?$/.test(id))) 
        {
            get_customer_by_id(id);
        }
    */
    // SAVE
    /*
        $(document).on('click', '#form-customer', function(e) {

            var form = $("#form-customer");

            // $.validator.setDefaults({ ignore: ":hidden:not(.chosen-select)" });
            $(form).validate({
                rules: {
                    'name': {
                        required: true,
                    },
                    'mobile': {
                        required: true,
                        maxlength: 11,
                        number: true
                    },
                    'product_service[]': {
                        required: true,
                    }
                },
                submitHandler: function(form) {

                    e.preventDefault();
                    var url = baseUrl+"/customers/store";
                    
                    $.ajax({
                        url: url,
                        type: "POST",
                        dataType: "JSON",
                        data: new FormData(form),
                        processData: false,
                        contentType: false,
                        cache: false,
                        async: false,
                        success: function (res)
                        {
                            if (res.status === 200 || res.status === 201) {
                                toast_notify('success',res.msg);
                            } 
                            else if (res.status === 503 || res.status === 400) {
                                toast_notify('error',res.msg);
                            } 

                        },
                        error: function (xhr, textStatus, thrownError) {
                            var msg = 'Sorry but there was an error: '+ xhr.status + ' ' + xhr.statusText;
                            toast_notify('error',msg);
                        }
                    });
                }
            });   
        });
    */
    // Delete
    /*
        $(document).on('click', '.btn-del', function(e){

            e.preventDefault();
            var id = $(this).data("id");
            var url = baseUrl+"/customers/delete";
                        
            $.ajax({
                url: url,
                type: "POST",
                dataType: "JSON",
                data: {id:id},
                beforeSend:function(){
                    return confirm("You are about to delete a record. This cannot be undone. Are you sure?");
                },
                success: function (res)
                {
                    // console.log(res);
                    toast_notify('error',res.msg);
                    customerList();
                },
                error: function (xhr, textStatus, thrownError) {
                    var msg = 'Sorry but there was an error: '+ xhr.status + ' ' + xhr.statusText;
                    toast_notify('error',msg);
                }
            });
        });
    */
});



// Fn - get customerdata by id
function get_customer_by_id(id) {

    var url = baseUrl+"/customers/ajax_get_customers/"+id;

    $.ajax({
        type: 'GET',
        url: url,  
        dataType: 'json',
        success: function (data, textStatus, xhr) {
            $('#id').val(data.data.id);
            $('#code').val(data.data.code);
            $('#name').val(data.data.name);
            $('#mobile').val(data.data.mobile);
            $('#email').val(data.data.email);
            $('#address').val(data.data.address);

            var product_service = data.data.product_service;
            var productArr = product_service.split(",");
            $('#product_service').chosen().val(productArr).trigger('chosen:updated');

            $('#is_active').val(data.data.is_active);
            if (data.data.is_active == 0) {
                $("input[name='is_active']").prop('checked', false);
            }
            else {
                $("input[name='is_active']").prop('checked', true);
            }

            // iCheck data update
            $('.i-checks').iCheck('update');
            
            $('.form-title').text('Customer Edit');
            $('.btn-form-submit').text('UPDATE');
        },
        error: function (xhr, textStatus, thrownError) {
            var msg = 'Sorry but there was an error: '+ xhr.status + ' ' + xhr.statusText;
            toast_notify('error',msg);   
        }
    });
}

// customerList
function customerList() {
    $('.dataTable-customer').DataTable({
        "ajax": baseUrl+"/customers/ajax_get_customers",
        "columns": [
            { "data": "id" },
            { "data": "code" },
            { "data": "name" },
            { "data": "mobile" },
            { "data": "email" },
            { "data": "address" },
            {
                data: "product_service",
                render: function (data, type, full, meta) 
                {
                    var productArr = data.split(",");
                    var dom = "";
                    $.each(productArr, function(key, val){
                        dom += "<span class=\"label label-primary\">"+val+"</span>&nbsp;";
                    });
                return dom;
                }
            },
            {
                data: "is_active",
                render: function (data, type, full, meta) 
                {
                    var is_active = (data == 1) ? '<span class="label label-primary">ACTIVE</span>' : '<span class="label label-danger">DEACTIVE</span>';
                    return is_active;
                }
            },
            {
                data: "id",
                render: function (data, type, full, meta) {
                    return "<a href=\""+baseUrl+"/customers/edit/"+data+"\" class=\"btn-edit btn btn-primary btn-xs btn-outline\" data-id="+data+"><i class=\"fa fa-edit\"></i></a>"
                    +"&nbsp;"+
                    "<a href=\""+baseUrl+"/customers/del/"+data+"\" class=\"btn-del btn btn-danger btn-xs btn-outline\" data-id="+data+"><i class=\"fa fa-remove\"></i></a>";
                }
            }
        ],
        pageLength: 10,
        responsive: true,
        dom: '<"html5buttons"B>lTfgitp',
        buttons: [
            { extend: 'copy'},
            {extend: 'csv'},
            {extend: 'excel', title: 'ExampleFile'},
            {extend: 'pdf', title: 'ExampleFile'},

            {extend: 'print',
             customize: function (win){
                    $(win.document.body).addClass('white-bg');
                    $(win.document.body).css('font-size', '10px');

                    $(win.document.body).find('table')
                            .addClass('compact')
                            .css('font-size', 'inherit');
            }
            }
        ],
        destroy: true   // https://datatables.net/manual/tech-notes/3 [bugfix - call in fn and realaod]
    });
}