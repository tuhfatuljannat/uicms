$(document).ready(function() {
    
    console.log('Checked! [admin/team.js]');

    // Datatable Load - Users - index()
        teamList();

    // Edit - Show user data by ID
        var id = window.location.href.substring(window.location.href.lastIndexOf('/') + 1);
        // console.log('Here am I Findng...!'+id);
        if(typeof(id) != "undefined" && id !== null && (/^[0-9]+(?:\.[0-9]+)?$/.test(id))) {
            // console.log('SUCCESS..!'+id);
            get_team_by_id();
        }

    // SAVE
        $(document).on('click', '#form-team', function() {
            // e.preventDefault();
            // console.log('prevented!');
            var form = $("#form-team");
            $(form).validate({
                rules: {
                    'name': {
                        required: true,
                    },
                     'designation': {
                        required: true,
                    }

                },
                submitHandler: function(form) {
                    var url = baseUrl+"/team/store";
                    $.ajax({
                        url: url,
                        type: "POST",
                        dataType: "JSON",
                        data: new FormData(form),
                        processData: false,
                        contentType: false,
                        cache: false,
                        async: false,
                        success: function (res)
                        {
                            // console.log(res);
                            if (res.status === 200 || res.status === 201) {
                                toast_notify('success',res.msg);
                            } 
                            else if (res.status === 503 || res.status === 400) {
                                toast_notify('error',res.msg);
                            }
                            // $('#form-about').trigger("reset");
                            $(".dataTables-team").empty();
                             teamList(); 
                        },
                        error: function (xhr, textStatus, thrownError) {
                            var msg = 'Sorry but there was an error: ';
                            $('#error').html(msg + xhr.status + ' ' + xhr.statusText); 
                            console.log(xhr.status);
                            console.log(textStatus);
                            console.log(thrownError);
                        }
                    });
                }
            });   
        });

    // Delete
        $(document).on('click', '.btn-del', function(e){
            e.preventDefault();
            var id = $(this).data("id");
            var url = baseUrl+"/team/delete";            
            $.ajax({
                url: url,
                type: "POST",
                dataType: "JSON",
                data: {id:id},
                success: function (res)
                {
                    console.log(res);
                    toast_notify('error',res.msg);
                     $(".dataTables-team").empty();
                     teamList();
                },
                error: function (xhr, textStatus, thrownError) {
                    var msg = 'Sorry but there was an error: ';
                    $('#error').html(msg + xhr.status + ' ' + xhr.statusText); 
                    console.log(xhr.status);
                    console.log(textStatus);
                    console.log(thrownError);
                }
            });
        });
});

// Fn - get userdata by id
function get_team_by_id() {
    var id = window.location.href.substring(window.location.href.lastIndexOf('/') + 1);
    var url = baseUrl+"/team/ajax_get_team/"+id;
    $.ajax({
        type: 'GET',
        url: url,  
        dataType: 'json',
        success: function (data, textStatus, xhr) {
            // console.log(data);
            $('#id').val(data.data.id);
            $('#name').val(data.data.name);
            $('#designation').val(data.data.designation);
            $('#link_fb').val(data.data.link_fb);
            $('#link_twt').val(data.data.link_twt);
            $('#link_in').val(data.data.link_in);
            $('#link_gp').val(data.data.link_gp);

            // Selected TRUE
            $('#is_active').val(data.data.is_active);
            if (data.data.is_active == 0) {
                $("input[name='is_active']").prop('checked', false);
            }
            else {
                $("input[name='is_active']").prop('checked', true);
            }

            // iCheck data update
            $('.i-checks').iCheck('update');
             var src = baseUrl+"/public/file_manager/"+data.data.file_name; 
            $('#preview').html("<img src=\""+src+"\">");  

            $('.form-title').text('Team Edit');
            $('.btn-form-submit').text('UPDATE');
        },
        error: function (xhr, textStatus, thrownError) {
            var msg = 'Sorry but there was an error: ';
            $('#error').html(msg + xhr.status + ' ' + xhr.statusText); 
            console.log(xhr.status);
            console.log(textStatus);
            console.log(thrownError);  
        }
    });
}

// Team List
function teamList() {
    $('.dataTables-team').DataTable({
        "ajax": baseUrl+"/team/ajax_get_team",
        "columns": [
            { "data": "id" },
            { "data": "name" },
            { "data": "designation" },
            {
                data: "file_name",
                render: function (data, type, full, meta) {
                    return "<img src=\""+baseUrl+"/public/file_manager/"+data+"\" class=\"img-thumbnail\" alt=\""+data+"\" data-img=\""+data+"\" width=\"50\" height=\"50\" >";
                }
            },
            { "data": "link_fb" }, 
            { "data": "link_twt" }, 
            { "data": "link_in" }, 
            { "data": "link_gp" }, 
          
             {
                data: "is_active",
                render: function (data, type, full, meta) 
                {
                    var is_active = (data == 1) ? '<span class="label label-primary">ACTIVE</span>' : '<span class="label label-danger">DEACTIVE</span>';
                    // console.log(is_active);
                    // var dom = "<span class=\"label label-primary\">"+is_active+"</span>&nbsp;";
                    return is_active;
                }
            },
            {
                data: "id",
                render: function (data, type, full, meta) {
                    return "<a href=\""+baseUrl+"/team/edit/"+data+"\" class=\"btn-edit btn btn-primary btn-xs btn-outline\" data-id="+data+"><i class=\"fa fa-edit\"></i></a>"
                    +"&nbsp;"+
                    "<a href=\""+baseUrl+"/team/del/"+data+"\" class=\"btn-del btn btn-danger btn-xs btn-outline\" data-id="+data+" onclick=\"return confirm('You are about to delete a record. This cannot be undone. Are you sure?');\"><i class=\"fa fa-remove\"></i></a>";
                }
            }
        ],
        pageLength: 10,
        responsive: true,
        dom: '<"html5buttons"B>lTfgitp',
        buttons: [
            { extend: 'copy'},
            {extend: 'csv'},
            {extend: 'excel', title: 'ExampleFile'},
            {extend: 'pdf', title: 'ExampleFile'},
            {extend: 'print',
             customize: function (win){
                    $(win.document.body).addClass('white-bg');
                    $(win.document.body).css('font-size', '10px');

                    $(win.document.body).find('table')
                            .addClass('compact')
                            .css('font-size', 'inherit');
                }
            }
        ],
        destroy: true   // https://datatables.net/manual/tech-notes/3 [bugfix - call in fn and realaod]
    });
}
