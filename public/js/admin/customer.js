$(document).ready(function() {
    
    // Datatable Load - Customer - index()
        
        customerList();

    // Edit - Show Role data by ID
        var id = window.location.href.substring(window.location.href.lastIndexOf('/') + 1);
        // console.log('Here am I Findng...!'+id);
        if(typeof(id) != "undefined" && id !== null && (/^[0-9]+(?:\.[0-9]+)?$/.test(id))) 
        {
            // console.log('SUCCESS..!'+id);
            get_customer_by_id();
        }

        // $(document).on('click', '.btn-edit', function(e) {

        //     e.preventDefault();

        //     var id = $(this).data("id");
        //     get_customer_by_id(id);
        // });
    
    // SAVE
        $(document).on('click', '#form-customer', function(e) {

            var form = $("#form-customer");

            // $.validator.setDefaults({ ignore: ":hidden:not(.chosen-select)" });
            $(form).validate({
                rules: {
                    'code': {
                        required: true,
                    },
                     'name': {
                        required: true,
                    },
                     'mobile': {
                        required: true,
                        maxlength: 11,
                        number: true
                    },
                     'email': {
                        required: true,
                    },
                     'address': {
                        required: true,
                    },
                    'product_service[]': {
                        required: true,
                    }
                },
                submitHandler: function(form) {

                    e.preventDefault();
                    var url = baseUrl+"/customers/store";
                    
                    $.ajax({
                        url: url,
                        type: "POST",
                        dataType: "JSON",
                        data: new FormData(form),
                        processData: false,
                        contentType: false,
                        cache: false,
                        async: false,
                        success: function (res)
                        {
                            if (res.status === 200 || res.status === 201) {
                                toast_notify('success',res.msg);
                            } 
                            else if (res.status === 503 || res.status === 400) {
                                toast_notify('error',res.msg);
                            } 

                        },
                        error: function (xhr, textStatus, thrownError) {
                            var msg = 'Sorry but there was an error: ';
                            $('#error').html(msg + xhr.status + ' ' + xhr.statusText); 
                            console.log(xhr.status);
                            console.log(textStatus);
                            console.log(thrownError);
                        }
                    });
                }
            });   
        });
    
    // Delete
        $(document).on('click', '.btn-del', function(e){

            e.preventDefault();
            var id = $(this).data("id");
            var url = baseUrl+"/customers/delete";
                        
            $.ajax({
                url: url,
                type: "POST",
                dataType: "JSON",
                data: {id:id},
                success: function (res)
                {
                    // console.log(res);
                    toast_notify('error',res.msg);
                    customerList();
                },
                error: function (xhr, textStatus, thrownError) {
                    var msg = 'Sorry but there was an error: ';
                    $('#error').html(msg + xhr.status + ' ' + xhr.statusText); 
                    console.log(xhr.status);
                    console.log(textStatus);
                    console.log(thrownError);
                }
            });
        });
});



// Fn - get customerdata by id
    function get_customer_by_id() {

        var id = window.location.href.substring(window.location.href.lastIndexOf('/') + 1);
        var url = baseUrl+"/customers/ajax_get_customers/"+id;

        $.ajax({
            type: 'GET',
            url: url,  
            dataType: 'json',
            success: function (data, textStatus, xhr) {
                $('#id').val(data.data.id);
                $('#code').val(data.data.code);
                $('#name').val(data.data.name);
                $('#mobile').val(data.data.mobile);
                $('#email').val(data.data.email);
                $('#address').val(data.data.address);

                var product_service = data.data.product_service;
                var productArr = product_service.split(",");
                $('#product_service').chosen().val(productArr).trigger('chosen:updated');


                // var product_service = data.data.product_service;
                // console.log(product_service);
                // var productArr = product_service.split(",");
                // console.log(productArr);
                // Selected TRUE
                // $('#product_service').chosen().val(productArr).trigger('chosen:updated');

                $('#is_active').val(data.data.is_active);
                if (data.data.is_active == 0) {
                    $("input[name='is_active']").prop('checked', false);
                }
                else {
                    $("input[name='is_active']").prop('checked', true);
                }

                // iCheck data update
                $('.i-checks').iCheck('update');
                
                $('.form-title').text('Customer Edit');
                $('.btn-form-submit').text('UPDATE');
            },
            error: function (xhr, textStatus, thrownError) {
                var msg = 'Sorry but there was an error: ';
                $('#error').html(msg + xhr.status + ' ' + xhr.statusText); 
                console.log(xhr.status);
                console.log(textStatus);
                console.log(thrownError);  
            }
        });
    }

// customerList
    function customerList() {
        $('.dataTables-customer').DataTable({
            "ajax": baseUrl+"/customers/ajax_get_customers",
            "columns": [
                { "data": "id" },
                { "data": "code" },
                { "data": "name" },
                { "data": "mobile" },
                { "data": "email" },
                { "data": "address" },
                {
                    data: "product_service",
                    render: function (data, type, full, meta) 
                    {
                        var productArr = data.split(",");
                        var dom = "";
                        $.each(productArr, function(key, val){
                            dom += "<span class=\"label label-primary\">"+val+"</span>&nbsp;";
                        });
                    return dom;
                    }
                },
                 {
                    data: "is_active",
                    render: function (data, type, full, meta) 
                    {
                        var is_active = (data == 1) ? '<span class="label label-primary">ACTIVE</span>' : '<span class="label label-danger">DEACTIVE</span>';
                        // console.log(is_active);

                        // var dom = "<span class=\"label label-primary\">"+is_active+"</span>&nbsp;";
                        return is_active;
                    }
                },

                {
                    data: "id",
                    render: function (data, type, full, meta) {
                        return "<a href=\""+baseUrl+"/customers/edit/"+data+"\" class=\"btn-edit btn btn-primary btn-xs btn-outline\" data-id="+data+"><i class=\"fa fa-edit\"></i></a>"
                        +"&nbsp;"+
                        "<a href=\""+baseUrl+"/customers/del/"+data+"\" class=\"btn-del btn btn-danger btn-xs btn-outline\" data-id="+data+" onclick=\"return confirm('You are about to delete a record. This cannot be undone. Are you sure?');\"><i class=\"fa fa-remove\"></i></a>";
                    }
                }
            ],
            pageLength: 10,
            responsive: true,
            dom: '<"html5buttons"B>lTfgitp',
            buttons: [
                { extend: 'copy'},
                {extend: 'csv'},
                {extend: 'excel', title: 'ExampleFile'},
                {extend: 'pdf', title: 'ExampleFile'},

                {extend: 'print',
                 customize: function (win){
                        $(win.document.body).addClass('white-bg');
                        $(win.document.body).css('font-size', '10px');

                        $(win.document.body).find('table')
                                .addClass('compact')
                                .css('font-size', 'inherit');
                }
                }
            ],
            destroy: true   // https://datatables.net/manual/tech-notes/3 [bugfix - call in fn and realaod]
        });
    }