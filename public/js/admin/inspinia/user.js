$(document).ready(function() {
	
	console.log('User Checked!');

    userList();

    // SAVE
    $('#form-user').on('submit', function(e)
	{
		e.preventDefault();

		// console.log('User FORM Checked!');

		/*
		var id = $('#id').val();
		alert(id)
		var action = $('.btn-form-submit').text();
		if (action === 'SAVE' && id=='') {
			alert('sa..');
		} 
		else{
			alert('up..');
		}
		*/

		
		var url="http://localhost:85/warehouse/inspinia/store";

		$.ajax({
			url: url,
			type: "POST",
			dataType: "JSON",
			data: new FormData(this),
			processData: false,
			contentType: false,
			success: function (res)
			{
				console.log('User FORM Ajax Checked!');
				console.log(res);
				// $('#vendor_form').trigger("reset");
				// $("#tbl-vendor tbody").empty();
				// getVendors();

			},
			error: function (xhr, textStatus, thrownError) {
		        var msg = 'Sorry but there was an error: ';
				$('#error').html(msg + xhr.status + ' ' + xhr.statusText); 
				console.log(xhr.status);
				console.log(textStatus);
				console.log(thrownError);
		    }
		});        
	});

	$('.btn-edit').on('click', function(e){
	    
	    e.preventDefault();

	    var id = $(this).data("id");

	    // var url = 'http://localhost:85/warehouse/inspinia/ajax_get_users/'+id;
		
		// console.log(id);
		// console.log(url);

		$.ajax({
			type: 'GET',
			url: 'http://localhost:85/warehouse/inspinia/ajax_get_users/'+id,  
			dataType: 'json',
			// data:{id:id},
			async: false,
			cache: false,
		    success: function (data, textStatus, xhr) {
		    	// console.log(data.data[0].name);
		    	
		    	$('#id').val(data.data.id);
		    	$('#name').val(data.data.name);
		    	$('#mobile').val(data.data.mobile);
		    	$('#email').val(data.data.email);
		    	$('#address').val(data.data.address);
				$('.form-title').text('User Edit');
				$('.btn-form-submit').text('UPDATE');
				// $('#box p').html(data);form-title
				// $('#error').html('');
		    },
		    error: function (xhr, textStatus, thrownError) {
		        var msg = 'Sorry but there was an error: ';
				$('#error').html(msg + xhr.status + ' ' + xhr.statusText); 
				console.log(xhr.status);
				console.log(textStatus);
				console.log(thrownError);
				$('#box p').empty();	
		    }
		});	
	});

	$('.btn-del').on('click', function(e){
	    
	    e.preventDefault();

	    var id = $(this).data("id");

	    var url = 'http://localhost:85/warehouse/inspinia/ajax_del_user/'+id;
		
		// console.log(id);
		console.log(url);
	/*
		$.ajax({
			type: 'POST',
			url: 'http://localhost:85/warehouse//inspinia/ajax_del_user',  
			dataType: 'json',
			data:{id:id},
			async: false,
			cache: false,
		    success: function (data, textStatus, xhr) {
		    	// console.log(data.data[0].name);
		    	
		    	// console.log(data);
		    	$('#id').val(data.data.id);
		    	$('#name').val(data.data.name);
		    	$('#mobile').val(data.data.mobile);
		    	$('#email').val(data.data.email);
		    	$('#address').val(data.data.address);
				// $('#box p').html(data);
				// $('#error').html('');
		    },
		    error: function (xhr, textStatus, thrownError) {
		        var msg = 'Sorry but there was an error: ';
				$('#error').html(msg + xhr.status + ' ' + xhr.statusText); 
				console.log(xhr.status);
				console.log(textStatus);
				console.log(thrownError);
				$('#box p').empty();	
		    }
		});
	*/	
	});

});

function userList() {
	$.ajax({
		type: 'GET',
		url: 'http://localhost:85/warehouse/inspinia/ajax_get_users',  
		dataType: 'json',
		async: false,
		cache: false,
	    success: function (data, textStatus, xhr) {
	    	// console.log(data.data[0].name);
	    	console.log(data);
	    	$.each(data.data, function(entryIndex, entry){
									
				var html = "";
				html += "<tr>";
                    html += "<td>1</td>";
                    html += "<td>"+entry['name']+"</td>";
                    html += "<td>"+entry['mobile']+"</td>";
                    html += "<td>"+entry['email']+"</td>";
                    html += "<td>"+entry['address']+"</td>";
                    html += "<td>";

                    	html += "<a href=\"http://localhost:85/warehouse/inspinia/user/"+entry['id']+"\" class=\"btn-edit\" data-id="+entry['id']+"><i class=\"fa fa-edit\"></i></a>";
                    	html += "&nbsp;&nbsp;";
                    	html += "<a href=\"http://localhost:85/warehouse/inspinia/user/"+entry['id']+"\" class=\"btn-del\" data-id="+entry['id']+" onclick=\"return confirm('You are about to delete a record. This cannot be undone. Are you sure?');\"><i class=\"fa fa-remove\"></i></a>";
                    html += "</td>";
                html += "</tr>";

				$('.tbl-user tbody').append(html);  
			});	
			// $('#box p').html(data);
			// $('#error').html('');
	    },
	    error: function (xhr, textStatus, thrownError) {
	        var msg = 'Sorry but there was an error: ';
			$('#error').html(msg + xhr.status + ' ' + xhr.statusText); 
			console.log(xhr.status);
			console.log(textStatus);
			console.log(thrownError);
			$('#box p').empty();	
	    }
	});	
}