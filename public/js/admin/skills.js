$(document).ready(function() {
	// console.log('Checked! [admin/block.js]');
    // Datatable Load - Users - index()
        skillsList();

    // Edit - Show user data by ID
        var id = window.location.href.substring(window.location.href.lastIndexOf('/') + 1);
        // console.log('Here am I Findng...!'+id);
        if(typeof(id) != "undefined" && id !== null && (/^[0-9]+(?:\.[0-9]+)?$/.test(id))) {
            // console.log('SUCCESS..!'+id);
              get_skills_by_id();
        }

    // SAVE
        $(document).on('click', '#form-skills', function() {
            // e.preventDefault();
            // console.log('prevented!');
            var form = $("#form-skills");
            $(form).validate({
                rules: {
                    'title': {
                        required: true,
                    },
                    'description': {
                        required: true,
                         minlength: 4
                    },
                     'progress': {
                        required: true,
                    }
                },
                submitHandler: function(form) {
                    var url = baseUrl+"/skills/store";
                    $.ajax({
                        url: url,
                        type: "POST",
                        dataType: "JSON",
                        data: new FormData(form),
                        processData: false,
                        contentType: false,
                        cache: false,
                        async: false,
                        success: function (res)
                        {
                            // console.log(res);
                            if (res.status === 200 || res.status === 201) {
                                toast_notify('success',res.msg);
                            } 
                            else if (res.status === 503 || res.status === 400) {
                                toast_notify('error',res.msg);
                            }
                            $('#form-skills').trigger("reset");
                            $(".dataTables-skills").empty();
                             skillsList(); 
                        },
                        error: function (xhr, textStatus, thrownError) {
                            var msg = 'Sorry but there was an error: ';
                            $('#error').html(msg + xhr.status + ' ' + xhr.statusText); 
                            console.log(xhr.status);
                            console.log(textStatus);
                            console.log(thrownError);
                        }
                    });
                }
            });   
        });
    
    // Delete
        $(document).on('click', '.btn-del', function(e){
            e.preventDefault();
            var id = $(this).data("id");
            var url = baseUrl+"/skills/delete";        
            $.ajax({
                url: url,
                type: "POST",
                dataType: "JSON",
                data: {id:id},
                success: function (res)
                {
                    // console.log(res);
                    toast_notify('error',res.msg);
                    $(".dataTables-skills").empty();
                    skillsList();   
                },
                error: function (xhr, textStatus, thrownError) {
                    var msg = 'Sorry but there was an error: ';
                    $('#error').html(msg + xhr.status + ' ' + xhr.statusText); 
                    console.log(xhr.status);
                    console.log(textStatus);
                    console.log(thrownError);
                }
            });
        });
});

// Fn - get Skills data by id
function get_skills_by_id(id) {
    var id = window.location.href.substring(window.location.href.lastIndexOf('/') + 1);
    var url = baseUrl+"/skills/ajax_get_skills/"+id;
    $.ajax({
        type: 'GET',
        url: url,  
        dataType: 'json',
        success: function (data, textStatus, xhr) {
            // console.log(data);
            $('#id').val(data.data.id);
            $('#title').val(data.data.title);
            $('#description').val(data.data.description);
            $('#progress').val(data.data.progress);
            $('#bg_color').val(data.data.bg_color);
                
            // Selected TRUE
            $('#is_active').val(data.data.is_active);
            if (data.data.is_active == 0) {
                $("input[name='is_active']").prop('checked', false);
            }
            else {
                $("input[name='is_active']").prop('checked', true);
            }

            // iCheck data update
            $('.i-checks').iCheck('update');

            $('.form-title').text('Skills Edit');
            $('.btn-form-submit').text('UPDATE');
        },
        error: function (xhr, textStatus, thrownError) {
            var msg = 'Sorry but there was an error: ';
            $('#error').html(msg + xhr.status + ' ' + xhr.statusText); 
            console.log(xhr.status);
            console.log(textStatus);
            console.log(thrownError);  
        }
    });
}

// for showing block table data
function skillsList() {
    $('.dataTables-skills').DataTable({
        "ajax": baseUrl+"/skills/ajax_get_skills",
        "columns": [
            { "data": "id" },
            { "data": "title" },
            { "data": "description" },
            { "data": "progress" },
            {
                data: "bg_color",
                render: function (data, type, full, meta) 
                {
                    var bg_color = "<span class=\"label label-"+data+"\">"+data+"</span>";
                    // console.log(is_active);
                    // var dom = "<span class=\"label label-primary\">"+is_active+"</span>&nbsp;";
                    return bg_color;
                }
            },
            {
                data: "is_active",
                render: function (data, type, full, meta) 
                {
                    var is_active = (data == 1) ? '<span class="label label-primary">ACTIVE</span>' : '<span class="label label-danger">DEACTIVE</span>';
                    // console.log(is_active);
                    // var dom = "<span class=\"label label-primary\">"+is_active+"</span>&nbsp;";
                    return is_active;
                }
            },
            {
                data: "id",
                render: function (data, type, full, meta) {
                    return "<a href=\""+baseUrl+"/skills/edit/"+data+"\" class=\"btn-edit btn btn-primary btn-xs btn-outline\" data-id="+data+"><i class=\"fa fa-edit\"></i></a>"
                    +"&nbsp;"+
                    "<a href=\""+baseUrl+"/skills/del/"+data+"\" class=\"btn-del btn btn-danger btn-xs btn-outline\" data-id="+data+" onclick=\"return confirm('You are about to delete a record. This cannot be undone. Are you sure?');\"><i class=\"fa fa-remove\"></i></a>";
                }
            }
        ],
        pageLength: 10,
        responsive: true,
        dom: '<"html5buttons"B>lTfgitp',
        buttons: [
            { extend: 'copy'},
            {extend: 'csv'},
            {extend: 'excel', title: 'ExampleFile'},
            {extend: 'pdf', title: 'ExampleFile'},
            {extend: 'print',
             customize: function (win){
                    $(win.document.body).addClass('white-bg');
                    $(win.document.body).css('font-size', '10px');

                    $(win.document.body).find('table')
                            .addClass('compact')
                            .css('font-size', 'inherit');
                }
            }
        ],
        destroy: true   // https://datatables.net/manual/tech-notes/3 [bugfix - call in fn and realaod]
    });
}




