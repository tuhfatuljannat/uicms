

$(document).ready(function() {
	
	console.log('Checked! [admin/bulkunit.js]');

    // Datatable Load - Users - index()
    bulkunitList();
    typeList();


    // Edit - Show user data by ID
    	$(document).on('click', '.btn-edit', function(e) {

    	e.preventDefault();

    	var id = $(this).data("id");
    	get_bulkunit_by_id(id);
    	});


    // SAVE
    $(document).on('click', '#form-bulkunit', function() {

            // e.preventDefault();
            // console.log('prevented!');
            var form = $("#form-bulkunit");

            $(form).validate({
            	rules: {
            		'name': {
            			required: true,
            		},
            		'code': {
            			required: true,

            		},
            		'type_id': {
            			required: true,
            		}
            	},
            	submitHandler: function(form) {

            		var url = baseUrl+"/bulkunit/store";

            		$.ajax({
            			url: url,
            			type: "POST",
            			dataType: "JSON",
            			data: new FormData(form),
            			processData: false,
            			contentType: false,
            			cache: false,
            			async: false,
            			success: function (res)
            			{
                            // console.log(res);
                            if (res.status === 200 || res.status === 201) {
                            	toast_notify('success',res.msg);
                            } 
                            else if (res.status === 503 || res.status === 400) {
                            	toast_notify('error',res.msg);
                            }
                            bulkunitList(); 
                            // toast_notify('success',res.msg);
                            $("#form-bulkunit")[0].reset();
                        },
                        error: function (xhr, textStatus, thrownError) {
                        	var msg = 'Sorry but there was an error: ';
                        	$('#error').html(msg + xhr.status + ' ' + xhr.statusText); 
                        	console.log(xhr.status);
                        	console.log(textStatus);
                        	console.log(thrownError);
                        }
                    });
            	}
            });   
        });
    
    // Delete
    $(document).on('click', '.btn-del', function(e){

    	e.preventDefault();
    	var id = $(this).data("id");
    	var url = baseUrl+"/bulkunit/delete";

    	$.ajax({
    		url: url,
    		type: "POST",
    		dataType: "JSON",
    		data: {id:id},
    		success: function (res)
    		{
                    // console.log(res);
                    toast_notify('error',res.msg);
                    $(".dataTables-bulkunit").empty();
                    bulkunitList();

                },
                error: function (xhr, textStatus, thrownError) {
                	var msg = 'Sorry but there was an error: ';
                	$('#error').html(msg + xhr.status + ' ' + xhr.statusText); 
                	console.log(xhr.status);
                	console.log(textStatus);
                	console.log(thrownError);
                }
            });
    });
});



// Fn - get  data by id
function get_bulkunit_by_id(id) {

    // var id = window.location.href.substring(window.location.href.lastIndexOf('/') + 1);
    var url = baseUrl+"/bulkunit/ajax_get_bulkunit/"+id;

    $.ajax({
    	type: 'GET',
    	url: url,  
    	dataType: 'json',
    	success: function (data, textStatus, xhr) {

            // console.log(data);

            $('#id').val(data.data.id);
            $('#name').val(data.data.name);
            $('#code').val(data.data.code);
            $('#type_id').val(data.data.type_id);


            // Selected TRUE
            $('#is_active').val(data.data.is_active);
            if (data.data.is_active == 0) {
            	$("input[name='is_active']").prop('checked', false);
            }
            else {
            	$("input[name='is_active']").prop('checked', true);
            }

            // iCheck data update
            $('.i-checks').iCheck('update');

            $('.form-title').text('bulkunit Edit');
            $('.btn-form-submit').text('UPDATE');
        },
        error: function (xhr, textStatus, thrownError) {
        	var msg = 'Sorry but there was an error: ';
        	$('#error').html(msg + xhr.status + ' ' + xhr.statusText); 
        	console.log(xhr.status);
        	console.log(textStatus);
        	console.log(thrownError);  
        }
    });
}


//Getting value from type
function typeList() {

	$.ajax({

		type: 'GET',
		url: baseUrl+'/type/ajax_get_type',  
		dataType: 'json',
		async: false,
		cache: false,

		success: function (data, textStatus, xhr) {

             // console.log(data.data[0].name);

             // console.log(data);
             $.each(data.data, function(entryIndex, entry){


             	var html = '';
                     // console.log(entryIndex); 
                     // console.log(entry['name']);  

                     html += "<option value=\""+entry.id+"\">" + entry.name + "</option>";  
                     $('#type_id').append(html);
                 }); 
             // $('#box p').html(data);

            // $('#error').html('');

        },

        error: function (xhr, textStatus, thrownError) {

        	var msg = 'Sorry but there was an error: ';

        	$('#error').html(msg + xhr.status + ' ' + xhr.statusText); 

        	console.log(xhr.status);

        	console.log(textStatus);
        	console.log(thrownError);

        	$('#box p').empty();    

        }

    }); 

}

// for showing bulkunit table data
function bulkunitList() {
	$('.dataTables-bulkunit').DataTable({
		"ajax": baseUrl+"/bulkunit/ajax_get_bulkunit",
		"columns": [
		{ "data": "id" },
		{ "data": "name" },
		{ "data": "code" },
		{ "data": "u_typename" },



		{
			data: "is_active",
			render: function (data, type, full, meta) 
			{
				var is_active = (data == 1) ? '<span class="label label-primary">ACTIVE</span>' : '<span class="label label-danger">DEACTIVE</span>';
                    // console.log(is_active);

                    // var dom = "<span class=\"label label-primary\">"+is_active+"</span>&nbsp;";
                    return is_active;
                }
            },

            {
            	data: "id",
            	render: function (data, type, full, meta) {
            		return "<a href=\""+baseUrl+"/bulkunit/"+data+"\" class=\"btn-edit btn btn-primary btn-xs btn-outline\" data-id="+data+"><i class=\"fa fa-edit\"></i></a>"
            		+"&nbsp;"+
            		"<a href=\""+baseUrl+"/bulkunit/del/"+data+"\" class=\"btn-del btn btn-danger btn-xs btn-outline\" data-id="+data+" onclick=\"return confirm('You are about to delete a record. This cannot be undone. Are you sure?');\"><i class=\"fa fa-remove\"></i></a>";
            	}
            }
            ],
            pageLength: 10,
            responsive: true,
            dom: '<"html5buttons"B>lTfgitp',
            buttons: [
            { extend: 'copy'},
            {extend: 'csv'},
            {extend: 'excel', title: 'ExampleFile'},
            {extend: 'pdf', title: 'ExampleFile'},

            {extend: 'print',
            customize: function (win){
            	$(win.document.body).addClass('white-bg');
            	$(win.document.body).css('font-size', '10px');

            	$(win.document.body).find('table')
            	.addClass('compact')
            	.css('font-size', 'inherit');
            }
        }
        ],
        destroy: true   // https://datatables.net/manual/tech-notes/3 [bugfix - call in fn and realaod]
    });
}

