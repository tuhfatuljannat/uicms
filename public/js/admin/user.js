$(document).ready(function() {

    // userList();
    
    var id = window.location.href.substring(window.location.href.lastIndexOf('/') + 1);
    // var id = $('#id').val();
    // alert(id);
    var url = baseUrl+"/user/ajax_get_user/"+id;

    $.ajax({
        type: 'GET',
        url: url,  
        dataType: 'json',
        success: function (data, textStatus, xhr) {
            
            // console.log(data.data[0].file_name);
            console.log(data.data);

            $('#id').val(data.data.id);
            $('#name').val(data.data.name);
            $('#username').val(data.data.username);
            $('#email').val(data.data.email);
            
            // Selected TRUE
            // $('#is_active').val(data.data.is_active);
            // if (data.data.is_active == 0) {
            //     $("input[name='is_active']").prop('checked', false);
            // }
            // else {
            //     $("input[name='is_active']").prop('checked', true);
            // }

            // iCheck data update
            // $('.i-checks').iCheck('update');
            var src = baseUrl+"/public/file_manager/"+data.data.file_name; 
            $('#preview').html("<img src=\""+src+"\">");  
            
        },
        error: function (xhr, textStatus, thrownError) {
            var msg = 'Sorry but there was an error: ';
            $('#error').html(msg + xhr.status + ' ' + xhr.statusText); 
            console.log(xhr.status);
            console.log(textStatus);
            console.log(thrownError);  
        }
    });


    // var id = window.location.href.substring(window.location.href.lastIndexOf('/') + 1);
    // var url = baseUrl+"/user/ajax_get_user_info/"+id;

    // $.ajax({
    //     type: 'GET',
    //     url: url,  
    //     dataType: 'json',
    //     success: function (data, textStatus, xhr) {
            
    //         console.log(data);

    //         $('#id').val(data.data.id);
    //         $('#name').val(data.data.name);
    //         $('#username').val(data.data.username);
    //         $('#email').val(data.data.email);
            
    //         // Selected TRUE
    //         // $('#is_active').val(data.data.is_active);
    //         // if (data.data.is_active == 0) {
    //         //     $("input[name='is_active']").prop('checked', false);
    //         // }
    //         // else {
    //         //     $("input[name='is_active']").prop('checked', true);
    //         // }

    //         // iCheck data update
    //         // $('.i-checks').iCheck('update');
    //         // var src = baseUrl+"/public/file_manager/"+data.data.file_name; 
    //         // $('#preview').html("<img src=\""+src+"\">");  
    //         // $('.form-user').text('User Edit');
    //         // $('.btn-form-submit').text('UPDATE');
    //     },
    //     error: function (xhr, textStatus, thrownError) {
    //         var msg = 'Sorry but there was an error: ';
    //         $('#error').html(msg + xhr.status + ' ' + xhr.statusText); 
    //         console.log(xhr.status);
    //         console.log(textStatus);
    //         console.log(thrownError);  
    //     }
    // });
        
         

    // SAVE
        $(document).on('click', '#form-user', function() {

            // e.preventDefault();
            // console.log('prevented!');
            var form = $("#form-user");

            $(form).validate({
                rules: {
                    // 'company_name': {
                    //     required: true,
                    // }
                },
                submitHandler: function(form) {

                    var url = baseUrl+"/user/store";
                    
                    $.ajax({
                        url: url,
                        type: "POST",
                        dataType: "JSON",
                        data: new FormData(form),
                        processData: false,
                        contentType: false,
                        cache: false,
                        async: false,
                        success: function (res)
                        {
                            // console.log(res);
                            get_user_info(id);

                            if (res.status === 200 || res.status === 201) {
                                toast_notify('success',res.msg);
                            } 
                            else if (res.status === 503 || res.status === 400) {
                                toast_notify('error',res.msg);
                            }
                            // $('#form-user').trigger("reset");
                            // $(".dataTables-about").empty();
                             // aboutList(); 
                        },
                        error: function (xhr, textStatus, thrownError) {
                            var msg = 'Sorry but there was an error: ';
                            $('#error').html(msg + xhr.status + ' ' + xhr.statusText); 
                            console.log(xhr.status);
                            console.log(textStatus);
                            console.log(thrownError);
                        }
                    });
                }
            });   
        });
    
    
});

// function userList(){
//     $.ajax({

//         type: 'GET',
//         url: baseUrl+'/user/ajax_get_user',  
//         dataType: 'json',
//         async: false,
//         cache: false,
//         success: function (data, textStatus, xhr) {

//             // console.log(data.data[0].name);
//             // console.log(data);
//             // console.log(data.data[0].mobile);   
//             // $('#headings').text(data.data[0].company_name);
//             // $('#company_description').text(data.data[0].company_desc);   
//             // $('#about_des').text(data.data[0].about_intro); 
//             // $('#service-desc').text(data.data[0].service_intro);
//             // $('#tem_intro').text(data.data[0].team_intro);
//             // $('#contact_intro').text(data.data[0].contact_intro);  
//             // $('#address').text(data.data[0].company_addr); 
//             // $('#company_ad').text(data.data[0].company_addr);
//             // $('#company_m').text("Phone : "+data.data[0].company_mobile);
//             // $('#company_e').text("Email :"+data.data[0].company_email);
//             // $("#company_mobile").prop("href",data.data[0].company_mobile).text(data.data[0].company_mobile);
//             // $("#company_email").prop("href",+"mailto:"+data.data[0].company_email).text(data.data[0].company_email);
//         },
//         error: function (xhr, textStatus, thrownError) {

//             var msg = 'Sorry but there was an error: ';
//             $('#error').html(msg + xhr.status + ' ' + xhr.statusText); 
//             console.log(xhr.status);
//             console.log(textStatus);
//             console.log(thrownError);
//             // $('#box p').empty();    

//         }

//     }); 

// }








