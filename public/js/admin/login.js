$(document).ready(function(){
    
     console.log('Checked! [admin/login.js]');

   $(document).on('click', '#login', function() {
            // e.preventDefault();
            // console.log('prevented!');
            var form = $("#login");
            $(form).validate({
                rules: {
                    'email': {
                        required: true,
                    }
                },
                submitHandler: function(form) {
                   
                    var url = baseUrl+"/login/validate_login";
                    $.ajax({
                        url: url,
                        type: "POST",
                        dataType: "JSON",
                        data: new FormData(form),
                        processData: false,
                        contentType: false,
                        cache: false,
                        async: false,
                        success: function (res)
                        {
                            // console.log(res);
                            // alert('success');
                            // rest of da code
                            // redirect
                            if (res.status === 200 || res.status === 201) {
                                toast_notify('success',res.msg);
                                setTimeout(function(){
                                    window.location.href = baseUrl + '/slides';
                                },2100);
                            } 
                            else if (res.status === 503 || res.status === 400) {
                                toast_notify('error',res.msg);
                            }
                           
                        },
                        error: function (xhr, textStatus, thrownError) {
                            var msg = 'Sorry but there was an error: ';
                            $('#error').html(msg + xhr.status + ' ' + xhr.statusText); 
                            console.log(xhr.status);
                            console.log(textStatus);
                            console.log(thrownError);
                            // alert('error');

                        }
                    });
                }
            });   
    });
});

    