 
$(document).ready(function() {

    console.log('Checked! [admin/brand.js]');

    // Datatable Load - brand - index()
    brandList();

    // Edit - Show brand data by ID
    $(document).on('click', '.btn-edit', function(e) {

        e.preventDefault();

        var id = $(this).data("id");
        get_brand_by_id(id);
    });
    
    // SAVE
        $(document).on('click', '#form-brand', function(e) {

        var form = $("#form-brand");

        $(form).validate({
            rules: {
                'name': {
                    required: true
                },
                'description': {
                    required: true
                    
                }
            },
            submitHandler: function(form) {

                e.preventDefault();
                var url = baseUrl+"/brand/store";

                $.ajax({
                    url: url,
                    type: "POST",
                    dataType: "JSON",
                    data: new FormData(form),
                    processData: false,
                    contentType: false,
                    cache: false,
                    async: false,
                    success: function (res)
                    {
                        if (res.status === 200 || res.status === 201) {
                            brandList();
                            toast_notify('success',res.msg);
                            $("#form-brand")[0].reset();
                        } 
                        else if (res.status === 503 || res.status === 400) {
                            toast_notify('error',res.msg);
                        } 
                    },
                    error: function (xhr, textStatus, thrownError) {
                        var msg = 'Sorry but there was an error: ';
                        $('#error').html(msg + xhr.status + ' ' + xhr.statusText); 
                        console.log(xhr.status);
                        console.log(textStatus);
                        console.log(thrownError);
                    }
                });
            }
        });   
    });


    // Delete
    
         $(document).on('click', '.btn-del', function(e){

        e.preventDefault();
        var id = $(this).data("id");
        var url = baseUrl+"/brand/delete";

        $.ajax({
            url: url,
            type: "POST",
            dataType: "JSON",
            data: {id:id},
            success: function (res)
            {
                console.log(res);
                toast_notify('error',res.msg);
                $(".dataTables-brand").empty();
                brandList();

            },
            error: function (xhr, textStatus, thrownError) {
                var msg = 'Sorry but there was an error: ';
                $('#error').html(msg + xhr.status + ' ' + xhr.statusText); 
                console.log(xhr.status);
                console.log(textStatus);
                console.log(thrownError);
            }
        });
    });

});


// Fn - get userdata by id
function get_brand_by_id(id) {

    var url = baseUrl+"/brand/ajax_get_brand/"+id;

    $.ajax({
        type: 'GET',
        url: url,  
        dataType: 'json',
        success: function (data, textStatus, xhr) {

            $('#id').val(data.data.id);
            $('#name').val(data.data.name);
            $('#description').val(data.data.description);
            
           

            $('#is_active').val(data.data.is_active);
            if (data.data.is_active == 0) {
                $("input[name='is_active']").prop('checked', false);
            }
            else {
                $("input[name='is_active']").prop('checked', true);
            }

            // iCheck data update
            $('.i-checks').iCheck('update');
            

            $('.form-title').text('brand Edit');
            $('.btn-form-submit').text('UPDATE');
        },
        error: function (xhr, textStatus, thrownError) {
            var msg = 'Sorry but there was an error: ';
            $('#error').html(msg + xhr.status + ' ' + xhr.statusText); 
            console.log(xhr.status);
            console.log(textStatus);
            console.log(thrownError);  
        }
    });
}

// brandList

function brandList() {
    $('.dataTables-brand').DataTable({
        "ajax": baseUrl+"/brand/ajax_get_brand",
        "columns": [
        { "data": "id" },
        { "data": "name" },
        { "data": "description" },
       
        {
            data: "is_active",
            render: function (data, type, full, meta) 
            {
                var is_active = (data == 1) ? '<span class="label label-primary">ACTIVE</span>' : '<span class="label label-danger">DEACTIVE</span>';
                    // console.log(is_active);

                    // var dom = "<span class=\"label label-primary\">"+is_active+"</span>&nbsp;";
                    return is_active;
                }
            },

            {
                data: "id",
                render: function (data, type, full, meta) {
                    return "<a href=\""+baseUrl+"/brand/"+data+"\" class=\"btn-edit btn btn-primary btn-xs btn-outline\" data-id="+data+"><i class=\"fa fa-edit\"></i></a>"
                    +"&nbsp;"+
                    "<a href=\""+baseUrl+"/brand/del/"+data+"\" class=\"btn-del btn btn-danger btn-xs btn-outline\" data-id="+data+" onclick=\"return confirm('You are about to delete a record. This cannot be undone. Are you sure?');\"><i class=\"fa fa-remove\"></i></a>";
                }
            }
            ],
            pageLength: 10,
            responsive: true,
            dom: '<"html5buttons"B>lTfgitp',
            buttons: [
            { extend: 'copy'},
            {extend: 'csv'},
            {extend: 'excel', title: 'ExampleFile'},
            {extend: 'pdf', title: 'ExampleFile'},

            {extend: 'print',
            customize: function (win){
                $(win.document.body).addClass('white-bg');
                $(win.document.body).css('font-size', '10px');

                $(win.document.body).find('table')
                .addClass('compact')
                .css('font-size', 'inherit');
            }
        }
        ],
        destroy: true   // https://datatables.net/manual/tech-notes/3 [bugfix - call in fn and realaod]
    });
}
