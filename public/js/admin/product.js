$(document).ready(function() {	
	// console.log('Checked! [admin/product.js]');

    // Datatable Load - Products - index()
        productList();

    // Edit - Show product data by ID
        var id = window.location.href.substring(window.location.href.lastIndexOf('/') + 1);
        // console.log('Here am I Findng...!'+id);
        if(typeof(id) != "undefined" && id !== null && (/^[0-9]+(?:\.[0-9]+)?$/.test(id))) {
            // console.log('SUCCESS..!'+id);
            get_product_by_id();
        }

    // hide and show 
	    $("input[name='product_type']").click(function () {
            $('.form-title').text('ADD PART (RAW MATERIAL)');
            if ($("#finished_good").is(":checked")) {
                $("#even").show();
            } else {
                $("#even").hide();
            }
        });

    // add  custom field
        var max_fields_limit = 8; 
        var x = 1; 
        $('.add_field').click(function(e){ 
            e.preventDefault();
            if(x < max_fields_limit){ 
                x++; 
                $('.input_field').append('<div><textarea class="form-control" name="add_custom_field[]" id="add_custom_field[]" /><a class="btn-remove btn btn-danger btn-xs btn-outline" style="margin-left:314px;margin-top:-95px;"><i class="fa fa-remove"></i></a></div>'); //add input field
            }
        }); 

    // add  buiding parts input field
        var max_fields_limit = 8; 
        var x = 1; 
        $('.add_input_field').click(function(e){ 
            e.preventDefault();
            if(x < max_fields_limit){ 
                x++; 
                $('.building_part_input').append('<div><input type="text" class="form-control" placeholder="Part Name" name="building_parts[]" id ="building_parts[]"/><a class="btn-remove btn btn-danger btn-xs btn-outline" style="margin-left:233px;margin-top:-59px;"><i class="fa fa-remove"></i></a></div>'); //add input field
            }
        });

    // add  parts input field
        var max_fields_limit = 8; 
        var x = 1; 
        $('.add_part_input_field').click(function(e){ 
            e.preventDefault();
            if(x < max_fields_limit){ 
                x++; 
                $('.part_input').append('<div><input type="text" class="form-control" placeholder="Part Name" name="add_parts[]" id ="add_parts[]"/><a class="btn-remove btn btn-danger btn-xs btn-outline" style="margin-left:233px;margin-top:-59px;"><i class="fa fa-remove"></i></a></div>'); //add input field
            }
        });

    // add  service parts input field
        var max_fields_limit = 8; 
        var x = 1; 
        $('.add_service_input_field').click(function(e){ 
            e.preventDefault();
            if(x < max_fields_limit){ 
                x++; 
                $('.service_input').append('<div><input type="text" class="form-control" placeholder="Part Name" name="add_service[]" id ="add_service[]"/><a class="btn-remove btn btn-danger btn-xs btn-outline" style="margin-left:233px;margin-top:-59px;"><i class="fa fa-remove"></i></a></div>'); //add input field
            }
        });  
 

    //Once remove button is clicked
        $(wrapper).on('click', '.btn-remove', function(e){
            e.preventDefault();
            $(this).parent('div').remove();
            x--; 
        });

    //save
        $(document).on('click', '#product_form', function(e) {

            var form = $("#product_form");

            // $.validator.setDefaults({ ignore: ":hidden:not(.chosen-select)" });
            $(form).validate({
                rules: {
                     'name': {
                        required: true,
                    },
                     'code': {
                        required: true,
                    },
                     'product_type': {
                        required: true,
                    },
                     'type_id': {
                        required: true,
                    },
                    'category_id': {
                        required: true,
                    },
                    'sub_category_id': {
                        required: true,
                    },
                    'brand_id': {
                        required: true,
                    },
                    'unit_id': {
                        required: true,
                    },
                    'bulk_unit_id': {
                        required: true,
                    },
                    'dim_in[]': {
                        required: true,
                    },
                    'dim_op': {
                        required: true,
                    }
                },
                submitHandler: function(form) {

                    e.preventDefault();
                    var url = baseUrl+"/product/store";
                    
                    $.ajax({
                        url: url,
                        type: "POST",
                        dataType: "JSON",
                        data: new FormData(form),
                        processData: false,
                        contentType: false,
                        cache: false,
                        async: false,
                        success: function (res)
                        {
                            if (res.status === 200 || res.status === 201) {
                                toast_notify('success',res.msg);
                            } 
                            else if (res.status === 503 || res.status === 400) {
                                toast_notify('error',res.msg);
                            } 

                        },
                        error: function (xhr, textStatus, thrownError) {
                            var msg = 'Sorry but there was an error: ';
                            $('#error').html(msg + xhr.status + ' ' + xhr.statusText); 
                            console.log(xhr.status);
                            console.log(textStatus);
                            console.log(thrownError);
                        }
                    });
                }
            });   
        });

	

    $(document).on('keyup', '#dimentions3, #dimentions2', function() {

        var a = $("#dimentions1").val();
        var b = $("#dimentions2").val();
        var c = $("#dimentions3").val();

        if( $('#dimentions3').val() ){
            // console.log('3 inp');
            var result = multiply(a,b,c);
            $('#unit_label').text('Cubic Unit');
        }
        else {
            // console.log('2 inp');

            var result = multiply(a,b, -9999);
             $('#unit_label').text('Square Unit');
        }
        
        $('#dimentions_result ').val(result);
    });
    
    
    $(document).on('keyup', '#default_packaging3, #default_packaging2', function() {
        var a = $("#default_packaging1").val();
        var b = $("#default_packaging2").val();
        var c = $("#default_packaging3").val(); 
          if( $('#default_packaging3').val() ){
            // console.log('3 inp');
            var result = multiply(a,b,c);
            $('#packaging-label').text('Cubic Unit');
        }
        else {
            // console.log('2 inp');

            var result = multiply(a,b, -9999);
             $('#packaging-label').text('Square Unit');
        }
        $('#default_packaging').val(result); 
    });
    
    $(document).on('change', '#convert-unit', function() {
        var unitFrom = $('#dimentions_result').data('unit');
        var val = $('#dimentions_result').val();
        var unitTo = $(this).val();

        var test = convertUnit(unitFrom, unitTo, val);
        // console.log(test);
        $('#dimentions_result').val(test);
        

        if( !$('#dimentions3').val() ){
            $('#unit_label').text('s'+unitTo);
        }
        else {
            $('#unit_label').text('c'+unitTo);
        }
    });
       //default packeging
    $(document).on('change', '#convert_default_unit', function() {
        var unitFrom = $('#default_packaging').data('unit');
        var val = $('#default_packaging').val();
        var unitTo = $(this).val();

        var test = convertUnit(unitFrom, unitTo, val);
        // console.log(test);
        $('#default_packaging').val(test);
        

        if( !$('#default_packaging3').val() ){
            $('#packaging-label').text('s'+unitTo);
        }
        else {
            $('#packaging-label').text('c'+unitTo);
        }
    }); 
		
});

// Fn - get product by id
    function get_product_by_id() {
        var id = window.location.href.substring(window.location.href.lastIndexOf('/') + 1);
        var url = baseUrl+"/product/ajax_get_products/"+id;

        $.ajax({
            type: 'GET',
            url: url,  
            dataType: 'json',
            success: function (data, textStatus, xhr) {
                
                // console.log(data);

                $('#id').val(data.data.id);
                $('#name').val(data.data.name);
                $('#code').val(data.data.code);
                $('#type_id').val(data.data.type_id);
                // $('#product_type').val(data.data.product_type);
                $('#category_id').val(data.data.category_id);
                $('#sub_category_id').val(data.data.sub_category_id);
                $('#brand_id').val(data.data.brand_id);
                $('#unit_id').val(data.data.unit_id);
                $('#bulk_unit_id').val(data.data.bulk_unit_id);
                $('#dim_op').val(data.data.dim_op);
                $('#weight_id').val(data.data.weight_id);
                $('#pack_op').val(data.data.pack_op);
                $('#gross_weight_id').val(data.data.gross_weight_id);

                // radio button
                $("input[name='product_type'][value='"+data.data.product_type+"']").prop('checked', true);
                
                // dimention array
                var dim_in = data.data.dim_in;
                // console.log(dim_in);
                var dimArr = dim_in.split(",");
                // console.log(dimArr);

                // default packaging array
                var pack_in = data.data.pack_in;
                var pakArr = pack_in.split(",");
                // console.log(pakArr);


                // Building parts array
                var building_parts = data.data.building_parts;
                var buildingArr = building_parts.split(",");
                // console.log(buildingArr);
                // $.each(buildingArr, function(key, val){
                //     $("input[name='building_parts[]'][value='"+val+"']").prop('checked', true);
                // });



                // Add parts array
                var add_parts = data.data.add_parts;
                var partsArr = add_parts.split(",");
                // console.log(partsArr);



                // Add service array
                var add_service = data.data.add_service;
                var serviceArr = add_service.split(",");
                // console.log(serviceArr);


                // Add custom field array
                var add_custom_field = data.data.add_custom_field;
                var customArr = add_custom_field.split(",");
                // console.log(customArr);



                // iCheck data update
                $('.i-checks').iCheck('update');
                $('.form-title').text('User Edit');
                $('.btn-form-submit').text('UPDATE');
            },
            error: function (xhr, textStatus, thrownError) {
                var msg = 'Sorry but there was an error: ';
                $('#error').html(msg + xhr.status + ' ' + xhr.statusText); 
                console.log(xhr.status);
                console.log(textStatus);
                console.log(thrownError);  
            }
        });
    }

// productList
    function productList() {
        $('.dataTables-product').DataTable({
            "ajax": baseUrl+"/product/ajax_get_products",
            "columns": [
                { "data": "id" },
                { "data": "name" },
                { "data": "code" },
                { "data": "type_id" },
                { "data": "product_type" },
                { "data": "category_id" },
                { "data": "sub_category_id" },
                { "data": "brand_id" },
                { "data": "unit_id" },
                { "data": "bulk_unit_id" },
                {
                    data: "dim_in",
                    render: function (data, type, full, meta) {
                        var dimArr = data.split(",");
                        var dom = "";
                        $.each(dimArr, function(key, val){
                            dom += "<span class=\"label label-primary\">"+val+"</span>&nbsp;";
                        });
                        return dom;
                    }
                },
                { "data": "dim_op" },
                { "data": "weight_id" },
                {
                    data: "pack_in",
                    render: function (data, type, full, meta) {
                        var packArr = data.split(",");
                        var dom = "";
                        $.each(packArr, function(key, val){
                            dom += "<span class=\"label label-primary\">"+val+"</span>&nbsp;";
                        });
                        return dom;
                    }
                },
                { "data": "pack_op" },
                { "data": "gross_weight_id" },
                {
                    data: "building_parts",
                    render: function (data, type, full, meta) {
                        var buildingArr = data.split(",");
                        var dom = "";
                        $.each(buildingArr, function(key, val){
                            dom += "<span class=\"label label-primary\">"+val+"</span>&nbsp;";
                        });
                        return dom;
                    }
                },
                {
                    data: "add_parts",
                    render: function (data, type, full, meta) {
                        var partsArr = data.split(",");
                        var dom = "";
                        $.each(partsArr, function(key, val){
                            dom += "<span class=\"label label-primary\">"+val+"</span>&nbsp;";
                        });
                        return dom;
                    }
                },
                {
                    data: "add_service",
                    render: function (data, type, full, meta) {
                        var serviceArr = data.split(",");
                        var dom = "";
                        $.each(serviceArr, function(key, val){
                            dom += "<span class=\"label label-primary\">"+val+"</span>&nbsp;";
                        });
                        return dom;
                    }
                },
                {
                    data: "add_custom_field",
                    render: function (data, type, full, meta) {
                        var customArr = data.split(",");
                        var dom = "";
                        $.each(customArr, function(key, val){
                            dom += "<span class=\"label label-primary\">"+val+"</span>&nbsp;";
                        });
                        return dom;
                    }
                },
                {
                    data: "id",
                    render: function (data, type, full, meta) {
                        return "<a href=\""+baseUrl+"/product/edit/"+data+"\" class=\"btn-edit btn btn-primary btn-xs btn-outline\" data-id="+data+"><i class=\"fa fa-edit\"></i></a>"
                        +"&nbsp;"+
                        "<a href=\""+baseUrl+"/product/del/"+data+"\" class=\"btn-del btn btn-danger btn-xs btn-outline\" data-id="+data+" onclick=\"return confirm('You are about to delete a record. This cannot be undone. Are you sure?');\"><i class=\"fa fa-remove\"></i></a>";
                    }
                }
            ],
            pageLength: 10,
            responsive: true,
            dom: '<"html5buttons"B>lTfgitp',
            buttons: [
                { extend: 'copy'},
                {extend: 'csv'},
                {extend: 'excel', title: 'ExampleFile'},
                {extend: 'pdf', title: 'ExampleFile'},

                {extend: 'print',
                 customize: function (win){
                        $(win.document.body).addClass('white-bg');
                        $(win.document.body).css('font-size', '10px');

                        $(win.document.body).find('table')
                                .addClass('compact')
                                .css('font-size', 'inherit');
                }
                }
            ],
            destroy: true   // https://datatables.net/manual/tech-notes/3 [bugfix - call in fn and realaod]
        });
    }




function multiply(a, b, c)
{
    if (c == -9999) {
        var res = a * b ; 
        
    } 
    else {
       var res = a * b * c; 
    }
    
    return res;
}

// m, cm, mm, ft 

// m - cm 
// m - mm
// m - ft


// cm - m 
// cm - mm
// cm - ft
//  resM = 20 * 100
//  resCm = 

// data-unit ="m"  'cm'

function convertUnit(unitFrom, unitTo, val) {
    var result;
    if (unitFrom == 'm') 
    {
        result = convertM2others(unitTo, val);
    }
    else if(unitFrom=='cm')
    {
       result = convertCM2others(unitTo, val);
    }

    return result;
}


 function convertM2others(convertTo, val){

    var res;
    if (convertTo =='cm') 
    {
        res = val * 100;  
    } 
    else if (convertTo =='mm') 
    {
         res = val * 1000; 
    } 
    else if (convertTo =='ft') 
    {
      res = val * 3.281;   
    } 

    return res;
}

function convertCM2others(convertTo, val){

    var res;
    if (convertTo =='m') 
    {
        res = val * 0.01;  
    } 
    else if (convertTo =='mm') 
    {
        res = val * 10;
    } 
    else if (convertTo =='ft') 
    {
       res = val * 0.032808399; 
    } 
    return res;
}

function convertMM2others(convertTo, val){

    var res;
    if (convertTo =='m') 
    {
        res = val *  0.001;  
    } 
    else if (convertTo =='cm') 
    {
        res = val * 0.1;
    } 
    else if (convertTo =='ft') 
    {
       res = val *  0.0032808399; 
    } 
    return res;
}

function convertFT2others(convertTo, val){

    var res;
    if (convertTo =='m') 
    {
        res = val * 0.3048;  
    } 
    else if (convertTo =='cm') 
    {
        res = val * 30.48;
    } 
    else if (convertTo =='mm') 
    {
       res = val * 304.8; 
    } 
    return res;
}

