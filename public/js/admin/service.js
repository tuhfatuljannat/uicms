$(document).ready(function() {

    console.log('Checked! [admin/service.js]');

        // Datatable Load - Service - index()
        serviceList();
        categoryList();
     
$(document).on('change', '#category_id', function(){        
        
        // e.preventDefault();
        var pid = $(this).val();

        if(pid != '')
        {
            // alert(pid);
            // console.log(pid);
            $.ajax({
                url: baseUrl+'/service/ajax_get_subcategory/'+pid,
                type: 'GET',
                dataType: "JSON",
                 success: function (data, textStatus, xhr) {
                    var html = '';
                    $.each(data.data, function(entryIndex, entry){
                        html += "<option value=\""+entry.id+"\">" + entry.name + "</option>";
                       // $('#sub_category_id').append(html);  
                    }); 
                    // $('#sub_category_id').append(html);
                    $('#sub_category_id').html(html);
                },

                error: function (xhr, textStatus, thrownError) {

                    var msg = 'Sorry but there was an error: ';

                    $('#error').html(msg + xhr.status + ' ' + xhr.statusText); 

                    console.log(xhr.status);

                    console.log(textStatus);
                     console.log(thrownError);

                     // $('#box p').empty();    

                }

            }); 
        
        }

});
    
        
    // Edit - Show user data by ID
        var id = window.location.href.substring(window.location.href.lastIndexOf('/') + 1);
        // console.log('Here am I Findng...!'+id);
        if(typeof(id) != "undefined" && id !== null && (/^[0-9]+(?:\.[0-9]+)?$/.test(id))) {
            // console.log('SUCCESS..!'+id);
            get_service_by_id();
        }

    // SAVE
        $(document).on('click', '#form-service', function() {

            // e.preventDefault();
            // console.log('prevented!');
            var form = $("#form-service");

            $(form).validate({
                rules: {
                    'name': {
                        required: true,
                    },
                    'code': {
                        required: true,
                         
                         minlength: 4

                    },
                    'type_id': {
                        required: true,
                    },
                    
                    'category_id': {
                        required: true,
                    },
                     
                    'unit_id': {
                        required: true,
                    },
                },
                submitHandler: function(form) {

                    var url = baseUrl+"/service/store";
                    
                    $.ajax({
                        url: url,
                        type: "POST",
                        dataType: "JSON",
                        data: new FormData(form),
                        processData: false,
                        contentType: false,
                        cache: false,
                        async: false,
                        success: function (res)
                        {
                            console.log(res);
                            if (res.status === 200 || res.status === 201) {
                                toast_notify('success',res.msg);
                            } 
                            else if (res.status === 503 || res.status === 400) {
                                toast_notify('error',res.msg);
                            }
                            $('#form-service').trigger("reset");
                            $(".dataTables-service").empty();
                            serviceList(); 
                        },
                        error: function (xhr, textStatus, thrownError) {
                            var msg = 'Sorry but there was an error: ';
                            $('#error').html(msg + xhr.status + ' ' + xhr.statusText); 
                            console.log(xhr.status);
                            console.log(textStatus);
                            console.log(thrownError);
                        }
                    });
                }
            });   
        });
    
    // Delete
        $(document).on('click', '.btn-del', function(e){

            e.preventDefault();
            var id = $(this).data("id");
            var url = baseUrl+"/service/delete";
                        
            $.ajax({
                url: url,
                type: "POST",
                dataType: "JSON",
                data: {id:id},
                success: function (res)
                {
                    console.log(res);
                    toast_notify('error',res.msg);
                     $(".dataTables-service").empty();
                        serviceList();
                   
                },
                error: function (xhr, textStatus, thrownError) {
                    var msg = 'Sorry but there was an error: ';
                    $('#error').html(msg + xhr.status + ' ' + xhr.statusText); 
                    console.log(xhr.status);
                    console.log(textStatus);
                    console.log(thrownError);
                }
            });
        });
});


// Fn - get userdata by id
function get_service_by_id() {

    var id = window.location.href.substring(window.location.href.lastIndexOf('/') + 1);
    var url = baseUrl+"/service/ajax_get_service/"+id;

    $.ajax({
        type: 'GET',
        url: url,  
        dataType: 'json',
        success: function (data, textStatus, xhr) {
            
            // console.log(data);
            $('#id').val(data.data.id);
            $('#name').val(data.data.name);
            $('#code').val(data.data.code);
            $('#type_id').val(data.data.type_id);
            $('#category_id').val(data.data.category_id);
            // sub cat
            pid = data.data.category_id;
            sub_cat_id = data.data.sub_category_id;
            $.ajax({
                url: baseUrl+'/service/ajax_get_subcategory/'+pid,
                type: 'GET',
                dataType: "JSON",
                success: function (data, textStatus, xhr) {
                    var html = '';
                    $.each(data.data, function(entryIndex, entry){
                        html += "<option value=\""+entry.id+"\">" + entry.name + "</option>";
                        
           
                    }); 
                    $('#sub_category_id').empty(); 
                    $('#sub_category_id').html(html);

                    $('#sub_category_id').val(sub_cat_id);
                },

                error: function (xhr, textStatus, thrownError) {

                    var msg = 'Sorry but there was an error: ';

                    $('#error').html(msg + xhr.status + ' ' + xhr.statusText); 

                    console.log(xhr.status);

                    console.log(textStatus);
                     console.log(thrownError);

                     // $('#box p').empty();    

                }

            }); 

            $('#unit_id').val(data.data.unit_id);
            
            //status cheak
            $('#is_active').val(data.data.is_active);
            if (data.data.is_active == 0) {
                $("input[name='is_active']").prop('checked', false);
            }
            else {
                $("input[name='is_active']").prop('checked', true);
            }   
            // iCheck data update
            $('.i-checks').iCheck('update');
 

            $('.form-title').text('Service Edit');
            $('.btn-form-submit').text('UPDATE');
        },
        error: function (xhr, textStatus, thrownError) {
            var msg = 'Sorry but there was an error: ';
            $('#error').html(msg + xhr.status + ' ' + xhr.statusText); 
            console.log(xhr.status);
            console.log(textStatus);
            console.log(thrownError);  
        }
    });
}


// Service List
  function serviceList() {
        $('.dataTables-service').DataTable({
            "ajax": baseUrl+"/service/ajax_get_service",
            "columns": [
                { "data": "id" },
                { "data": "name" },
                { "data": "code" },
                { "data": "type_id" },
                { "data": "cat_name" },
                // { "data": "subcat_name" },
                {
                    data: "subcat_name",
                    render: function (data, type, full, meta) 
                    {
                        var subcat_name = (data ==null) ? 'NO PARENT' : data;
                      
                        return subcat_name;
                    }
                },
                { "data": "unit_id" },
                {
                    data: "is_active",
                    render: function (data, type, full, meta) 
                    {
                        var is_active = (data == 1) ? '<span class="label label-primary">ACTIVE</span>' : '<span class="label label-danger">DEACTIVE</span>';
                        // console.log(is_active);

                        // var dom = "<span class=\"label label-primary\">"+is_active+"</span>&nbsp;";
                        return is_active;
                    }
                },

                {
                    data: "id",
                    render: function (data, type, full, meta) {
                        return "<a href=\""+baseUrl+"/service/edit/"+data+"\" class=\"btn-edit btn btn-primary btn-xs btn-outline\" data-id="+data+"><i class=\"fa fa-edit\"></i></a>"
                        +"&nbsp;"+
                        "<a href=\""+baseUrl+"/service/del/"+data+"\" class=\"btn-del btn btn-danger btn-xs btn-outline\" data-id="+data+" onclick=\"return confirm('You are about to delete a record. This cannot be undone. Are you sure?');\"><i class=\"fa fa-remove\"></i></a>";
                    }
                }
            ],
            pageLength: 10,
            responsive: true,
            dom: '<"html5buttons"B>lTfgitp',
            buttons: [
                { extend: 'copy'},
                {extend: 'csv'},
                {extend: 'excel', title: 'ExampleFile'},
                {extend: 'pdf', title: 'ExampleFile'},

                {extend: 'print',
                 customize: function (win){
                        $(win.document.body).addClass('white-bg');
                        $(win.document.body).css('font-size', '10px');

                        $(win.document.body).find('table')
                                .addClass('compact')
                                .css('font-size', 'inherit');
                }
                }
            ],
            destroy: true   // https://datatables.net/manual/tech-notes/3 [bugfix - call in fn and realaod]
        });
    }


function categoryList() {

    $.ajax({

        type: 'GET',

        url: baseUrl+'/service/ajax_get_category',  

         dataType: 'json',

         async: false,

         cache: false,

         success: function (data, textStatus, xhr) {

             // console.log(data.data[0].name);

             // console.log(data);

             // console.log(data.data[0].mobile);

             $.each(data.data, function(entryIndex, entry){

                                   
                 var html = '';
                     // console.log(entryIndex); 
                     // console.log(entry['name']);  

                     html += "<option value=\""+entry.id+"\">" + entry.name + "</option>";  
                      $('#category_id').append(html);
                    }); 
             // $('#box p').html(data);

            // $('#error').html('');

        },

        error: function (xhr, textStatus, thrownError) {

            var msg = 'Sorry but there was an error: ';

            $('#error').html(msg + xhr.status + ' ' + xhr.statusText); 

            console.log(xhr.status);

            console.log(textStatus);
             console.log(thrownError);

             $('#box p').empty();    

         }

     }); 

}

