$(document).ready(function() {	
	console.log('Checked! [common/function.js]');
});

// toastr notification
function toast_notify(type,msg) {
    // toastr.success(res.msg);
    // toastr.info(res.msg);
    // toastr.warning(res.msg);
    // toastr.error(res.msg);
    setTimeout(function() {
        toastr.options = {
            "closeButton": true,
            "debug": false,
            "progressBar": true,
            "preventDuplicates": false,
            "positionClass": "toast-top-right",
            "showDuration": "400",
            "hideDuration": "1000",
            "timeOut": "4000",
            "extendedTimeOut": "1000",
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "slideDown",
            "hideMethod": "fadeOut"
        };

        switch(type) {
            case "success":
                toastr.success(msg);
                break;
            case "info":
                toastr.info(msg);
                break;
            case "warning":
                toastr.warning(msg);
                break;
            case "error":
                toastr.error(msg);
                break;
            default:
                toastr.warning(msg);
        }
    }, 1300);
}