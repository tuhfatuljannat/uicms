$(document).ready(function() {
	console.log('Checked! [inspinia/js/custom.js]');

    // iCheck Load - Inspinia - form()
        $('.i-checks').iCheck({
            checkboxClass: 'icheckbox_square-green',
            radioClass: 'iradio_square-green'
        });

    // Chosen-select
        $('.chosen-select').chosen({width: "100%"});

    // Datepicker - DOB
        var mem = $('#data_1 .input-group.date').datepicker({
            todayBtn: "linked",
            keyboardNavigation: false,
            forceParse: false,
            calendarWeeks: true,
            autoclose: true,
            format: "dd/mm/yyyy"
        });
        $('.date').datepicker().datepicker('setDate', 'today');

    // Get Base Url
        var getUrl = window.location;
        baseUrl = getUrl .protocol + "//" + getUrl.host + "/" + getUrl.pathname.split('/')[1];

    


/*
	$('.dataTables-users').DataTable({
        "ajax": "http://localhost:85/inspinia/users/ajax_get_users",
       // "ajax": {
       //            "url": "http://localhost:85/inspinia/users/ajax_get_users",
       //            "dataSrc": ""
       //          },
        
        "columns": [
            { "data": "id" },
            { "data": "name" },
            { "data": "username" },
            { "data": "role_id" },
            { "data": "email" },
            { "data": "phone" },
            { "data": "password" },
            { "data": "picture" },
            { "data": "address" },
            { "data": "is_active" },
            {
                data: "id",
                render: function (data, type, full, meta) {
                    return '<a class="edit" href="http://localhost:85/inspinia/users/user/'+ data +'">' + 'Edit' + '</a>';
                }
            }
        ],
        // "columnDefs": [ {
        //     "targets": -1,
        //     "data": null,
        //     "defaultContent": "<button>Click!</button>"
        // } ],
        "deferRender": true,
        pageLength: 10,
        responsive: true,
        dom: '<"html5buttons"B>lTfgitp',
        buttons: [
            { extend: 'copy'},
            {extend: 'csv'},
            {extend: 'excel', title: 'ExampleFile'},
            {extend: 'pdf', title: 'ExampleFile'},

            {extend: 'print',
            	customize: function (win){
                    $(win.document.body).addClass('white-bg');
                    $(win.document.body).css('font-size', '10px');

                    $(win.document.body).find('table')
                            .addClass('compact')
                            .css('font-size', 'inherit');
            	}
            }
        ]
    });
*/
/*
    $(".edit").click(function(event){
        event.preventDefault();
        alert('asdsd');
    });
*/

/*
    $.ajax({
        type: 'GET',
        url: 'http://localhost:85/inspinia/users/ajax_get_users',  
        dataType: 'json',
        cache: false,
        success: function (data, textStatus, xhr) {
                // $('#box p').html(data);
                // $('#error').html('');
                data = JSON.stringify(data);
                console.log(data);


                data = JSON.parse(data);
                // console.log(data);
                // console.log(data[0]);
                // $('.dataTables-users').DataTable({
                //     pageLength: 10,
                //     "data": data,
                //     "ajax": {
                //           "url": data,
                //           "dataSrc": ""
                //         },
                //     "columns" : [
                //         { "data": "id" },
                //         { "data": "name" },
                //         { "data": "username" },
                //         { "data": "role_id" },
                //         { "data": "email" },
                //         { "data": "phone" },
                //         { "data": "password" },
                //         { "data": "picture" },
                //         { "data": "address" },
                //         { "data": "is_active" }
                //     ],
                //     pageLength: 10,
                //     responsive: true,
                //     dom: '<"html5buttons"B>lTfgitp',
                //     buttons: [
                //         { extend: 'copy'},
                //         {extend: 'csv'},
                //         {extend: 'excel', title: 'ExampleFile'},
                //         {extend: 'pdf', title: 'ExampleFile'},

                //         {extend: 'print',
                //             customize: function (win){
                //                 $(win.document.body).addClass('white-bg');
                //                 $(win.document.body).css('font-size', '10px');

                //                 $(win.document.body).find('table')
                //                         .addClass('compact')
                //                         .css('font-size', 'inherit');
                //             }
                //         }
                //     ]
                // });
        },
        error: function (xhr, textStatus, thrownError) {
            var msg = 'Sorry but there was an error: ';
            $('#error').html(msg + xhr.status + ' ' + xhr.statusText); 
            console.log(xhr.status);
            console.log(textStatus);
            console.log(thrownError);
            // $('#box p').empty();    
        }
    }); 
*/
    
    // Get seesion id
    var id = $('.uid').text();
    get_user_info(id);
    // console.log(id);
    
});


function get_user_info(id){
    var url = baseUrl+"/user/ajax_get_user/"+id;
    $.ajax({
        type: 'GET',
        url: url,  
        dataType: 'json',
        success: function (data, textStatus, xhr) {
            // console.log(data);

            // $('#id').val(data.data.id);
            $('.uname').text(data.data.name);
            $('.uemail').text(data.data.email);

            $("#upic").prop("src",baseUrl+"/public/file_manager/"+data.data.file_name);
            
        },
        error: function (xhr, textStatus, thrownError) {
            var msg = 'Sorry but there was an error: ';
            $('#error').html(msg + xhr.status + ' ' + xhr.statusText); 
            console.log(xhr.status);
            console.log(textStatus);
            console.log(thrownError);  
        }
    });
}

